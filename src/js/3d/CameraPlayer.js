var CameraPlayer = function(camera, track, options){
	console.log("CameraPlayer: constructor", camera, track);
	this.options = Object.assign({}, CameraPlayer.defaults, (options || {}));
	this.track = track || [];
	this.cursor = 0;
	this.animation = null;
	this.state = CameraPlayer.STATE.NONE;
	this.camera = camera;

	// callbacks
	this.callbacks = {
		'end': null,
	};
};
CameraPlayer.STATE = {
	NONE: 0,
	PLAYING: 1,
	PAUSED: 2,
};
CameraPlayer.defaults = {
	easing: TWEEN.Easing.Linear.None,
	timing: 2000  // animation timing in milliseconds
};
CameraPlayer.create = function(camera, track) {
	console.log("CameraPlayer: create");
	return new CameraPlayer(camera, track);
};
CameraPlayer.prototype = Object.create( THREE.EventDispatcher.prototype );
CameraPlayer.prototype.constructor = CameraPlayer;

CameraPlayer.prototype.onEnd = function(callback) {
	console.log("CameraPlayer: onEnd");
	this.callbacks['end'] = callback;
	return this;
};
CameraPlayer.prototype.setCamera = function (camera) {
	if (this.isPlaying()) {
		console.error("Cannot set camera while playing");
		return false;
	}
	if (this.camera !== camera) {
		this.camera = camera;
		// reset animation so that when play back starts from the beginning
		this.reset();
	}

	return this;
};
CameraPlayer.prototype.setTrack = function (track) {
	if (this.isPlaying()) {
		console.error("Cannot set track while playing");
		return false;
	}
	this.track = track;
	return this;
};
CameraPlayer.prototype.isPlaying = function() {
	return this.state === CameraPlayer.STATE.PLAYING;
};
CameraPlayer.prototype.play = function(startOver) {
	if (this.state === CameraPlayer.STATE.PLAYING) {
		console.warn("Already playing");
		return false;
	}

	if (startOver) {
		this.cursor = 0;
	} else {
		// resume if paused
		if (this.animation) {
			this.state = CameraPlayer.STATE.PLAYING;
			this.animation.start();
			return this;
		}
	}
	// start moving camera
	this._move();

	return this;
};
CameraPlayer.prototype.pause = function() {
	if (this.animation) {
		this.animation.stop();
	}
	if (this.state !== CameraPlayer.STATE.NONE) {
		this.state = CameraPlayer.STATE.PAUSED;
	}
};

CameraPlayer.prototype._trigger = function(event) {
	if (this.callbacks[event]) {
		this.callbacks[event]();
	}
};
CameraPlayer.prototype.reset = function() {
	this.state = CameraPlayer.STATE.NONE;
	this.cursor = 0;
	if (this.animation) {
		this.animation.stop();
		this.animation = null;
	}
};
CameraPlayer.prototype._end = function(){
	this.reset();
	this._trigger('end');
};
CameraPlayer.prototype._move = function() {
	if (this.cursor >= this.track.length-1) {
		console.log("End of track");
		this._end();
		return false;
	}
	let from = this.track[this.cursor];
	this.cursor++;
	let to = this.track[this.cursor];
	this.animation = this._animate(this.camera, from, to, this._move.bind(this));
	return true;
};
CameraPlayer.prototype._animate = function(camera, from, to, onComplete) {
	console.log("CameraPlayer: _animate", this.cursor, from, to);
	let start = {
		px: from.position.x,
		py: from.position.y,
		pz: from.position.z,
		tx: from.lookAt.x,
		ty: from.lookAt.y,
		tz: from.lookAt.z,
	};

	let end = {
		px: to.position.x,
		py: to.position.y,
		pz: to.position.z,
		tx: to.lookAt.x,
		ty: to.lookAt.y,
		tz: to.lookAt.z,
	};
	let easing = to.easing || this.options.easing;
	let timing = to.timing || this.options.timing;

	let anim = new TWEEN.Tween(start)
		.to(end, timing)
		.easing(easing)
		.onUpdate(function ( val ) {
			camera.position.set(val.px, val.py, val.pz);
			camera.lookAtPosition(val.tx, val.ty, val.tz);
		})
		.onComplete(function () {
			if (onComplete) {
				onComplete();
			}
		})
		.start();

	return anim;
};
