importScripts( './three/three.js' );

self.onmessage = function ( e ) {
  var data = e.data;

  if ( ! data ) {
    console.log("W: No data");
    return;
  }
  
  switch (data.cmd) {
    case 'setObstacles':
      self.setObstacles(data.data);
      break;
    case 'checkCollision':
      self.checkCollision(data.data);
      break;
    default:
      console.log("Unknown command", data.cmd, data);
      break;
  }
};

self.obstacles = {};
self.setObstacles = function(data) {
  console.log("W: setObstacles", data);
  self.obstacles[data.mode] = [];

  var objLoader = new THREE.ObjectLoader();
  var geometries = objLoader.parseGeometries(data.obstacles.g);
  var meshes = self.obstacles[data.mode];
  var material = new THREE.MeshBasicMaterial({side: THREE.DoubleSide});
  var matrix = new THREE.Matrix4();
  var i = 0;
  for (var k in geometries) {
    var mesh = new THREE.Mesh(geometries[k], material);
    matrix.set(data.obstacles.m[i++]);
    mesh.applyMatrix(matrix);
    meshes.push(mesh);
  }

  console.log("W: Obstacles ", data.mode, self.obstacles[data.mode]);
};

/**
 *
 * @param data
 * {
 *   source: source identifier
 *   mode: 3D/2D
 *   oldPosition: Vector3.array
 *   newPosition: Vector3.array
 *   oldTarget: Vector3.array
 * }
 */
self.checkCollision = function (data) {
  var MIN_GAP = 0.5;
  var direction = new THREE.Vector3(),
    oldPosition = new THREE.Vector3(),
    newPosition = new THREE.Vector3(),
    rayCaster = new THREE.Raycaster(),
    minDistance;
  // return function checkCollision(data) {

    console.log("W: checkCollision");
    oldPosition.fromArray(data.oldPosition);
    newPosition.fromArray(data.newPosition);

    direction.copy(newPosition).sub(oldPosition).normalize();
    minDistance = Math.max(oldPosition.distanceTo(newPosition), MIN_GAP);

    rayCaster.set(oldPosition, direction);

    var collides = false;
    var intersects = rayCaster.intersectObjects(self.obstacles[data.mode], true);
    for (var i=0, l=intersects.length; i<l; i++) {
      if (intersects[i].distance < minDistance ) {
        newPosition.copy(oldPosition);
        collides = true;
        console.log("W: Collides", newPosition);
        break;
      }
    }
    var result = {
      collides: collides,
      newPosition: newPosition.toArray(),
      oldTarget: data.oldTarget
    };

    // console.log("W: checkCollision result", result);

    self.postMessage({
      source: data.source,
      cmd: 'checkCollision',
      data: result
    });

  // }
};