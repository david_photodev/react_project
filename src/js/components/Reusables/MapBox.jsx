import React, { Component } from 'react';
import { Modal, Button } from 'react-bootstrap';
import PropTypes from 'prop-types';

class MapBox extends Component {
  constructor(props) {
    super(props);
  }

  componentWillMount() {
    var map_element = document.getElementById('hotel-map');
  }

  componentDidMount() {

    mapboxgl.accessToken = 'pk.eyJ1Ijoic3BhemlvdXMiLCJhIjoiY2pwdHB2bmVsMDZmbjQzdDc4eTZlc3k4cCJ9.dwEJBMDWRxue0KB0rxTCYA';

    var map_element = document.getElementById('hotel-map');
    $(document).ready(function() {
      var hotel_location_str = document.getElementById('hotel-map').getAttribute("data-location");
      var hotel_location_arr = hotel_location_str.split(',');
      var hotel_location = [];
      hotel_location[0] = hotel_location_arr[1].trim();
      hotel_location[1] = hotel_location_arr[0].trim();
      var map = new mapboxgl.Map({
        style: 'mapbox://styles/mapbox/light-v9',
        center: hotel_location,
        zoom: 15.5,
        pitch: 45,
        bearing: -17.6,
        container: 'hotel-map'
      });
      map.on('load', function() {
        var layers = map.getStyle().layers;
         
        var labelLayerId;
        for (var i = 0; i < layers.length; i++) {
          if (layers[i].type === 'symbol' && layers[i].layout['text-field']) {
            labelLayerId = layers[i].id;
            break;
          }
        }
       
        map.addLayer({
        'id': '3d-buildings',
        'source': 'composite',
        'source-layer': 'building',
        'filter': ['==', 'extrude', 'true'],
        'type': 'fill-extrusion',
        'minzoom': 15,
        'paint': {
        'fill-extrusion-color': '#aaa',
         
        // use an 'interpolate' expression to add a smooth transition effect to the
        // buildings as the user zooms in
        'fill-extrusion-height': [
        "interpolate", ["linear"], ["zoom"],
        15, 0,
        15.05, ["get", "height"]
        ],
        'fill-extrusion-base': [
        "interpolate", ["linear"], ["zoom"],
        15, 0,
        15.05, ["get", "min_height"]
        ],
        'fill-extrusion-opacity': .6
        }
        }, labelLayerId);

        map.addControl(new mapboxgl.NavigationControl());

        var element = document.createElement('div');
        element.id = 'property-map';
        element.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg" width="42.66" height="122" viewBox="0 0 42.66 122"><g id="Group_1" data-name="Group 1" transform="translate(0 0.03)">
                        <rect id="Rectangle_1258" data-name="Rectangle 1258" width="42.66" height="122" transform="translate(0 -0.03)" fill="none"/><g id="Group_4001" data-name="Group 4001" transform="translate(6 4)">
                          <g id="Group_3999" data-name="Group 3999">
                            <path class="fill-primary" id="b" d="M15.79,25a7.35,7.35,0,0,0,3.77-1.01,7.474,7.474,0,0,0,2.75-2.72,7.208,7.208,0,0,0,1.01-3.73,7.35,7.35,0,0,0-1.01-3.77,7.554,7.554,0,0,0-2.75-2.75,7.324,7.324,0,0,0-3.77-1.01,7.208,7.208,0,0,0-3.73,1.01,7.706,7.706,0,0,0-2.72,2.75,7.324,7.324,0,0,0-1.01,3.77A7.409,7.409,0,0,0,15.79,25ZM15.33-4A20.419,20.419,0,0,1,25.94-1.14,21.239,21.239,0,0,1,36.33,17.34,29.677,29.677,0,0,1,34.16,27.6a73,73,0,0,1-5.2,10.63q-2.745,4.545-6.28,9.46-2.475,3.465-5.2,6.74L15.33,57l-2.17-2.57q-2.745-3.3-5.2-6.74-3.54-4.92-6.28-9.46A75.636,75.636,0,0,1-3.52,27.6,29.874,29.874,0,0,1-5.68,17.34,21.239,21.239,0,0,1,4.71-1.14,20.506,20.506,0,0,1,15.33-4Z" fill="red" fill-rule="evenodd"/>
                          </g>
                          <g id="Group_4000" data-name="Group 4000">
                            <path class="fill-primary" id="Path_3665" data-name="Path 3665" d="M15.79,25a7.35,7.35,0,0,0,3.77-1.01,7.474,7.474,0,0,0,2.75-2.72,7.208,7.208,0,0,0,1.01-3.73,7.35,7.35,0,0,0-1.01-3.77,7.554,7.554,0,0,0-2.75-2.75,7.324,7.324,0,0,0-3.77-1.01,7.208,7.208,0,0,0-3.73,1.01,7.706,7.706,0,0,0-2.72,2.75,7.324,7.324,0,0,0-1.01,3.77A7.409,7.409,0,0,0,15.79,25ZM15.33-4A20.419,20.419,0,0,1,25.94-1.14,21.239,21.239,0,0,1,36.33,17.34,29.677,29.677,0,0,1,34.16,27.6a73,73,0,0,1-5.2,10.63q-2.745,4.545-6.28,9.46-2.475,3.465-5.2,6.74L15.33,57l-2.17-2.57q-2.745-3.3-5.2-6.74-3.54-4.92-6.28-9.46A75.636,75.636,0,0,1-3.52,27.6,29.874,29.874,0,0,1-5.68,17.34,21.239,21.239,0,0,1,4.71-1.14,20.506,20.506,0,0,1,15.33-4Z" fill="red" fill-rule="evenodd"/>
                          </g>
                        </g>
                      </g>
                    </svg>`;
        new mapboxgl.Marker(element)
        .setLngLat(hotel_location)
        .addTo(map);
      });
    });
  }


  render() {
    const {
      showMapModal, handleMapClose, hotelInfo
    } = this.props;

    return (
      <Modal show={showMapModal} className="mapbox-popup">
        <Modal.Header>
          <Modal.Title></Modal.Title>
          <span
            className="close-icon"
            onClick={handleMapClose}
          >
            &#x2715;
          </span>
        </Modal.Header>
        <Modal.Body className="modal-body">
          <div id='hotel-map' data-location={hotelInfo.data.location}></div>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleMapClose}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }
}

MapBox.propTypes = {
  handleMapClose: PropTypes.func.isRequired,
  showMapModal: PropTypes.bool.isRequired,
  hotelInfo:PropTypes.bool.isRequired,
};

export default MapBox;
