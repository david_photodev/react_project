import React, { Component } from 'react';
import {
  Grid, Row, Col,
} from 'react-bootstrap';
import PropTypes from 'prop-types';
import MeetingSpaceAction from '../../actions/MeetingSpaceAction';
import CapacityColumn from './CapacityColumn';
import RoomGallery from './RoomGallery';
import ReactSVG from 'react-svg';
import Helpers from '../Reusables/Helpers';

class CapacityChart extends Component {
  constructor(props) {
    super(props);
    const { hotelInfo } = this.props;
    const { meetingRoomInfo } = hotelInfo;
    this.state = {
      meetingRoomInfo,
    };
    this.capacityArray = ['0-20', '21-50', '51-100', '101-200', 'More than 201'];
    this.handleSetupConfig = this.handleSetupConfig.bind(this);
    this.handleKeyUp = this.handleKeyUp.bind(this);
    this.handleOwnConfig = this.handleOwnConfig.bind(this);
    this.showFullImage = this.showFullImage.bind(this);
    this.sortBySpace = this.sortBySpace.bind(this);
    this.sortByDimensions = this.sortByDimensions.bind(this);
    this.sortByHeight = this.sortByHeight.bind(this);
    this.sortBySetup = this.sortBySetup.bind(this);
  }

  componentDidMount() {
    Helpers.adjustForScrollBar('.capacity-chart-content');
  }

  static getDerivedStateFromProps(props, state) {
    if (state.meetingRoomInfo !== props.meetingRoomInfo) {
      return {
        meetingRoomInfo: props.hotelInfo.meetingRoomInfo,
      };
    }
    return null;
  }

  showFullImage(item, type) {
    const { showFullImage } = this.props;
    showFullImage(item, type);
  }

  handleOwnConfig(room) {
    MeetingSpaceAction.storeSceneData(room.scenes);
    const { handle3DPopup, match } = this.props;
    if (!room.available_model) {
      handle3DPopup(true);
    } else {
      MeetingSpaceAction.storeSceneData(room.scenes);
      const customSetup = room.setup.filter(setup => setup.type.toLowerCase() === 'custom');
      MeetingSpaceAction.updateLayout(room, customSetup[0]);
      MeetingSpaceAction.createOwnSetup();
      this.context.router.history.push(`${match.url}/dollhouse`);
      this.showFullImage(0, 'close');
    }
  }


  handleSetupConfig(room, setup) {
    const match = this.props;
    const roomId = room.id;
    const setupId = setup.id;
    const setupCount = setup.count;
    const { meetingRoomInfo } = this.state;
    let msg = '';
    const selectedSetup = meetingRoomInfo.filter(roomSetupConfigs => roomSetupConfigs.id === roomId);
    if (setupCount > 0){
      const selectedRoom = selectedSetup[0].setup.map((item) => {
        const selectedConfig = item;
        if (item.id !== setupId) {
          // selectedConfig['active'] = false;
          return selectedConfig;
        }
        selectedConfig['active'] = !selectedConfig['active'];
        if (selectedConfig['active']) {
          msg = `${selectedSetup[0].room_name} added to RFP`;
          MeetingSpaceAction.notificationMsg(msg, 'add', 'event');
        }
        setTimeout(() => {
          msg = `${selectedSetup[0].room_name} added to RFP`;
          MeetingSpaceAction.notificationMsg(msg, 'remove', 'event');
        }, 5000);
        return selectedConfig;
      });
      MeetingSpaceAction.updateRoomSetup(roomId, setupId, selectedRoom);
      MeetingSpaceAction.updateLayout(room, setup, 'view');

      MeetingSpaceAction.storeSceneData(room.scenes);
      MeetingSpaceAction.showSetupView(room);

      this.context.router.history.push(`${match.url}/dollhouse`);
    } 
  }

  handleKeyUp(e) {
    console.log(e);
  }

  sortBySpace(order) {
    this.setState(prevState => ({
      sortingSpaceAscOrder: prevState.order === 'room_space' ? !prevState.sortingSpaceAscOrder : true,
      order: 'room_space',
      setupType: '',
    }), () => {
      const { sortingSpaceAscOrder } = this.state;
      MeetingSpaceAction.sortBy('room_space', sortingSpaceAscOrder, false);
    });
  }

  sortByDimensions(order) {
    this.setState(prevState => ({
      sortingDimensAscOrder: prevState.order === 'dimensions' ? !prevState.sortingDimensAscOrder : true,
      order: 'dimensions',
      setupType: '',
    }), () => {
      const { sortingDimensAscOrder } = this.state;
      MeetingSpaceAction.sortBy('dimensions', sortingDimensAscOrder, false);
    });
  }

  sortByHeight(order) {
    this.setState(prevState => ({
      sortingHeightAscOrder: prevState.order === 'ceiling_height' ? !prevState.sortingHeightAscOrder : true,
      order: 'ceiling_height',
      setupType: '',
    }), () => {
      const { sortingHeightAscOrder } = this.state;
      MeetingSpaceAction.sortBy('ceiling_height', sortingHeightAscOrder, false);
    });
  }

  sortBySetup(setup) {
    this.setState(prevState => ({
      sortBySetupAscOrder: prevState.setupType === setup.type.toLowerCase() ? !prevState.sortBySetupAscOrder : true,
      setupType: setup.type.toLowerCase(),
      order: 'setup',
    }), () => {
      const { sortBySetupAscOrder } = this.state;
      MeetingSpaceAction.sortBy('setup', sortBySetupAscOrder, setup);
    });
  }

  render() {
    const {
      hotelInfo, handleDropdown, typeOfSetup, typeOfCapacity, isFullImage, handleToggle, sqmMode, userPrimaryColor,
    } = this.props;
    const { available_setup } = hotelInfo;
    const {
      meetingRoomInfo, sortingSpaceAscOrder, sortingDimensAscOrder, sortingHeightAscOrder, sortBySetupAscOrder,
      setupType, order,
    } = this.state;
    const styleObj = {
      borderColor: `${userPrimaryColor}24`,
    };
    const imgStyle = {
      borderTop: `13px solid ${userPrimaryColor}`,
    };
    const checkmarkStyle = {
      backgroundColor: userPrimaryColor,
    };
    const { data } = hotelInfo;
    const { primary_color } = data;
    const circleSliderStyle = { background: `linear-gradient(159deg, ${userPrimaryColor}80, ${userPrimaryColor})` };
    return (
      <Grid className="primary-color">
        <Row className="capacity-chart-room-container prelative">
          <div className="overlay" />
          <div className="text-right meter-feet-toggle">
            <span className={sqmMode ? "i-block font-bold rmargin20" : "i-block rmargin20"}>SQM</span>
            <label className="switch meter-toggle">
              <input type="checkbox" 
              checked = {!sqmMode}
              onChange={e => handleToggle(e)}
              />
              <span className="slider-no-background round">
                <span className="circle" style={circleSliderStyle} />
              </span>
            </label>
            <span className={sqmMode ? "i-block lmargin20" : "i-block font-bold lmargin20"}>SQFT</span>
          </div>
          <div className="filter-container fleft">
            <select
              className="filter-rooms"
              onChange={e => handleDropdown('setup', e)}
              value={typeOfSetup === 'setup' ? 'Type of Setup' : typeOfSetup}
            >
              <option key="setup" value="Type of Setup" disabled hidden>Type of Setup</option>
              <option
                key="all"
                value="all"
              >
                All
              </option>
              {
                available_setup.map(list => (
                  <option
                    key={list.type}
                    value={list.type}
                  >
                    {list.shape}
                  </option>
                ))
              }
            </select>
            <select
              className="filter-rooms"
              onChange={e => handleDropdown('capacity', e)}
              value={typeOfCapacity === 'capacity' ? 'Capacity' : typeOfCapacity}
            >
              <option key="capacity" value="Capacity" disabled hidden>Capacity</option>
              <option
                key="all"
                value="all"
              >
                All
              </option>
              {
                this.capacityArray.map(list => (
                  <option
                    key={list}
                    value={list}
                  >
                    {list}
                  </option>
                ))
              }
            </select>
          </div>
          {
            meetingRoomInfo && meetingRoomInfo.length >= 1
              ? (
                <Row className="capacity-chart-header block lmargin3">
                  <Col sm={2} md={2} lg={2} className="meeting-list-col text-center">
                    Rooms
                  </Col>
                  <Col sm={1} md={1} lg={1}
                    className={`meeting-list-col text-center ${order === 'room_space' ? 'text-bold' : ''}`}
                    style={styleObj}
                    onClick={() => this.sortBySpace(sortingSpaceAscOrder)}
                  >
                    {sqmMode ? 'm²': 'sq.ft.'}
                    {/*<img className="arrow-icon" src="dist/img/meeting_space/icons/arrow-icon.png" alt="arrow-icon" />*/}
                    <ReactSVG
                      wrapper="span"
                      className={`svg-wrapper arrow-icon
                        ${order === 'room_space' && sortingSpaceAscOrder ? 'transform' : ''}`}
                      src="dist/img/meeting_space/icons/arrow-icon.svg"
                      alt="arrow icon"
                    />
                  </Col>
                  <Col sm={1} md={1} lg={1} 
                    className={`meeting-list-col text-center ${order === 'dimensions' ? 'text-bold' : ''}`}
                    style={styleObj}
                    onClick={() => this.sortByDimensions(sortingDimensAscOrder)}
                  >
                    Room
                    <br />
                    Dimens.
                    {/*<img className="arrow-icon" src="dist/img/meeting_space/icons/arrow-icon.png" alt="arrow-icon" />*/}
                    <ReactSVG
                      wrapper="span"
                      className={`svg-wrapper arrow-icon
                        ${order === 'dimensions' && sortingDimensAscOrder ? 'transform' : ''}`}
                      src="dist/img/meeting_space/icons/arrow-icon.svg"
                      alt="arrow icon"
                    />
                  </Col>
                  <Col sm={1} md={1} lg={1}
                    className={`meeting-list-col text-center ${order === 'ceiling_height' ? 'text-bold' : ''}`}
                    style={styleObj}
                    onClick={() => this.sortByHeight(sortingHeightAscOrder)}
                  >
                    Ceiling
                    <br />
                    Height
                    {/*<img className="arrow-icon" src="dist/img/meeting_space/icons/arrow-icon.png" alt="arrow-icon" />*/}
                    <ReactSVG
                      wrapper="span"
                      className={`svg-wrapper arrow-icon
                        ${order === 'ceiling_height' && sortingHeightAscOrder ? 'transform' : ''}`}
                      src="dist/img/meeting_space/icons/arrow-icon.svg"
                      alt="arrow icon"
                    />
                  </Col>
                  {
                    available_setup.map(header => (
                      header.type.toLowerCase()  !== 'custom' ?
                      (<Col
                        className="meeting-list-col text-center"
                        sm={1}
                        md={1}
                        lg={1}
                        key={header.id}
                        style={styleObj}
                        onClick={() => this.sortBySetup(header)}
                      >
                        <div>
                          {/*<img className="setup-img" src={header.img} alt={header.shape} />*/}
                          <ReactSVG
                            wrapper="span"
                            className={`svg-wrapper setup-img`}
                            src={header.img}
                            alt={header.img}
                          />
                        </div>
                        <span
                          className={`i-block header-shape hidden-sm
                            ${header.type.toLowerCase() === setupType ? 'text-bold' : ''}`}
                        >
                          {header.shape}
                        </span>
                        {/*<img className="arrow-icon" src="dist/img/meeting_space/icons/arrow-icon.png" alt="arrow-icon" />*/}
                        <ReactSVG
                          wrapper="span"
                          className={`svg-wrapper arrow-icon
                            ${sortBySetupAscOrder && header.type.toLowerCase() === setupType ? 'transform' : ''}`}
                          src="dist/img/meeting_space/icons/arrow-icon.svg"
                          alt="arrow icon"
                        />
                      </Col>) : ''
                    ))
                  }
                </Row>
              )
              : (
                <div className={`padding100 text-center ${!meetingRoomInfo ? 'hide' : 'block'}`}>
                  Meeting rooms are not available for this filter. Please change the filter to see more meeting rooms.
                </div>
              )
          }
          {
            !meetingRoomInfo && (
              <div className="padding100 text-center">Meeting rooms are loading. Please wait...</div>
            )
          }
          {
            meetingRoomInfo && meetingRoomInfo.length >= 1
              ? (
                <Row className="capacity-chart-content block custom-scrollbar">
                  {
                      meetingRoomInfo.map(room => ([
                        <Row key={room.id} className="lmargin3">
                          <Col sm={2} md={2} lg={2} className="meeting-list-col" style={styleObj}>
                            <div
                              className="capacity-col pointer"
                              style={styleObj}
                            >
                              <img
                                style={room.room_selected ? imgStyle : {}}
                                className="meeting-room-thumbnail pointer"
                                src={room.room_image}
                                alt={room.room_name}
                                onClick={() => this.showFullImage(room, 'open')}
                              />

                              <span 
                              onClick={() => this.showFullImage(room, 'open')}
                              className="room-name block">
                              {room.room_name}
                              </span>
                              <div
                                onClick={() => this.showFullImage(room, 'open')} 
                                className="i-block capacity-info">
                                <img src="dist/img/meeting_space/icons/superficie-white-bg.png" alt="meeting icon" />
                                <span className="black-text opacity5 lpadding10">
                                {sqmMode? room.room_space : (parseFloat(room.room_space)*10.764).toFixed(2)+' sq.ft.'}
                                </span>
                              </div>
                              {
                                room.room_selected
                                  && ([
                                    <ReactSVG
                                      style={checkmarkStyle}
                                      wrapper="div"
                                      className="svg-wrapper checkmark-img-selected"
                                      src="dist/img/meeting_space/icons/checkmark.svg"
                                      alt="checkmark"
                                    />,
                                    <ReactSVG
                                      wrapper="div"
                                      className="svg-wrapper eye-icon"
                                      src="dist/img/meeting_space/icons/eye-red-bg.svg"
                                      alt="eye icon"
                                    />,
                                  ])
                              }
                            </div>
                          </Col>
                          <Col sm={1} md={1} lg={1} className="meeting-list-col lrpadding0 text-center" style={styleObj}>
                            <div className="capacity-col" style={styleObj}>
                              <span>{sqmMode? parseFloat(room.room_space) : (parseFloat(room.room_space)*10.764).toFixed(2)}</span>
                            </div>
                          </Col>
                          <Col sm={1} md={1} lg={1} className="meeting-list-col lrpadding0 text-center" style={styleObj}>
                            <div className="capacity-col" style={styleObj}>
                              <span>{room.dimensions}</span>
                            </div>
                          </Col>
                          <Col sm={1} md={1} lg={1} className="meeting-list-col lrpadding0 text-center" style={styleObj}>
                            <div className="capacity-col" style={styleObj}>
                              <span>{room.ceiling_height}</span>
                            </div>
                          </Col>
                          <CapacityColumn
                            item={room}
                            hotelInfo={hotelInfo}
                            handleSetupConfig={this.handleSetupConfig}
                            userPrimaryColor={this.props.userPrimaryColor}
                          />
                        </Row>,
                      ]))
                    }
                  {
                    isFullImage
                    && (
                      <RoomGallery
                        hotelInfo={hotelInfo}
                        handleOwnConfig={this.handleOwnConfig}
                        showFullImage={this.showFullImage}
                        userPrimaryColor={this.props.userPrimaryColor}
                      />
                    )
                  }
                </Row>
              ) : ''
          }
        </Row>
      </Grid>
    );
  }
}

CapacityChart.propTypes = {
  hotelInfo: PropTypes.object.isRequired,
  handleDropdown: PropTypes.func.isRequired,
  showFullImage: PropTypes.func.isRequired,
  handleTabClick: PropTypes.func.isRequired,
  handle3DPopup: PropTypes.func.isRequired,
  typeOfSetup: PropTypes.string.isRequired,
  typeOfCapacity: PropTypes.string.isRequired,
  toPageNavigate: PropTypes.string.isRequired,
  isFullImage: PropTypes.bool.isRequired,
  handleToggle: PropTypes.func.isRequired,
  sqmMode: PropTypes.bool.isRequired,
  match: PropTypes.string.isRequired,
};

CapacityChart.contextTypes = {
  router: PropTypes.object,
};


export default CapacityChart;
