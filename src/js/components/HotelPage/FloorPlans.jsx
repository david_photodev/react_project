import React, { Component } from 'react';
import {
  Grid, Row, Col, Button,
} from 'react-bootstrap';
import PropTypes from 'prop-types';
import ReactSVG from 'react-svg';
import MeetingSpaceAction from '../../actions/MeetingSpaceAction';

class FloorPlans extends Component {
  constructor(props) {
    super(props);
    const { hotelInfo } = this.props;
    const { allMeetingRooms, selectedRooms } = hotelInfo;
    this.state = {
      isRoomEntered: false,
      hoveredRoom: {},
      allMeetingRooms,
      selectedRooms,
    };
    this.roomEnter = this.roomEnter.bind(this);
    this.handleSelectedRoom = this.handleSelectedRoom.bind(this);
    this.handleRoomReady = this.handleRoomReady.bind(this);
    this.handlePopupHover = this.handlePopupHover.bind(this);
    this.handle3DSetup = this.handle3DSetup.bind(this);
  }

  static getDerivedStateFromProps(props, state) {
    if (state.allMeetingRooms !== props.hotelInfo.allMeetingRooms
      || state.selectedRooms !== props.hotelInfo.selectedRooms) {
      return {
        allMeetingRooms: props.hotelInfo.allMeetingRooms,
        selectedRooms: props.hotelInfo.selectedRooms,
      };
    }
    return null;
  }

  componentDidMount() {
    const { allMeetingRooms, selectedRooms } = this.state;
    const { setFloorPlanLoader, property } = this.props;
    console.log('property', property);
    // const rooms = [];
    // for (let i = 0; i < allMeetingRooms.length - 1; i++) {
    //   const roomName = i + 1;
    //   rooms.push({
    //     id: allMeetingRooms[i].id,  // room id from backend
    //     model: {  // floor plan model from backend
    //       id: 7000+i,  // model id
    //       path: 'models/HotelLowerLobbyV2/r-'+('000'+roomName).slice(-3)+'.gltf'  // model path
    //     },
    //   });
    // }
    var fp = null;
    if (fp) {
      console.warn("Floor plan already loaded");
      return;
    }
    var container = document.getElementById('floor-scene-container');
    fp = new FloorPlan(container);
    setFloorPlanLoader(true);
    fp.addEventListener(EVENT.ITEM_ENTER, event => this.roomEnter(event));
    fp.addEventListener(EVENT.ITEM_LEAVE, event => this.roomEnter(event));
    fp.addEventListener(EVENT.ITEM_SELECTED, event => this.handleSelectedRoom(event, true));
    fp.addEventListener(EVENT.ITEM_UNSELECTED, event => this.handleSelectedRoom(event, false));
    fp.addEventListener(EVENT.READY, this.handleRoomReady);

    MeetingSpaceAction.getFloorPlanDetails(property, (err, data) => {
      if (err || ! data.floorplan) {
        console.error("Could not get floorplan details", err, data);
        return;
      }
      console.log("Floorplan Data", data);
      fp.init(data);
      this.setState({ floorPlan: fp });
      MeetingSpaceAction.saveFloorPlan(fp);
      const selectedRoomIds = selectedRooms.map(room => room.id);
      selectedRoomIds.map((rid) => { fp.setRoomSelected(rid, true); });
    });

  // function removefp() {
  //   if ( ! fp) {
  //     console.warn("Floor plan not loaded");
  //     return;
  //   }
  //   fp.dispose();
  //   fp = null;
  // }
  }

  handleSelectedRoom(e, isActive) {
    console.log('TYPE:', e.type, 'DATA:', e.data);
    const match = this.props;
    const { hotelInfo } = this.props;
    const { floorPlan } = this.state;
    const { allMeetingRooms } = hotelInfo;
    const room = allMeetingRooms.filter(roomSetup => roomSetup.id === e.data.room.id)[0];
    const setup = room.setup.filter(setupConf => setupConf.type.toLowerCase() === 'custom')[0];
    const roomId = room.id;
    const setupId = setup.id;
    MeetingSpaceAction.updateAddons(room, setup, 'allMeetingRooms');
    let msg = '';
    const selectedSetup = allMeetingRooms.filter(roomSetupConfigs => roomSetupConfigs.id === roomId);
    const selectedRoom = selectedSetup[0].setup.map((item) => {
      const selectedConfig = item;
      if (item.id !== setupId) {
        // selectedConfig['active'] = false;
        return selectedConfig;
      }
      selectedConfig['active'] = isActive;
      selectedConfig['activity_name'] = '';
      selectedConfig['event_date_starts'] = '';
      selectedConfig['event_date_ends'] = '';
      selectedConfig['event_hour'] = '';
      selectedConfig['attendees_count'] = null;
      if (selectedConfig['active']) {
        msg = `${selectedSetup[0].room_name} added to RFP`;
        MeetingSpaceAction.notificationMsg(msg, 'add');
      }
      setTimeout(() => {
        msg = `${selectedSetup[0].room_name} added to RFP`;
        MeetingSpaceAction.notificationMsg(msg, 'remove');
      }, 5000);
      return selectedConfig;
    });
    MeetingSpaceAction.updateRoomSetup(roomId, setupId, selectedRoom);
    MeetingSpaceAction.updateLayout(room, setup, 'view');

    MeetingSpaceAction.storeSceneData(room.scenes);
    
    MeetingSpaceAction.showSetupView(room);

    this.context.router.history.push(`${match.url}/dollhouse`);

    floorPlan.onResize();
  }

  roomEnter(e) {
    const { hotelInfo } = this.props;
    let { hoveredRoom } = this.state;
    console.log('TYPE:', e.type, 'DATA:', e.data);
    console.log('ALLMeetingRooms', hotelInfo.allMeetingRooms);
    if (e.type === 'itementer') {
      const filterHoveredRoom = hotelInfo.allMeetingRooms.filter(room => room.id === e.data.room.id);
      hoveredRoom = filterHoveredRoom[0];
      MeetingSpaceAction.showFloorRoom(hoveredRoom);
      // To place the tooltip popup in the container
      const containerWidth = $('#floor-scene-container').width();
      const tooltipStyle = {
        right: containerWidth - ((e.data.bbox.min.x + e.data.bbox.max.x) / 2) - 270,
        top: e.data.bbox.max.y - 250,
      };
      this.setState({
        isRoomEntered: true,
        tooltipStyle,
        hoveredRoom,
      });
    } else if (e.type === 'itemleave') {
      this.setState({
        isRoomEntered: true,
      });
    }
  }

  handlePopupHover(e) {
    if (e.type === 'mouseenter') {
      this.setState({ isRoomEntered: true });
    } else {
      MeetingSpaceAction.hideFloorRoom();
      this.setState({ isRoomEntered: false });
    }
  }

  handleRoomReady() {
    const { floorPlan, selectedRooms } = this.state;
    const { setFloorPlanLoader } = this.props;
    setFloorPlanLoader(false);
    const selectedRoomIds = selectedRooms.map(room => room.id);
    selectedRoomIds.map(rid => floorPlan.setRoomSelected(rid, true));
  }

  handle3DSetup(item) {
    MeetingSpaceAction.storeSceneData(item.scenes);
    MeetingSpaceAction.showSetupView(item);
    const { handle3DPopup, match } = this.props;
    const { router } = this.context;
    if (!item.available_model) {
      handle3DPopup(true);
    } else {
      const customSetup = item.setup.filter(setup => setup.type.toLowerCase() === 'custom');
      MeetingSpaceAction.updateLayout(item, customSetup[0]);
      MeetingSpaceAction.createOwnSetup();
      router.history.push(`${match.url}/dollhouse`);
    }
  }

  render() {
    const {
      isRoomEntered, tooltipStyle, hoveredRoom, selectedRooms,
    } = this.state;
    const {
      room_image, room_space, room_capacity, id, name,
    } = hoveredRoom;
    const isSetupSelected = selectedRooms.filter(room => room.id === hoveredRoom.id).length >= 1;
    return (
      <Grid className="primary-color">
        <div
          className={`floorplan-popup-container ${isRoomEntered ? 'show' : 'hide'}
            ${isSetupSelected ? 'check-mark-border' : ''}`}
          onMouseEnter={this.handlePopupHover}
          onMouseLeave={this.handlePopupHover}
          style={tooltipStyle}
        >
          <div className="hotel-image">
            <div className="division">
              <img
                className={`popup-image ${isSetupSelected ? 'no-border-radius' : ''}`}
                src={room_image}
                alt="room_image"
                width="50px"
              />
              {
                isSetupSelected
                  && (
                    <ReactSVG
                      wrapper="div"
                      className="svg-wrapper checkmark-img-selected pointer primary-background"
                      src="dist/img/meeting_space/icons/checkmark.svg"
                      alt="checkmark"
                    />
                  )
              }
              <div className="room-detail margin-left">
                <p className="primary-color-popup-text text-font-18">
                  <strong>
                    {id}
                    &nbsp;
                    {name}
                  </strong>
                </p>
                <span className="primary-color-popup-text text-font-16">
                  <img src="dist/img/meeting_space/icons/superficie-white-bg.png" alt="room space" />
                  &nbsp;&nbsp;
                  {room_space}
                </span>
                <span className="primary-color-popup-text margin-left text-font-16">
                  <img src="dist/img/meeting_space/icons/people-icon-white.png" alt="room capacity" width="23px" />
                  &nbsp;&nbsp;
                  {room_capacity}
                </span>
              </div>
            </div>
          </div>
          <div
            className="primary-color-popup pointer primary_color"
            onClick={() => this.handle3DSetup(hoveredRoom)}
          >
            <p><strong>3D set up now</strong></p>
          </div>
          <div className="tooltip-arrow" />
        </div>
      </Grid>
    );
  }
}

FloorPlans.contextTypes = {
  router: PropTypes.object,
};

FloorPlans.propTypes = {
  hotelInfo: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
  handle3DPopup: PropTypes.func.isRequired,
  setFloorPlanLoader: PropTypes.func.isRequired,
};

export default FloorPlans;
