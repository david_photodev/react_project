import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Grid, Row, Col, Button, Alert,
} from 'react-bootstrap';
import { Route, Link, Redirect } from 'react-router-dom';
import ReactSVG from 'react-svg';
import '../../../css/MeetingSpace/MeetingSpace.scss';
import '../../../css/HomePage/HomePage.scss';
import MeetingSpaceAction from '../../actions/MeetingSpaceAction';
import RFPManagementAction from '../../actions/RFPManagementAction';
import MeetingSpaceStore from '../../stores/MeetingSpaceStore';
import AuthStore from '../../stores/AuthStore';
import MeetingSpace from './MeetingSpace';
import MeetingRoomList from './MeetingRoomList';
import LayoutCustomize from '../LayoutCustomize/LayoutCustomize';
import SubmitRFP from '../SubmitRFP/SubmitRFP';
import Tooltip from '../LayoutCustomize/popups/Tooltip';
import LightBox from '../Reusables/LightBox';
import customHistory from '../customHistory';
import Helpers from '../Reusables/Helpers';
import MapBox from '../Reusables/MapBox';
import CommonLayout from '../CommonLayout/CommonLayout';

// const scenes = [
//   {
//     model3d: {
//       id: 1001,
//       fileType: 'gltf',
//       path: 'models/meeting_room/meetingroom.gltf',
//     },
//     model2d: {
//       id: 2001,
//       fileType: 'gltf',
//       path: 'models/meeting_room/floorplan.gltf',
//     },
//   },
// ];

const tabs = [
  {
    id: 'meeting_spaces',
    tab: 1,
    name: 'Meeting Spaces',
    mobile_name: 'Spaces',
    path: '/capacity-chart',
    mobile_class: 'spaces-class',
  },
  {
    id: 'room_setup',
    tab: 2,
    name: '3D Room Set Up',
    mobile_name: '3D Set Up',
    path: '/dollhouse',
    mobile_class: '3d-class',
  },
  {
    id: 'submit_rfp',
    tab: 3,
    name: 'Submit RFP',
    mobile_name: 'Submit RFP',
    path: '/rfp/create',
    mobile_class: 'rfp-class',
  },
];
class HotelPage extends CommonLayout {
  constructor(props) {
    super(props);
    // const hotelInfo = MeetingSpaceStore.getAll();
    this.state = {
      // hotelInfo,
      toPageNavigate: 'meeting_spaces',
      droppedItems: [],
      show2DMiniViewer: true,
      enable2DcloseButton: false,
      zoomRange: 1,
      storageName: 'SceneData',
      // availableSetup: hotelInfo.available_setup,
      tool: 'view',
      isButtonDisable: false,
      isFullScreenBtn: true,
      isFullImage: false,
      dollhouse2Ddisable: false,
      is2DBtnDisable: false,
      showLoader: false,
      readOnlyMode: false,
      isDownload: false,
      mailSuccess: false,
      showModal3dAlert: false,
      typeOfSetup: 'setup',
      typeOfCapacity: 'capacity',
      setMode: '',
      isSharePopup: false,
      showMapModal:true,
      sqmMode:true,
      isClickedSubmitRfp:false,
      userPrimaryColor: null,
      enable3DbtnList: false,
      isGetAllMeetingRooms:false,
      reload:false,
      isUuId: true,
      // isDetection: false,
    };
    this.onMeetingSpaceStoreChange = this.onMeetingSpaceStoreChange.bind(this);
    this.onAuthStoreChange = this.onAuthStoreChange.bind(this);
    this.handleTabClick = this.handleTabClick.bind(this);
    this.onDragOver = this.onDragOver.bind(this);
    this.onDrop = this.onDrop.bind(this);
    this.setViewer = this.setViewer.bind(this);
    this.handle2Dview = this.handle2Dview.bind(this);
    this.handle3Dview = this.handle3Dview.bind(this);
    this.handleTooltipPosition = this.handleTooltipPosition.bind(this);
    this.handleFurnitureAction = this.handleFurnitureAction.bind(this);
    this.handleModelLoading = this.handleModelLoading.bind(this);
    this.handleZoom = this.handleZoom.bind(this);
    this.handleZoom2D = this.handleZoom2D.bind(this);
    this.setZoom = this.setZoom.bind(this);
    this.saveRoom = this.saveRoom.bind(this);
    this.enableTool = this.enableTool.bind(this);
    this.setStorageName = this.setStorageName.bind(this);
    this.handleLayout = this.handleLayout.bind(this);
    this.enableDollHouseView = this.enableDollHouseView.bind(this);
    this.handleDisableButtons = this.handleDisableButtons.bind(this);
    this.enterFullScreen = this.enterFullScreen.bind(this);
    this.escFunction = this.escFunction.bind(this);
    this.showFullImage = this.showFullImage.bind(this);
    this.setLoader = this.setLoader.bind(this);
    this.setReadWriteMode = this.setReadWriteMode.bind(this);
    this.handleDownloadPDF = this.handleDownloadPDF.bind(this);
    this.setSuccessMessage = this.setSuccessMessage.bind(this);
    this.setExpander = this.setExpander.bind(this);
    this.handleDropdown = this.handleDropdown.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleMapClose = this.handleMapClose.bind(this);
    this.handle3DPopup = this.handle3DPopup.bind(this);
    this.setMode = this.setMode.bind(this);
    this.set3dViewmode = this.set3dViewmode.bind(this);
    this.showSharePopup = this.showSharePopup.bind(this);
    this.viewInMap = this.viewInMap.bind(this);
    this.handleFurnitureCount = this.handleFurnitureCount.bind(this);
    this.handleToggle = this.handleToggle.bind(this);
    this.isSubmitRfp = this.isSubmitRfp.bind(this);
    this.setUUId = this.setUUId.bind(this);
    this.resetDroppedItems = this.resetDroppedItems.bind(this);
  }

  componentWillMount() {
    const { router } = this.context;
    const { params } = router.route.match;
    const { location } = router.route;
    const { pathname } = location;
    const { isOnlyView, hotelProperty } = this.props;
    this.resetDroppedItems();
    if (!(pathname.includes('/event/view'))) {
      ModelLoader.clearCache();
      MeetingSpaceAction.restoreData();
    }
    const hotelInfo = MeetingSpaceStore.getAll();
    this.setState({ mailSuccess: false });
    const isLoggedIn = AuthStore.isLoggedIn();
    if (isLoggedIn) {
      this.setState({ isLoggedIn });
      const userInfo = AuthStore.getAll();
      const { data } = userInfo.user;
      this.setState({ userPrimaryColor : data.profile.hotel_chain.primary_color });
    }
    MeetingSpaceStore.addStoreListener(this.onMeetingSpaceStoreChange);
    AuthStore.addStoreListener(this.onAuthStoreChange);
    let property;
    if (params) {
      property = params.property;
    }
    property = isOnlyView ? hotelProperty : property;
    this.setState({ property, hotelInfo });
    const values = ['3d', '2d', 'dollhouse'];
    let checkPath = pathname.split(`/property/${property}`);
    const checkVal = checkPath.length > 1 ? checkPath[1].split('/')[1] : checkPath[0];
    const checkValForProperty = checkVal || '';
    const availableVal = values.find(element => checkValForProperty.includes(element));
    if (pathname.includes('view') && (!pathname.includes('/event/view')) && availableVal) {
      const eventUuId = pathname.split(`${availableVal}/`)[1].split('/view')[0];
      this.context.router.history.push(`/property/${property}/rfp/view/${eventUuId}`);
      this.setState({ eventUUID : eventUuId });
    } else if (pathname.includes('/rfp/view')) {
        let changedHistory =  window.location.href;
        let propertyChange = changedHistory.split('/property/')[1];
        const eventuuIdChange = propertyChange.split('/view/')[1];
        this.setState({ eventUUID: eventuuIdChange });
    } else if ((!pathname.includes('/event/view')) && availableVal) {
      this.setState({ eventUUID : null });
      this.context.router.history.push(`/property/${property}/list`);
    } else {
      this.setState({ eventUUID : null, isUuId: false });
    }

    MeetingSpaceAction.getHotelData(property, hotelInfo.setUpImages, (res) => {
      $('link#font-link').remove();
      $('link#font-link-backoffice').remove();
      let mainTypography = 'Montserrat';
      mainTypography = res.main_typography || mainTypography;
      mainTypography = pathname.includes('/event/view') ? 'Montserrat' : mainTypography;
      const linkCss = `https://fonts.googleapis.com/css?family=${encodeURIComponent(mainTypography)}:400,700`;
      const $css = $(`<link id="font-link" href=${linkCss} rel='stylesheet'/>`);
      $css.appendTo('head');
      $(`<style>body {font-family: ${mainTypography}}</style>`).appendTo('head');
      $(`<style>.daterangepicker .drp-calendar {font-family: ${mainTypography}}</style>`).appendTo('head');
      $( "script#ga-backend-page" ).remove();
      $( "script#ga-backend-page-content" ).remove();
      const { ga_tracking } = hotelInfo.data
      if ((ga_tracking !== 'None') && (ga_tracking !== null)) {
        var content =`window.dataLayer = window.dataLayer || [];
                      function gtag() { dataLayer.push(arguments); }
                      gtag('js', new Date());
                      gtag('config', '${ga_tracking}');`
        var GaHotelPage = document.createElement("script");
        GaHotelPage.src = `https://www.googletagmanager.com/gtag/js?id=${ga_tracking}`;
        GaHotelPage.async = true;
        GaHotelPage.id = "ga-hotel-page";
        var GaHotelPageContent = document.createElement("script");
        GaHotelPageContent.id = "ga-hotel-page-content";
        GaHotelPageContent.text = content;
        $("body").append(GaHotelPage);
        $("body").append(GaHotelPageContent);
      }
      this.setState({ isGetAllMeetingRooms: false });
      MeetingSpaceAction.getMeetingRoomDetails(hotelInfo.setUpImages, property, hotelInfo.data.capacity, (success) => {
        if (success) {
          this.setState({ isGetAllMeetingRooms: true });
          this.setState({ enable3DbtnList: true });
        }
      });
    });
  }

  componentDidMount() {
    const { isFullScreenBtn } = this.state;
    const { isOnlyView } = this.props;
    if (this.context) {
      const path = this.context.router.route.location.pathname;
      let history = window.location.href.split('property')[1];
      history = `/property${history}`;
      this.context.router.history.push(history);
    }
    document.addEventListener('fullscreenchange', this.escFunction);
    document.addEventListener('webkitfullscreenchange', this.escFunction);
    document.addEventListener('mozfullscreenchange', this.escFunction);
    document.addEventListener('MSFullscreenChange', this.escFunction);
    if (/iphone|ipad|iPod/i.test(navigator.userAgent)) {
      const setupBtnFromTop = window.innerHeight - 100;
      $(`<style>.layout-header-tab-container #scene-container
        {height: ${window.innerHeight}px;}</style>`).appendTo('head');
      $(`<style>.room-setup-3d-container .setup-buttons{top: ${setupBtnFromTop}px;}</style>`).appendTo('head');
    }
    if (!isOnlyView) {
      localStorage.clear();
    }
    // Added the 3D container style for the landscape view
    window.addEventListener('orientationchange', () => {
      let setupBtnFromTop;
      if (isFullScreenBtn && /iphone|ipad|iPod/i.test(navigator.userAgent)) {
        if (window.orientation === 90 || window.orientation === -90) {
          setupBtnFromTop = window.innerWidth - 152;
          $(`<style>.layout-header-tab-container #scene-container{height: ${window.innerWidth - 52}px;
            margin-top: 52px;}</style>`).appendTo('head');
          $(`<style>.room-setup-3d-container .setup-buttons{top: ${setupBtnFromTop}px;}</style>`).appendTo('head');
        } else {
          setupBtnFromTop = window.innerWidth - 190;
          $(`<style>.layout-header-tab-container #scene-container{height: ${window.innerWidth - 90}px;
            margin-top: 0px;}</style>`).appendTo('head');
          $(`<style>.room-setup-3d-container .setup-buttons{top: ${setupBtnFromTop}px;}</style>`).appendTo('head');
        }
        viewer.onResize();
      }
    });
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    const { viewer, isFullScreenBtn, mailSuccess, isUuId } = this.state;
    let { eventUUID } = this.state;
    // inform viewer if fullscreen state changes
    if(isFullScreenBtn !== prevState.isFullScreenBtn) {
      if (viewer) {
        viewer.onResize();
      }
    }
    
    let changedHistory =  window.location.href;
    let propertyChange = changedHistory.split('/property/')[1];

    if (propertyChange != null){
      if (propertyChange.includes('view')){
        const eventuuIdChange = propertyChange.split('/view/')[1];
        if (!eventUUID && eventuuIdChange && !isUuId && !(changedHistory.includes('/event/view'))){
          this.setState({ eventUUID : eventuuIdChange });
          MeetingSpaceAction.restoreData();
          this.componentWillMount();
        } else if (eventUUID && eventuuIdChange && eventUUID.toString() !== eventuuIdChange.toString()) {
          this.setState({ eventUUID : eventuuIdChange });
          MeetingSpaceAction.restoreData();
          this.componentWillMount();
        } 
      } else {
        const hotelDetails = MeetingSpaceStore.getAll();
        eventUUID = hotelDetails.eventUuId;
        const eventuuIdChange = null;
        // this.setState({ eventUUID: null });
        propertyChange = propertyChange.split('/')[0];
        const { property } = this.state;
        if (eventUUID && !eventuuIdChange && !(changedHistory.includes('rfp/create'))){
          MeetingSpaceAction.restoreData();
          this.componentWillMount();
        } else if (propertyChange > -1){
          if (propertyChange !== property){
            this.setState({ property : propertyChange, reload: true });
            MeetingSpaceAction.restoreData();
            this.componentWillMount();
          } 
        }
      }
    }
  }

  setUUId() {
    this.setState({ isUuId: true });
  }

  componentWillUnmount() {
    MeetingSpaceStore.removeStoreListener(this.onMeetingSpaceStoreChange);
  }

  onMeetingSpaceStoreChange(action) {
    console.log("onMeetingSpaceStoreChange", arguments);
    const hotelInfo = MeetingSpaceStore.getAll();
    this.setState({ hotelInfo });
    if (action === 'SAVE_HOTEL_DATA') {
      this.loadHotelCss();
    }
  }

  onAuthStoreChange() {
    const isLoggedIn = AuthStore.isLoggedIn();
    this.setState({ isLoggedIn });
  }

  loadHotelCss() {
    const { hotelInfo, userPrimaryColor, isLoggedIn, reload } = this.state;
    let primary_color = userPrimaryColor;
    if ( (reload || userPrimaryColor === null) && (!isLoggedIn) ) {
       primary_color = hotelInfo.data.primary_color;
    } else {
       primary_color = userPrimaryColor;
    }
    this.setState ({ userPrimaryColor : primary_color});
    const mapTip = primary_color.slice(1);
    
    const styles = {
      '.primary-color': {
        color: primary_color
      },
      '.primary-background': {
        'background-color': primary_color + " !important",
      },
      '.primary-color::after, .primary-color::before':{
        'border-color': primary_color,
      },
      '.primary-border': {
        'border-color': primary_color
      },
      '.primary-border-top': {
        'border-top-color': primary_color
      },
      '.primary-border-left': {
        'border-left-color': primary_color + " !important",  // !important is required to override border: 0 set on btn-default,
      },
      '.primary-border-right': {
        'border-right-color': primary_color + " !important",
      },
      '.fill-primary': {
        fill: primary_color
      },
      '.stroke-primary': {
        stroke: primary_color
      },
      '.site-footer': {
        'background-image': `linear-gradient(0deg, ${primary_color} 45%, rgb(185,146,146) 145%)`
      },
      '.daterangepicker .drp-calendar .calendar-table table thead': {
        color: primary_color
      },
      '.daterangepicker .drp-calendar .calendar-table thead th.month': {
        color: primary_color
      },
      '.daterangepicker .drp-calendar .calendar-table table tbody td.active': {
        'background-color': primary_color
      },
      '.rdt input, .event-name input::placeholder': {
        color: primary_color
      },
      'input[type="text"].find-furniture::placeholder': {
        color: primary_color
      },
      '.modal-content': {
        color: primary_color
      },
      '.modal-content .modal-body .modal-action-btns button': {
        'border-color': primary_color
      },
      '.modal-content .modal-body .modal-action-btns .change': {
        'background-color': primary_color
      },
      '.meeting-room-wrapper': {
        'background-image': `linear-gradient(0deg, ${primary_color} 45%,
      rgb(185,146,146) 145%)`
      },
      '.meeting-room-wrapper:before': {
        'background-image': `linear-gradient(0deg, ${primary_color} 45%,
      rgb(185,146,146) 145%)`
      },
      '[type="radio"]:checked + label:after, [type="radio"]:not(:checked) + label:after': {
        background: primary_color
      }
      // '.custom-scrollbar::-webkit-scrollbar-thumb': {
      //   'background-color': primary_color
      // }
    };

    // add drop down arrow image
    const arrowData = 'data:image/svg+xml;base64,'+btoa('<svg xmlns="http://www.w3.org/2000/svg" width="21" height="12" viewBox="0 0 21 12"><g fill="'+primary_color+'" fill-rule="evenodd"><path fill-rule="evenodd" d="M0 .765C0 .663.017.565.051.47A.721.721 0 0 1 .205.219.652.652 0 0 1 .698 0c.192 0 .356.073.493.219l9.38 9.989L19.79.372a.663.663 0 0 1 .503-.219c.198 0 .366.073.503.219A.753.753 0 0 1 21 .907c0 .211-.068.39-.205.536l-9.73 10.338a.652.652 0 0 1-.493.219.652.652 0 0 1-.493-.219L.205 1.29A.803.803 0 0 1 0 .765z"/></g></svg>');
    styles['.filter-container select'] = {
      'background': 'url(\''+arrowData+'\') no-repeat #fcfcfc;',
      'background-position-x': '133px',
      'background-position-y': '12px'
    };
    // styles['.filter-container select'] = {
    //   'background': 'url(/dist/img/meeting_space/icons/arrow-down.png) no-repeat #fcfcfc;'
    // };
    let styles_entries = [];
    for (let selector in styles) {
      if (styles.hasOwnProperty(selector)) {
        let style = styles[selector];
        let s_attrs = [];
        for(let attr in style) {
          if (style.hasOwnProperty(attr)) {
            s_attrs.push(`${attr}: ${style[attr]};`);
          }
        }
        const s_attrs_txt = s_attrs.join("\n");
        styles_entries.push(`${selector} {\n${s_attrs_txt}\n}`);
      }
    }

    let $css = $('style#hotel-style');
    if ( ! $css.length) {
      $css = $("<style id='hotel-style'/>");
    }
    $css.text(styles_entries.join("\n"));
    $css.appendTo('head');
  }

  onDragOver(e) {
    e.preventDefault();
  }

  resetDroppedItems() {
    this.setState({ droppedItems: [] });
  }

  onDrop(e) {
    const { viewer, hotelInfo } = this.state;
    const { layout } = hotelInfo;
    const { droppedItems } = layout;
    e.preventDefault();
    const eventData = e.dataTransfer.getData('data');
    if ( ! eventData) {
      return;
    }
    let droppingItem = JSON.parse(eventData);

    let loc = null;
    if (e.touches && event.touches.length > 0) {
      loc = {
        pageX: e.touches[ 0 ].pageX,
        pageY: e.touches[ 0 ].pageY,
      }
    } else {
      loc = {
        pageX: e.pageX,
        pageY: e.pageY,
      }
    }
    const params = {
      loc: loc
    };
    const modelData = Object.assign(droppingItem, params);
    modelData['auto_placement'] = true;
    console.log('modelData', modelData);
    this.setState({ showLoader: true });
    viewer.addItems(modelData, (mData, error) => {
      if (error) {
        console.error("Could not add item", mData, error);
        this.setState({ showLoader: false });
        return;
      }
      const [alreadyDroppedItem] = droppedItems.filter(addedItem => addedItem.id === droppingItem.id);
      if (alreadyDroppedItem) {
        alreadyDroppedItem['count'] += droppingItem['count']
      } else {
        droppedItems.push(droppingItem);
      }
      this.setState({ droppedItems });
      MeetingSpaceAction.saveDroppedItems(droppedItems);
      this.saveRoom();
    });
  }

  setViewer(viewer) {
    this.setState({ viewer });
  }

  setZoom(value) {
    this.setState({ zoomRange: value });
  }

  setLoader(bool) {
    this.setState({ showLoader: bool });
    // const target = document.getElementById('room_setup_3d');
    //   if (target !== null) {
    //     target.scrollIntoView({ block: 'start', behavior: 'smooth' });
    //   }
  }

  setStorageName(name) {
    this.setState({ storageName: name });
  }

  handleZoom(e) {
    const value = parseInt(e.target.value, 10);
    viewer.setZoom2D(value);
    this.setState({ zoomRange: value });
  }

  handleZoom2D(type) {
    const { viewer } = this.state;
    let { zoomRange } = this.state;
    if (type === 'increase' && zoomRange < 100) {
      zoomRange += 1;
      viewer.setZoom2D(zoomRange);
    } else if (type === 'decrease' && zoomRange > 0) {
      zoomRange -= 1;
      viewer.setZoom2D(zoomRange);
    }
    this.setState({ zoomRange });
  }

  goBack() {
    const { toPageNavigate } = this.state;
    let pageIndex;
    tabs.forEach((page, i) => {
      if (page.id === toPageNavigate && i > 0 && i <= 2) {
        pageIndex = tabs[i - 1].id;
        // this.handleDispose(pageIndex);
        this.setState({ toPageNavigate: pageIndex, isTooltip: false });
      }
    });
  }

  goForward() {
    const { toPageNavigate } = this.state;
    let pageIndex;
    tabs.forEach((page, i) => {
      if (page.id === toPageNavigate && i >= 0 && i < 2) {
        pageIndex = tabs[i + 1].id;
        // this.handleDispose(pageIndex);
        this.setState({ toPageNavigate: pageIndex, isTooltip: false });
      }
    });
  }

  handleDispose(toPageNavigate) {
    const { viewer } = this.state;
    if (toPageNavigate !== 'room_setup' && viewer) {
      viewer.dispose();
      MeetingSpaceAction.setViewer(null);
      this.setViewer(null);
      this.setState({ viewer: null });
    }
  }

  handleTabClick(id) {
    this.handleDispose(id);
    this.set3dViewmode(false);
    if (id === 'room_setup') {
      this.enableDollHouseView();
      this.set3dViewmode(true);
    }
    if (id === 'meeting_spaces') {
      this.isSubmitRfp(false)
    }
    this.setState({
      toPageNavigate: id,
      isTooltip: false,
      show2DMiniViewer: true,
      enable2DcloseButton: false,
    });
  }

  handle2Dview() {
    const { viewer, hotelInfo, property } = this.state;
    viewer.ViewIn2D();
    viewer.unselectItem();
    // if (hotelInfo.eventId) {
    //   this.context.router.history.push(`/${property}/2d/${eventId}/view`);
    // } else {
    //   this.context.router.history.push(`/${property}/2d`);
    // }
    this.setState({
      show2DMiniViewer: false,
      enable2DcloseButton: true,
      isTooltip: false,
      tool: 'view',
      view: '2D',
      dollhouse2Ddisable: false,
    });
  }

  handle3Dview() {
    const {
      viewer, view, hotelInfo, property,
    } = this.state;
    // if (hotelInfo.eventId) {
    //   customHistory.push(`#/property/1/3d/${hotelInfo.eventId}/view`);
    // } else {
    //   customHistory.push('#/property/1/3d');
    // }
    // if (hotelInfo.eventId) {
    //   this.context.router.history.push(`/${property}/3d/${eventId}/view`);
    // } else {
    //   this.context.router.history.push(`/${property}/3d`);
    // }
    this.setState({
      show2DMiniViewer: true,
      enable2DcloseButton: false,
      isTooltip: false,
      dollhouse2Ddisable: false,
      is2DBtnDisable: view && view === 'dollhouse',
      view: '3D',
    });
    if (viewer) {
      viewer.ViewIn3D(() => this.on3Dview());
      viewer.unselectItem();
    }
  }

  handleTooltipPosition(isTooltip, tooltipPosition) {
    let { isFinishingPopup } = this.state;
    // if (isTooltip) {
    //   isFinishingPopup = false;
    // }
    this.setState({ isTooltip, tooltipPosition, isFinishingPopup });
  }

  handleFurnitureAction(action) {
    this.setState({ isFinishingPopup: (action && action === 'finishing') || false });
  }

  handleModelLoading(bool) {
    this.setState({ isModelLoaded: bool });
  }

  saveRoom() {
    const { viewer, storageName } = this.state;
    this.setState({ showLoader: false });
    const data = viewer.toJSON();
    DataProvider.storeData(storageName, data);
    RFPManagementAction.active3DEdit(true);
  }

  enableTool(action) {
    const { viewer } = this.state;
    this.setState({ tool: action });
    viewer.setTool(action);
  }

  handleLayout(bool) {
    this.setState({ enable2DcloseButton: bool });
  }

  enableDollHouseView() {
    const { hotelInfo, property } = this.state;
    const { viewer } = hotelInfo;
    // if (hotelInfo.eventId) {
    //   this.context.router.history.push(`/${property}/dollhouse/${eventId}/view`);
    // } else {
    //   this.context.router.history.push(`/${property}/dollhouse`);
    // }
    this.setState({
      show2DMiniViewer: true,
      enable2DcloseButton: false,
      isTooltip: false,
      view: 'dollhouse',
      dollhouse2Ddisable: true,
    });
    if (viewer) {
      viewer.viewInDollhouse(() => this.handleDollhouse());
      viewer.unselectItem();
    }
  }

  handleDollhouse() {
    this.setState({ dollhouse2Ddisable: false });
  }

  on3Dview() {
    this.setState({ is2DBtnDisable: false });
  }

  handleDisableButtons(type) {
    const isButtonDisable = type === 'disable';
    this.setState({ isButtonDisable });
  }

  viewInMap() {
    this.setState({ viewMap: true });
    this.setState({ showMapModal: true });
  }

  enterFullScreen(enter) {
    const { isFullScreenBtn } = this.state;
    const domElement = document.getElementById('room_setup_3d');
    let fsReqFunc = null;
    if (document.documentElement.requestFullscreen) {
      fsReqFunc = 'requestFullscreen';
    } else if (document.documentElement.mozRequestFullScreen) { /* Firefox */
      fsReqFunc = 'mozRequestFullScreen';
    } else if (document.documentElement.webkitRequestFullscreen) { /* Chrome, Safari and Opera */
      fsReqFunc = 'webkitRequestFullscreen';
    } else if (document.documentElement.msRequestFullscreen) { /* IE/Edge */
      fsReqFunc = 'msRequestFullscreen';
    } else if (document.documentElement.webkitEnterFullScreen) { /* IE/Edge */
      fsReqFunc = 'webkitEnterFullScreen';
    }

    let fsExitFunc = null;
    if (document.exitFullscreen) {
      fsExitFunc = 'exitFullscreen';
    } else if (document.mozCancelFullScreen) { /* Firefox */
      fsExitFunc = 'mozCancelFullScreen';
    } else if (document.webkitExitFullscreen) { /* Chrome, Safari and Opera */
      fsExitFunc = 'webkitExitFullscreen';
    } else if (document.msExitFullscreen) { /* IE/Edge */
      fsExitFunc = 'msExitFullscreen';
    }
    if (fsReqFunc && fsExitFunc) {
      if (enter) {
        domElement[fsReqFunc]();
      } else if (document.fullscreenElement) {
        document[fsExitFunc]();
      } else {
        document[fsExitFunc]();
      }
    } else {
      this.setState({ isFullScreenBtn: !isFullScreenBtn });
      // const target = document.getElementById('room_setup_3d');
      // if (target !== null) {
      //   target.scrollIntoView({ block: 'start', behavior: 'smooth' });
      // }
    }
  }

  escFunction() {
    const { isFullScreenBtn } = this.state;
    this.setState({ isFullScreenBtn: !isFullScreenBtn });
    viewer.onResize();
    // const target = document.getElementById('room_setup_3d');
    // if (target !== null) {
    //   target.scrollIntoView({ block: 'start', behavior: 'smooth' });
    // }
  }

  showFullImage(item, type) {
    this.setState({ isFullImage: type === 'open' });
    if (type === 'open') {
      MeetingSpaceAction.showSetupView(item);
    }
  }

  closeAlert(msg) {
    MeetingSpaceAction.notificationMsg(msg, 'remove', 'success');
  }

  setReadWriteMode(bool) {
    this.setState({
      readOnlyMode: bool,
    });
  }

  showSharePopup(bool) {
    this.setState({
      isSharePopup: bool,
    });
  }

  handleDownloadPDF(type) {
    if (type === 'download') {
      this.setExpander(true);
      const container = document.getElementsByClassName('layout-header-tab-container')[0];
      Helpers.downloadAsPDF(container, 'RFP.pdf', (() => { this.setExpander(false); }));
    }
  }

  setSuccessMessage(bool) {
    this.setState({ mailSuccess: bool });
    const { hotelInfo, property } = this.state;
    const { router } = this.context;
    const { match } = router.route;
    if (bool) {
      const rfpElement = document.getElementById('rfp-confirmation-msg-container');
      if (rfpElement !== null) {
        rfpElement.scrollIntoView({ block: 'start' });
      }
      this.setExpander(false);
      router.history.push(`${match.url}/rfp/view/${hotelInfo.eventUuId}`);
      // customHistory.push(`/#/${property}/rfp/view/${hotelInfo.eventId}`);
    }
  }

  setExpander(bool) {
    this.setState({ isDownload: bool });
  }

  handleDropdown(type, e) {
    let { typeOfSetup, typeOfCapacity } = this.state;
    typeOfSetup = typeOfSetup === 'setup' ? 'all' : typeOfSetup;
    typeOfCapacity = typeOfCapacity === 'capacity' ? 'all' : typeOfCapacity;
    if (type === 'setup') {
      this.setState({ typeOfSetup: e.target.value });
      MeetingSpaceAction.filterSelectedRoom(e.target.value, typeOfCapacity);
    } else {
      this.setState({ typeOfCapacity: e.target.value });
      MeetingSpaceAction.filterSelectedRoom(typeOfSetup, e.target.value);
    }
  }

  handle3DPopup(bool) {
    this.setState({ showModal3dAlert: bool });
  }

  handleClose() {
    this.setState({ showModal3dAlert: false });
  }

  handleMapClose() {
    this.setState({
      showMapModal: false,
      viewMap: false
    });
  }

  setMode(type) {
    this.setState({ setMode: type });
  }

  set3dViewmode(bool) {
    this.setState({ set3dViewmode: bool });
  }

  handleFurnitureCount(event) {
    const peopleCount = event.data.noOfChairs;
    const tablesCount = event.data.noOfTables;
    const { layout, viewLayout } = this.state.hotelInfo;
    const params = {
      roomId: viewLayout.id,
      setupId: layout.setup_id,
      peopleCount,
      tablesCount,
    };
    MeetingSpaceAction.updateLayoutInputs(params);
  }
  
  handleToggle(e){
    const { sqmMode } = this.state;
    if (e.target.checked) {
      // Checked means "SQFT" mode
      this.setState({ sqmMode: false });
    } else {
      this.setState({ sqmMode: true });
    }
  }

  isSubmitRfp(isClickedSubmitRfpBtn){
    const { isClickedSubmitRfp } = this.state;
    this.setState({ isClickedSubmitRfp: isClickedSubmitRfpBtn});

  }

  // autoPlacement(e) {
  //   const isDetection = e.target.checked;
  //   this.setState({ isDetection });
  // }

  render() {
    const {
      hotelInfo, droppedItems, show2DMiniViewer, isTooltip, tooltipPosition, viewer, isModelLoaded,
      enable2DcloseButton, isFinishingPopup, zoomRange, tool, storageName, availableSetup, isButtonDisable,
      isFullScreenBtn, isFullImage, dollhouse2Ddisable, is2DBtnDisable, showLoader, mailSuccess,
      typeOfSetup, typeOfCapacity, showModal3dAlert, setMode, set3dViewmode, property, viewMap, showMapModal, sqmMode,
      isClickedSubmitRfp, isLoggedIn, isRedirect, enable3DbtnList, isMobileSidebar, isGetAllMeetingRooms, userPrimaryColor,
      isFirstPage, reload,
    } = this.state;
    let { isDownload } = this.state;
    const { isOnlyView, hide3DView } = this.props;
    isDownload = isOnlyView && hide3DView ? hide3DView : isDownload; 
    const { eventId, eventUuId, selectedRooms } = hotelInfo;
    let { toPageNavigate, readOnlyMode, isSharePopup } = this.state;
    const { router } = this.context;
    const { match } = router.route;
    const context = router.route.location.pathname;
    const path = window.location.href;
    if (context === `${match.url}/list` || context === `${match.url}`) {
      toPageNavigate = 'meeting_spaces';
    } else if (context === `${match.url}/rfp` || context === `${match.url}/rfp/create`) {
      toPageNavigate = 'submit_rfp';
    } else if (context.includes('/rfp/view')) {
      toPageNavigate = 'submit_rfp';
    } else if (path.includes(`/dollhouse/${hotelInfo.eventUuId}/view`)) {
      toPageNavigate = 'room_setup';
    }

    if (context === '/dollhouse' || path.includes('/dollhouse')) {
      toPageNavigate = 'room_setup';
    }
    if (path.includes('/2d') || path.includes('/3d')) {
      toPageNavigate = 'room_setup';
    }
    if (set3dViewmode) {
      toPageNavigate = 'room_setup';
    }
    if (path.includes('/view')) {
      readOnlyMode = true;
    }
    if (isOnlyView) {
      toPageNavigate = 'room_setup';
      readOnlyMode = false;
    }

    if (path.includes('/list')) {
      toPageNavigate = 'meeting_spaces';
      readOnlyMode = false;
    }
    
    const roomSetupDisabled = toPageNavigate !== 'room_setup';
    const is2DView = enable2DcloseButton ? 'gray-bg-btn' : '';
    const modelAlertMessage = 'This meeting room is not yet available for 3D diagramming.';
    const hotelInfoData = Object.keys(hotelInfo.data).length;
    if (isRedirect) {
      return <Redirect to="/" />;
    }
    return (
      <div>
        {isLoggedIn && !isOnlyView ? this.sideBar(userPrimaryColor, true) : ''}
        {
          isMobileSidebar && (
            this.mobileSideBar()
          )
        }
        <div className={`${isLoggedIn && !isOnlyView ? 'content-right align-content-right' : ''}`}>
          {
            hotelInfoData >= 1 ? (
              <Grid className="primary-color layout-header-tab-container">
                <progress id="scene-load-progress" value="0" max="100" style={toPageNavigate === 'room_setup' ? {display : 'block'} : {display: 'none'}}/>
                {
                  isLoggedIn && !isOnlyView ? this.pageHeader(false, false, userPrimaryColor) : ''
                }
                {
                  ((!readOnlyMode && hotelInfo.notificationMsg.length >= 1 && !isDownload) || isSharePopup)
                    && (
                      hotelInfo.notificationMsg.map((msg, i) => (
                        <Alert key={`notification-msg${i}`} style={{ top: `${(i * 50) + 10}px` }} className={`alert-msg-${i} ${msg.includes('Please upload') ? 'alert-msg-upload-documents' : ''}`}>
                          {msg}
                          <span
                            className="fa fa-times lmargin20 close-alert pointer"
                            onClick={() => this.closeAlert(msg)}
                            onKeyUp={this.handleKeyUp}
                          />
                        </Alert>
                      )))
                }
                {
                  !isOnlyView && (
                    <div>
                    <Row
                      className={`header-container ${!isFullScreenBtn ? 'hidden-xs hidden-sm' : ''}`}
                      style={(readOnlyMode || isDownload) ? { marginBottom: '20px' } : {}}
                    >
                      <Col lg={3} md={3} className="header-logo hidden-xs">
                        <img src={hotelInfo.data.hotel_logo} width="164px" alt="hotel logo" />
                      </Col>
                      {
                        ([
                          <Col xs={12} lg={6} md={6} sm={6} className="header-content">
                            <p className="hotel-name primary-color text-bold hidden-xs">{hotelInfo.data.hotel_name}</p>
                            <p className="hotel-name primary-color text-bold visible-xs">{hotelInfo.data.hotel_name}</p>
                            <p className="primary-color hidden-xs">{hotelInfo.data.address}</p>
                            <p className="primary-color hidden-xs">{`${hotelInfo.data.phone || ''}
                              ${hotelInfo.data.phone ? '|' : ''}
                              ${hotelInfo.data.email_address || ''}`}</p>
                            <Button className="localization-button" onClick={() => this.viewInMap()}>
                              <ReactSVG wrapper="span" className="svg-wrapper" src="dist/img/red-location.svg" alt="localization" />
                              <span className="primary-color i-block">See Location</span>
                            </Button>
                            <hr className="visible-xs separator-line" />
                          </Col>,
                          <Col lg={3} md={3} sm={6} xs={12}>
                            <div className="header-capacity">
                              <div>
                                <ReactSVG wrapper="span" className="svg-wrapper" src="dist/img/meeting_space/icons/meeting-icon.svg" alt="meeting icon" />
                                <span className="primary-color hidden-xs">Meeting rooms</span>
                                <span className="primary-color visible-xs inline-block lmargin10">{hotelInfo.totalMeetingRooms} Meeting rooms</span>
                              </div>
                              <p className="primary-color text-bold hidden-xs">{hotelInfo.totalMeetingRooms} multifunctional rooms</p>
                              <div>
                                <ReactSVG wrapper="span" className="svg-wrapper" src="dist/img/meeting_space/icons/people-icon.svg" alt="people icon" />
                                <span className="primary-color hidden-xs">Capacity</span>
                                <span className="primary-color visible-xs inline-block tmargin10 lmargin10">Capacity: {hotelInfo.capacity} Guests</span>
                              </div>
                              <p className="primary-color hidden-xs text-bold">{hotelInfo.capacity} Guests</p>
                            </div>
                          </Col>
                        ])
                      }
                    </Row>
                    <Row className={`tab-container ${(readOnlyMode || isDownload) ? 'hide' : ''}
                      ${!isFullScreenBtn ? 'hidden-xs hidden-sm' : ''}`}
                    >
                      <div id="layout_tab" className="tmargin50">
                        <ul className={`nav nav-pills ${toPageNavigate}`}>
                          {
                            tabs.map(({ id, tab, name, mobile_name, path, mobile_class }) => (
                              <Col lg={3} md={3} sm={3} xs={3} className={`individual-tab ${id}`} key={id}>
                                <div 
                                  className={`tab-number tab-number-alignment primary-background ${mobile_class}`}
                                  >{tab}</div>
                                {
                                  roomSetupDisabled && id === 'room_setup' ? (
                                    <Button
                                      className={`i-block primary-color ${mobile_class}`}
                                      onClick={() => this.handleTabClick(id)}
                                      disabled={roomSetupDisabled && id === 'room_setup'}
                                    >
                                      <li className="primary-color text-left">
                                        <span className="i-block text-sub-tab hidden-xs">{name}</span>
                                        <span className="i-block visible-xs">{mobile_name}</span>
                                      </li>
                                    </Button>
                                  ) : (
                                    <Link to={`${match.url}${path}`}>
                                      <Button
                                        className={`i-block primary-color ${mobile_class}`}
                                        onClick={() => this.handleTabClick(id)}
                                        disabled={roomSetupDisabled && id === 'room_setup'}
                                      >
                                        <li className="primary-color text-left">
                                          <span className="i-block text-sub-tab hidden-xs">{name}</span>
                                          <span className="i-block visible-xs">{mobile_name}</span>
                                        </li>
                                      </Button>
                                      </Link>
                                    )
                                  }
                                </Col>
                              ))
                            }
                            <Col lg={3} md={3} sm={3} xs={3} className="individual-tab-toggle primary-color" />
                          </ul>
                        </div>
                      </Row>
                    <div
                      className={`meeting-room-info-container ${!isFullScreenBtn ? 'hidden-xs hidden-sm' : ''}`}
                      id="rfp-confirmation-msg-container"
                    >
                      {
                        <MeetingRoomList
                          hotelInfo={hotelInfo}
                          setReadWriteMode={this.setReadWriteMode}
                          showSharePopup={this.showSharePopup}
                          readOnlyMode={readOnlyMode}
                          isSharePopup={isSharePopup}
                          handleTabClick={this.handleTabClick}
                          viewer={viewer}
                          handleModelLoading={this.handleModelLoading}
                          toPageNavigate={toPageNavigate}
                          setStorageName={this.setStorageName}
                          handleLayout={this.handleLayout}
                          setLoader={this.setLoader}
                          handleDownloadPDF={this.handleDownloadPDF}
                          isDownload={isDownload}
                          mailSuccess={mailSuccess}
                          handle3DPopup={this.handle3DPopup}
                          setViewer={this.setViewer}
                          handleTooltipPosition={this.handleTooltipPosition}
                          setZoom={this.setZoom}
                          saveRoom={this.saveRoom}
                          handleDisableButtons={this.handleDisableButtons}
                          context={context}
                          setMode={this.setMode}
                          set3dViewmode={this.set3dViewmode}
                          property={property}
                          setSuccessMessage={this.setSuccessMessage}
                          handleFurnitureCount={this.handleFurnitureCount}
                          isSubmitRfp={this.isSubmitRfp}
                          isClickedSubmitRfp={isClickedSubmitRfp}
                          enable3DbtnList={enable3DbtnList}
                          setExpander={this.setExpander}
                          resetDroppedItems={this.resetDroppedItems}
                          userPrimaryColor={userPrimaryColor}
                        />
                      }
                      {
                        toPageNavigate === 'meeting_spaces'
                          && (
                            <Route
                              path={`${match.path}`}
                              render={() => (
                                <MeetingSpace
                                  hotelInfo={hotelInfo}
                                  isFullImage={isFullImage}
                                  sqmMode={sqmMode}
                                  showFullImage={this.showFullImage}
                                  toPageNavigate={toPageNavigate}
                                  handleTabClick={this.handleTabClick}
                                  handleDropdown={this.handleDropdown}
                                  typeOfSetup={typeOfSetup}
                                  handleToggle={this.handleToggle}
                                  typeOfCapacity={typeOfCapacity}
                                  handle3DPopup={this.handle3DPopup}
                                  context={context}
                                  property={property}
                                  setLoader={this.setLoader}
                                  isGetAllMeetingRooms={isGetAllMeetingRooms}
                                  userPrimaryColor={userPrimaryColor}
                                />
                              )}
                            />
                          )
                      }
                      {
                        toPageNavigate === 'submit_rfp' && (
                          <Route
                            path={`${match.path}/rfp/create`}
                            render={() => (
                              <SubmitRFP
                                hotelInfo={hotelInfo}
                                readOnlyMode={readOnlyMode}
                                setReadWriteMode={this.setReadWriteMode}
                                handleDownloadPDF={this.handleDownloadPDF}
                                setLoader={this.setLoader}
                                setExpander={this.setExpander}
                                setSuccessMessage={this.setSuccessMessage}
                                setMode={setMode}
                                isDownload={isDownload}
                                isSubmitRfp={this.isSubmitRfp}
                                property={property}
                                isClickedSubmitRfp={isClickedSubmitRfp}
                                userPrimaryColor={userPrimaryColor}
                                setUUId={this.setUUId}
                              />
                            )}
                          />
                        )
                      }
                      {
                        toPageNavigate === 'submit_rfp' && (
                          <Route
                            path={`${match.path}/rfp/view/${eventUuId}`}
                            render={() => (
                              <SubmitRFP
                                hotelInfo={hotelInfo}
                                readOnlyMode={readOnlyMode}
                                setReadWriteMode={this.setReadWriteMode}
                                handleDownloadPDF={this.handleDownloadPDF}
                                setLoader={this.setLoader}
                                setExpander={this.setExpander}
                                setSuccessMessage={this.setSuccessMessage}
                                setMode={setMode}
                                isDownload={isDownload}
                                property={property}
                                userPrimaryColor={userPrimaryColor}
                                setUUId={this.setUUId}
                              />
                            )}
                          />
                        )
                      }
                    </div>
                    </div>
                  )
                }
              {
                toPageNavigate === 'room_setup'
                && (
                  // <Route
                  //   path={`${match.path}/rfp/view/${eventUuId}`}
                  //   render={() => (
                  <div className="room-setup-container">
                    <div
                      className="room-setup-3d-container"
                      id="room_setup_3d"
                      style={isFullScreenBtn ? {} : { maxWidth: '100%' }}
                    >
                    {
                      isFullScreenBtn && !showLoader && !isDownload && (
                        <div className="loader-3d-img-overlay loader-img-overlay pointer visible-xs visible-sm">
                          <div
                            className="loader-img primary-background"
                            onClick={() => this.enterFullScreen(isFullScreenBtn)}
                            onKeyUp={this.handleKeyUp}
                          >
                            <img src="dist/img/full_screen.png" alt="full_screen" />
                            <span>Enter interactive 3D</span>
                          </div>
                        </div>
                      )
                    }
                      <div
                        className={isDownload ? 'hide' : 'block'}
                        id="scene-container"
                        onDragOver={e => this.onDragOver(e)}
                        onDrop={e => this.onDrop(e, 'complete')}
                      />
                      {
                        isModelLoaded && !isDownload
                          && (
                          <Button
                            id="full-view-btn"
                            className={`${isFullScreenBtn ? 'hidden-xs hidden-sm' : ''}
                              ${enable2DcloseButton ? 'blact-text' : 'white-text-btn'}`}
                            onClick={() => this.enterFullScreen(isFullScreenBtn)}
                          >
                            {
                              isFullScreenBtn
                                ? (
                                  <img
                                    src={`dist/img/${enable2DcloseButton ? 'full-screen-black.png' : 'full_screen.png'}`}
                                    alt="full_screen"
                                  />
                                ) : (
                                  <span className="close-icon">&#x2715;</span>
                                )
                            }
                            <span className={`i-block hidden-xs ${!show2DMiniViewer ? 'primary-color' : ''}`}>
                              {isFullScreenBtn ? 'Change to full screen to work easily' : 'Exit Full Screen'}
                            </span>
                          </Button>
                          )
                      }
                      {
                        enable2DcloseButton && isModelLoaded && !isDownload
                          && (
                            <div className={`${isFullScreenBtn ? 'hidden-xs hidden-sm' : ''}`}>
                              <div className={`zoom-slider-container ${!isFullScreenBtn ? 'top-position' : ''}`}>
                                <div className="zoom-plus-container">
                                  <Button
                                    className="zoom-plus"
                                    onClick={() => this.handleZoom2D('increase')}
                                  >
                                    <i className="fa fa-plus" />
                                  </Button>
                                </div>
                                <div className="zoom-slider">
                                  <input
                                    // className="zoom-slider"
                                    value={zoomRange}
                                    type="range"
                                    min="0"
                                    max="100"
                                    onChange={this.handleZoom}
                                  />
                                </div>
                                <div className="zoom-minus-container">
                                  <Button
                                    className="zoom-minus"
                                    onClick={() => this.handleZoom2D('decrease')}
                                  >
                                    <i className="fa fa-minus" />
                                  </Button>
                                </div>
                                <div className="selection-button-container">
                                  <div className="">
                                    <Button
                                      className={`drag ${tool === 'view' ? 'active' : ''}`}
                                      onClick={() => this.enableTool('view')}
                                    >
                                      <img src="dist/img/mouse-drag.png" alt="view" />
                                    </Button>
                                  </div>
                                  <div className="">
                                    <Button
                                      className={`ruler ${tool === 'ruler' ? 'active' : ''}`}
                                      onClick={() => this.enableTool('ruler')}
                                    >
                                      <img src="dist/img/ruler.png" alt="ruler" />
                                    </Button>
                                  </div>
                                  <div className="">
                                    <Button
                                      className={`select ${tool === 'select' ? 'active' : ''}`}
                                      onClick={() => this.enableTool('select')}
                                    >
                                      <img src="dist/img/select.png" alt="select" />
                                    </Button>
                                  </div>
                                </div>
                              </div>
                              <Button className="primary-color close-floor-button" onClick={this.handle3Dview}>
                                <span className="close-icon">&#x2715;</span>
                              </Button>
                              {
                                // <Button id="scene-3d-btn" onClick={this.handle3Dview} className="gray-bg-btn">
                                //                       <img src="dist/img/meeting_space/icons/3d-icon.png" alt="3d viewer" />
                                //                     </Button>
                                                  }
                            </div>
                          )
                      }
                      <div className={`setup-buttons ${isFullScreenBtn ? 'hidden-xs hidden-sm' : ''}`}>
                      {
                        isModelLoaded && !isOnlyView && !isDownload
                          && ([
                            <Link to={eventUuId ? `${match.url}/2d/${eventUuId}/view` : `${match.url}/2d`}>
                              <Button
                                id="scene-2d-container"
                                onClick={this.handle2Dview}
                                className={is2DView}
                                disabled={dollhouse2Ddisable || is2DBtnDisable}
                                style={{ opacity: this.state.view != '2D' ?  0.4  : ''}}
                              >
                                <img src="dist/img/Room-plan-white.png" alt="2dMiniViewer" style={{ maxWidth: 30 }}/>
                              </Button>
                            </Link>,
                            <Link to={eventUuId ? `${match.url}/3d/${eventUuId}/view` : `${match.url}/3d`}>
                              <Button
                                id="scene-3d-btn"
                                onClick={this.handle3Dview}
                                className={is2DView}
                                disabled={dollhouse2Ddisable}
                              >
                                <img src="dist/img/meeting_space/icons/3d-icon.png" alt="3d viewer" style={{ opacity: this.state.view != '3D' ? 0.4 : ''}}/>
                              </Button>
                            </Link>,
                            <Link to={eventUuId ? `${match.url}/dollhouse/${eventUuId}/view/` : `${match.url}/dollhouse`}>
                              <Button
                                id="dollhouse-view-btn"
                                onClick={this.enableDollHouseView}
                                className={is2DView}
                                disabled={is2DBtnDisable}
                              >
                                <img src="dist/img/Dollhouse-icon.png" alt="Dollhouse" style={{ opacity: this.state.view == undefined || this.state.view == 'dollhouse' ?  ''  : 0.4 }} className={this.state.view}/>
                              </Button>

                            </Link>,
                          ])
                      }
                      {
                        isModelLoaded && isOnlyView && !isDownload
                          && ([
                              <Button
                                id="scene-2d-container"
                                onClick={this.handle2Dview}
                                className={is2DView}
                                disabled={dollhouse2Ddisable || is2DBtnDisable}
                                style={{ opacity: this.state.view != '2D' ?  0.4  : ''}}
                              >
                                <img src="dist/img/Room-plan-white.png" alt="2dMiniViewer"/>
                              </Button>,
                              <Button
                                id="scene-3d-btn"
                                onClick={this.handle3Dview}
                                className={is2DView}
                                disabled={dollhouse2Ddisable}
                              >
                                <img src="dist/img/meeting_space/icons/3d-icon.png" alt="3d viewer" style={{ opacity: this.state.view != '3D' ? 0.4 : ''}}/>
                              </Button>,
                              <Button
                                id="dollhouse-view-btn"
                                onClick={this.enableDollHouseView}
                                className={is2DView}
                                disabled={is2DBtnDisable}
                              >
                                  <img src="dist/img/Dollhouse-icon.png" alt="Dollhouse" style={{ opacity: this.state.view == undefined || this.state.view == 'dollhouse' ?  ''  : 0.4 }} className={this.state.view}/>
                                />
                              </Button>
                          ])
                      }
                      </div>
                      {
                        isTooltip
                          && (
                            <Tooltip
                              handleFurnitureAction={this.handleFurnitureAction}
                              tooltipPosition={tooltipPosition}
                              handleTooltipPosition={this.handleTooltipPosition}
                              isTooltip={isTooltip}
                              viewer={viewer}
                              saveRoom={this.saveRoom}
                              show2DMiniViewer={show2DMiniViewer}
                              isButtonDisable={isButtonDisable}
                            />)
                      }
                      <LayoutCustomize
                        setViewer={this.setViewer}
                        droppedItems={droppedItems}
                        handleTooltipPosition={this.handleTooltipPosition}
                        isFinishingPopup={isFinishingPopup}
                        isTooltip={isTooltip}
                        handleFurnitureAction={this.handleFurnitureAction}
                        handleModelLoading={this.handleModelLoading}
                        show2DMiniViewer={show2DMiniViewer}
                        setZoom={this.setZoom}
                        saveRoom={this.saveRoom}
                        hotelInfo={hotelInfo}
                        storageName={storageName}
                        handleDisableButtons={this.handleDisableButtons}
                        setStorageName={this.setStorageName}
                        setLoader={this.setLoader}
                        showLoader={showLoader}
                        readOnlyMode={readOnlyMode}
                        handleFurnitureCount={this.handleFurnitureCount}
                        isFullScreenBtn={isFullScreenBtn}
                        handle3DPopup={this.handle3DPopup}
                        isOnlyView={isOnlyView}
                        isDownload={isDownload}
                        property={property}
                        userPrimaryColor={userPrimaryColor}
                        resetDroppedItems={this.resetDroppedItems}
                      />
                      {
                        path.includes('/view') && !isOnlyView && (
                          <SubmitRFP
                            hotelInfo={hotelInfo}
                            readOnlyMode={readOnlyMode}
                            setReadWriteMode={this.setReadWriteMode}
                            handleDownloadPDF={this.handleDownloadPDF}
                            setLoader={this.setLoader}
                            setExpander={this.setExpander}
                            setSuccessMessage={this.setSuccessMessage}
                            setMode={setMode}
                            isDownload={isDownload}
                            property={property}
                            toPageNavigate={toPageNavigate}
                            userPrimaryColor={userPrimaryColor}
                          />
                        )
                      }
                    </div>
                  </div>
                  //   )}
                  // />
                )
              }
              {
                showLoader && (
                  <div className="loader-img-overlay">
                    <div className="loader-img">
                      <img src="dist/img/loader.gif" alt="loading" />
                    </div>
                  </div>
                )
              }
              {
                isFullImage && !isOnlyView && (
                  <div className={`site-footer matterport-footer visible-xs
                    ${!isFullScreenBtn ? 'hidden-xs hidden-sm' : ''}`}>
                    <div className="logo-section">
                      <span className="rmargin10">Powered by</span>
                      <img src="dist/img/white-logo.png" alt="White Logo" />
                    </div>
                    <div className="footer-menu-section">
                      <span>Hotel</span>
                      <span>Meeting Rooms</span>
                    </div>
                  </div>
                )
              }
              {
                !isFullImage && !isOnlyView && (
                  <div className={`site-footer ${!isFullScreenBtn ? 'hidden-xs hidden-sm' : ''}`}>
                    <div className="logo-section">
                      <span className="rmargin10">Powered by</span>
                      <img src="dist/img/white-logo.png" alt="White Logo" width="90px" />
                    </div>
                    <div className="footer-menu-section">
                      <span>Hotel</span>
                      <span>Meeting Rooms</span>
                    </div>
                  </div>
                )
              }
              {
                showModal3dAlert
                  && (
                    <LightBox
                      handleClose={this.handleClose}
                      handleSubmit={this.handleClose}
                      message={modelAlertMessage}
                      header="3D diagramming"
                      successText="Ok"
                      showModal={showModal3dAlert}
                    />
                  )
              } 
              {
                viewMap && (
                  <MapBox
                    handleMapClose={this.handleMapClose}
                    showMapModal={showMapModal}
                    hotelInfo={hotelInfo}
                  />
                )
              }     
            </Grid>
            ) : (
              <div>
                {
                  showLoader && (
                    <div className="loader-img-overlay">
                      <div className="loader-img">
                        <img src="dist/img/loader.gif" alt="loading" />
                      </div>
                    </div>
                  )
                }
              </div>
            )
          }
        </div>
      </div>
    );
  }
}

HotelPage.contextTypes = {
  router: PropTypes.object,
};

HotelPage.propTypes = {
  isOnlyView: PropTypes.bool,
};

export default HotelPage;
