import React, { Component } from 'react';
import {
  Grid, Row, Col, Button, Alert,
} from 'react-bootstrap';
import { Link, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import ReactSVG from 'react-svg';
import Autosuggest from 'react-autosuggest';
import CommonLayout from '../CommonLayout/CommonLayout';
import MeetingSpaceAction from '../../actions/MeetingSpaceAction';
import '../../../css/HomePage/HomePage.scss';
import AuthStore from '../../stores/AuthStore';
import DateRangePicker from 'react-bootstrap-daterangepicker';
import RFPManagementStore from '../../stores/RFPManagementStore';
import RFPManagementAction from '../../actions/RFPManagementAction';
import AutosuggestHighlightMatch from 'autosuggest-highlight/match';
import AutosuggestHighlightParse from 'autosuggest-highlight/parse';
import HotelBrandStore from '../../stores/HotelBrandStore';
import HotelBrandAction from '../../actions/HotelBrandAction';

class CreateEvent extends CommonLayout {
  constructor(props) {
    super(props);
    const hotelEventInfo = RFPManagementStore.getAll();
    const { selectedHotel } = hotelEventInfo;
    const value = selectedHotel.hasOwnProperty('name') ? selectedHotel.name : '';
    this.state = {
      isLoggedIn: false,
      checkIn: '--/--/--',
      checkOut: '--/--/--',
      hotelEventInfo,
      hotelBrandInfo : HotelBrandStore.getAll(),
      eventType: '',
      eventName: '',
      eventInfo: {
        name: '',
        start: null,
        end: null,
        attendees: '',
        hotel: selectedHotel.hasOwnProperty('id') ? selectedHotel.id : null,
        type: null,
      },
      value,
      suggestions: [],
      validationStatus:false,
      isClickedCreateEvent:false,
      isValidHotelName:'',
      isBorderRed:false,
      setLoader: false,
    };
    this.handleDateEvent = this.handleDateEvent.bind(this);
    this.handleEventTypes = this.handleEventTypes.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.handleInput = this.handleInput.bind(this);
    this.onRFPManagementStoreChange = this.onRFPManagementStoreChange.bind(this);
    this.onHotelBrandStoreChange = this.onHotelBrandStoreChange.bind(this);
    this.onChange = this.onChange.bind(this);
    this.onSuggestionsFetchRequested = this.onSuggestionsFetchRequested.bind(this);
    this.onSuggestionsClearRequested = this.onSuggestionsClearRequested.bind(this);
    this.handleSelect = this.handleSelect.bind(this);
    this.eventInfoValidation = this.eventInfoValidation.bind(this);
    this.onAuthStoreChange = this.onAuthStoreChange.bind(this);
  }
   
  componentWillMount() {
    window.scrollTo(0, 0);
    const isLoggedIn = AuthStore.isLoggedIn();
    if (isLoggedIn) {
      this.setState({ isLoggedIn });
    }
    const { hotelEventInfo } = this.state;
    const { eventTypes } = hotelEventInfo;
    if (Object.keys(eventTypes).length < 1) {
      RFPManagementAction.getUserEventTypes();
    }
    this.setState({ setLoader: true });
    RFPManagementAction.getHotelList(res => {
      if (res) {
        this.setState({ setLoader: false });
      }
    });
    RFPManagementStore.addStoreListener(this.onRFPManagementStoreChange);
    HotelBrandStore.addStoreListener(this.onHotelBrandStoreChange);
    AuthStore.addStoreListener(this.onAuthStoreChange);
  }

  onRFPManagementStoreChange(action) {
    console.log("onRFPManagementStoreChange", arguments);
    const hotelEventInfo = RFPManagementStore.getAll();
    this.setState({ hotelEventInfo: hotelEventInfo});
  }

  onAuthStoreChange() {
    const isLoggedIn = AuthStore.isLoggedIn();
    this.setState({ isLoggedIn });
  }

  onHotelBrandStoreChange(action) {
    console.log("onHotelBrandStoreChange", arguments);
    const hotelBrandInfo = HotelBrandStore.getAll();
    this.setState({ hotelBrandInfo });
  }

  handleDateEvent(e, picker, type) {
    const { eventInfo, value } = this.state;
    let { startDate } = picker;
    if (type === 'start') {
      startDate = moment(startDate).startOf('day').format('YYYY-MM-DD HH:mm');
    } else {
      startDate = moment(startDate).endOf('day').format('YYYY-MM-DD HH:mm');
    }
    eventInfo[type] = startDate;
    this.setState({ eventInfo });
    this.eventInfoValidation(eventInfo, false, value);
  }

  handleEventTypes(e) {
    const { eventInfo, value } = this.state;
    const typeId = e.target.value;
    eventInfo['type'] = parseInt(typeId, 10);
    this.setState({ eventInfo });
    this.eventInfoValidation(eventInfo, false, value);
  }

  handleClick() {
    const { eventInfo } = this.state;
    let isClickedCreateEvent = true;
    const validationStatus = this.eventInfoValidation(eventInfo, true);
    if (validationStatus) {
      MeetingSpaceAction.restoreData();
      MeetingSpaceAction.storeCreatedEventInfo(eventInfo);
    }
    this.setState({ 
      isClickedCreateEvent, 
      isBorderRed: true,
    });
  }

  eventInfoValidation(eventInfo, addNotificationMsg, hotelName='') {
    let validationStatus = true;
    const { suggestions, hotelEventInfo, value } = this.state;
    const { hotelListDetails } = hotelEventInfo;
    if ((eventInfo.hasOwnProperty('type') && (!eventInfo['type'])) || (eventInfo.hasOwnProperty('hotel') && (!value.length>=1)) 
        || (eventInfo.hasOwnProperty('name') && (eventInfo['name'] === '')) || (eventInfo.hasOwnProperty('attendees') && (eventInfo['attendees'] === '')) 
          || (eventInfo.hasOwnProperty('start') && (!eventInfo['start'])) || (eventInfo.hasOwnProperty('end') && (!eventInfo['end']))) {
      if (addNotificationMsg) {
        const msg = 'Please fill all the fields'
        HotelBrandAction.getNotificationMsg(msg, 'add');
        setTimeout(() => {
          HotelBrandAction.getNotificationMsg(msg, 'remove');
          this.setState({ isClickedCreateEvent :false });
        }, 5000);
      }
      validationStatus = false;
    } 
    hotelName = addNotificationMsg ? value : hotelName;
    const isHotelNameCorrect = hotelListDetails.filter(hotel => (hotel.name).toLowerCase() === (hotelName).toLowerCase()).length>=1;
    this.setState({ isValidHotelName: isHotelNameCorrect })
    if (eventInfo.hasOwnProperty('hotel')  && (!isHotelNameCorrect) && value.length>=1) {
      if (addNotificationMsg) {
        const msg = 'Please choose the valid hotel'
        HotelBrandAction.getNotificationMsg(msg, 'add');
        setTimeout(() => {
          HotelBrandAction.getNotificationMsg(msg, 'remove');
          this.setState({ isClickedCreateEvent :false });
        }, 5000);
      }
      validationStatus = false;
    }
    
    this.setState({ validationStatus });
    return validationStatus    
  }

  handleInput(e) {
    const { eventInfo } = this.state;
    const { value, name } = e.target;
    eventInfo[name] = value;
    if (name !== 'name') {
      eventInfo[name] = parseInt(value, 10);
    }
    this.setState({ eventInfo });
    this.eventInfoValidation(eventInfo, false, this.state.value);
  }

  getSuggestions(value) {
    const { hotelEventInfo } = this.state;
    const { hotelListDetails } = hotelEventInfo;
    const inputValue = value.trim().toLowerCase();
    const inputLength = inputValue.length;
    return inputLength === 0 ? hotelListDetails.filter(
      hotel => hotel.name
    ) : hotelListDetails.filter(
      hotel => hotel.name.toLowerCase().slice(0, inputLength) === inputValue,
    );
  }

  onSuggestionsFetchRequested({ value }) {
    this.setState({
      suggestions: this.getSuggestions(value),
    });
  }

  onSuggestionsClearRequested() {
    this.setState({
      suggestions: [],
    });
  }

  onChange(event, { newValue }) {
    const { eventInfo } = this.state;
    this.setState({
      value: newValue,
    });
    this.eventInfoValidation(eventInfo, false, newValue);
  }

  handleSelect(event, { suggestion }) {
    const { eventInfo } = this.state;
    const hotelId = parseInt(suggestion.id, 10);
    eventInfo['hotel'] = hotelId;
    this.setState({ eventInfo });
  }

  render() {
    const {
      isLoggedIn, isRedirect, value, checkIn, checkOut, hotelEventInfo, eventType, eventName, eventInfo, suggestions,
      hotelBrandInfo, validationStatus, isClickedCreateEvent, isMobileSidebar, isValidHotelName, isBorderRed,
      setLoader,
    } = this.state;
    const { notificationMsg } = hotelBrandInfo;
    const { eventTypes } = hotelEventInfo;
    const getSuggestionValue = suggestion => suggestion.name;
    const renderSuggestion = (suggestion, { query }) => {
      const matches = AutosuggestHighlightMatch(suggestion.name, query);
      const parts = AutosuggestHighlightParse(suggestion.name, matches);
      return (
        <span>
          {parts.map((part, index) => {
            const className = part.highlight ? 'react-autosuggest__suggestion-match primary-color' : null;
            return (
              <span className={className} key={index}>
                {part.text}
              </span>
            );
          })}
        </span>
      );
    };
    const inputProps = {
      value,
      onChange: this.onChange,
      className: isBorderRed && (!isValidHotelName) ? "border-red" : ""
    };
    if (isRedirect || !isLoggedIn) {
      return <Redirect to="/" />;
    }
  	return(
  		<Grid className="primary-color home-container create-event-list-container layout-header">
  		  {this.sideBar()}
        {
          isMobileSidebar && (
            this.mobileSideBar()
          )
        }

  		  <Row className="content-right">
  	        <Row>
            {this.pageHeader((!validationStatus) && isClickedCreateEvent, notificationMsg)}
  	          <Row>
                <Button
                  className="back-button primary-color"
                  onClick={this.context.router.history.goBack}
                >
                  <ReactSVG wrapper="span" className="svg-wrapper" src="dist/img/event_list/back-arrow.svg" alt="back button" width="25px" />
                  <span className="hidden-xs">&nbsp;&nbsp;Back</span>
                </Button>
  	          </Row>
  			    </Row>
            {
              setLoader && !eventInfo.hotel ? (
                <div className="loader-style">Loading hotels to create an event. Please wait...</div>
              ) : (
              <Row>
                <Row>
                  <Col>
                    <div className="column-header-title primary-color" >
                      <p className="list-title tmargin20 text-font-22"><strong>Create new event.</strong></p>
                    </div>
                  </Col>
                </Row>
                <Row className="main-content"> 
                  <Row className="row-content hidden-xs">
                     <div className="column-header-title primary-color">
                       <p><strong>Tell us about your event.</strong></p>
                     </div>
                  </Row>
                   <Row className="visible-xs">
                     <div className="lmargin50 tmargin20 text-font-22 primary-color">
                      <p><strong>Tell us about your </strong></p>
                      <p><strong>event.</strong></p>
                     </div>
                  </Row>
        
                  <Row className="row-content">
                    <Col lg={5} md={5} sm={12} xs={12}>
                     <div className="column-width-column-left">
                        <div>
                          <p className="primary-color-light heading">Hotel</p>
                        </div>
                        <Autosuggest
                          suggestions={suggestions}
                          onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
                          onSuggestionsClearRequested={this.onSuggestionsClearRequested}
                          getSuggestionValue={getSuggestionValue}
                          onSuggestionSelected={this.handleSelect}
                          renderSuggestion={renderSuggestion}
                          shouldRenderSuggestions={() => { return true }}
                          inputProps={inputProps}
                        />
                      {
                      // <div className="form-search primary-color primary-border-color">     
                      //                  <input className="form-search primary-border-color primary-color lpadding10" type="text" placeholder="" name="" size="40"/>
                        <span className="hotel-list-search"><img src="dist/img/event_list/search-black.png" width="20px"/></span>
                      //                </div>
                      }
                     </div>
                    </Col>
                    <Col lg={7} md={7} sm={12} xs={12}>
                      <div className="column-width-column-right">
                        <div>
                          <p className="primary-color-light heading">Event name</p>
                        </div>
                        <div>
                          <input className={`enter-input primary-color-light primary-border-color-light ${isBorderRed && (!validationStatus) && (eventInfo['name'] === '') ? 'border-red' : ''}`}
                          type="text"
                          size="50"
                          value={eventInfo.name}
                          onChange={this.handleInput}
                          name="name"/>
                        </div>
                      </div>
                    </Col>
                  </Row>
                  <Row className="row-content">
                    <Col lg={5} md={5} sm={12} xs={12}>
                      <Col  className="padding0" lg={4} md={4} sm={4} xs={4}>
                        <p className="primary-color-light">Check in</p>
                        <div className="date">
                          <DateRangePicker
                            minDate={moment()}
                            singleDatePicker
                            containerStyles={{ 'box-shadow': 'none', 'width':'100%'}}
                            onApply={(e, picker) => this.handleDateEvent(
                              e, picker, 'start'
                            )}
                          >
                            <Button className={`pick-date ${(isBorderRed && (!validationStatus) && (!eventInfo['start']))? 'border-red' : ''}`}>
                              <strong>
                                {
                                  eventInfo.start ? moment(`${eventInfo.start}`, 'YYYY-MM-DD HH:mm').format('DD/MMM/YYYY') : '- - / - - / - -'
                                }
                              </strong>
                            </Button>
                          </DateRangePicker>
                        </div>
                      </Col>
                      <Col className="lpadding0 tmargin35" lg={4} md={4} sm={4} xs={4}>
                        <div>
                         <img className="image-align-center" src="dist/img/event_list/arrow-black.png" width="25px"/>
                        </div>
                      </Col>
                      <Col className="padding0" lg={4} md={4} sm={4} xs={4}>
                        <p className="primary-color-light">Check out</p>
                        <div className="date">
                          <DateRangePicker
                            minDate={moment()}
                            singleDatePicker
                            containerStyles={{ 'box-shadow': 'none', 'width':'100%' }}
                            onApply={(e, picker) => this.handleDateEvent(
                              e, picker, 'end'
                            )}
                          >
                            <Button className={`pick-date ${(isBorderRed && (!validationStatus) && (!eventInfo['end']))? 'border-red' : ''}`}>
                              <strong>
                                {
                                  eventInfo.end ? moment(`${eventInfo.end}`, 'YYYY-MM-DD HH:mm').format('DD/MMM/YYYY') : '- - / - - / - -'
                                }
                              </strong>
                            </Button>
                          </DateRangePicker>
                        </div>
                      </Col>
                    </Col>
                    <Col lg={7} md={7} xs={12} sm={12}>
                      <Col className="padding0 column-width-column-right hidden-sm hidden-xs" lg={6} md={6}> 
                        <div>
                          <p className="padding0 primary-color-light"># of attendees</p>
                        </div>
                        <div>
                          <input
                            type="text"
                            className={`enter-input primary-border-color primary-border-color-light lpadding10 ${isBorderRed && (!validationStatus) && (eventInfo['attendees'] === '' || isNaN(eventInfo['attendees'])) ? 'border-red' : ''}`}
                            size="20"
                            name="attendees"
                            value={eventInfo.attendees || ''}
                            onChange={this.handleInput}
                          />
                        </div>
                      </Col>
                      <Col className="padding0 tmargin10 visible-sm visible-xs" sm={12} xs={12}>
                        <div className="column-width-column-right">
                          <div>
                            <p className="primary-color-light heading"># of attendees</p>
                          </div>
                          <div>
                            <input className={`enter-input primary-border-color primary-border-color-light lpadding10 ${isBorderRed && (!validationStatus) && (eventInfo['attendees'] === '' || isNaN(eventInfo['attendees'])) ? 'border-red' : ''}`}
                            type="text"
                            size="20"
                            name="attendees"
                            value={eventInfo.attendees || ''}
                            onChange={this.handleInput}
                            />
                          </div>
                        </div>
                      </Col>
                    </Col>
                  </Row>
                  <Row className="row-content">
                    <Col lg={5} md={5} sm={12} xs={12}>
                      
                       <div>
                         <p className="primary-color-light">Event type</p>
                       </div>
                       <div className={`fleft style-background ${(isBorderRed && (!validationStatus) && (!eventInfo['type']))? 'border-red' : ''}`}>
                         <select 
                         className="event-name-dropdown text-font16 dropdown-event-type"
                         onChange={this.handleEventTypes}
                         value={eventInfo.type ? eventInfo.type : 'not selected'}
                         >
                          <option disabled hidden>not selected</option>
                          {
                            Object.keys(eventTypes).length !== 0 && eventTypes.results.map(type => (
                              <option
                                value={type.id}
                                id={type.id}
                              >
                                {type.name}
                              </option>
                            ))
                          }
                         </select>
                       </div>
                      
                    </Col>
                    <Col lg={7} md={7} sm={12} xs={12}>
                      <div>
                        <Link to={validationStatus ? `/property/${eventInfo.hotel}` : `/property/event/create`}>
                          <Button
                            className="primary-color primary-border-color primary-background-color create-event-button"
                            onClick={this.handleClick}
                          >
                            <strong>Create event</strong>
                          </Button>
                        </Link>
                      </div>
                    </Col>
                  </Row>
                </Row>
              </Row>
              )
            }

            <div className="site-footer visible-xs">
                <div className="logo-section">
                    <span className="rmargin10">Powered by</span>
                    <img src="dist/img/white-logo.png" alt="White Logo" width="90px" />
                </div>
                <div className="footer-menu-section">
                    <span>Hotel</span>
                    <span>Meeting Rooms</span>
                </div>
            </div>
          
  			</Row>
  		</Grid>
  		);
  	}
  }

CreateEvent.contextTypes = {
  router: PropTypes.object,
};

export default CreateEvent;
