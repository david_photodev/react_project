import React, { Component } from 'react';
import {
  Grid, Row, Col, Button, Alert,
} from 'react-bootstrap';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import ReactSVG from 'react-svg';
import RFPManagement from '../EventList/RFPManagement';
import '../../../css/HomePage/HomePage.scss';

class Popup extends Component {
  constructor(props){
    super(props);
  }

  componentWillMount() {
    window.scrollTo(0, 0);
  }
  render(){
		const origin = window.location.origin
  	const {
        handleClose, historyInfo, selectedEventInfo, eventStatus
    } = this.props;

    const updatedEventStatus = (name) => {
      if (name) {
        const selectedStatus = eventStatus.results.filter(status => status.name.toLowerCase() === name.toLowerCase());
        return selectedStatus[0].event_status.toUpperCase();
      }
      return '';
    }

    const recentHistoryInfo = (history) => {
      let message;
      const dateFields = ['start', 'end'];
      if ((Object.keys(history.changes).length >= 1) && (Object.keys(history.changes)[0] === 'created' || Object.keys(history.changes)[0] === 'updated')) {
        if (Object.keys(history.changes)[0] === 'created') {
          message = `${history.user.first_name} ${history.user.last_name} created an event ${history.changes.created.hasOwnProperty('name') ?  ': '+history.changes.created.name : ''}`
          return message;
        }
        if (Object.keys(history.changes)[0] === 'updated' && Object.keys(history.changes.updated).length >= 1) {
          if (dateFields.includes(Object.keys(history.changes.updated)[0])) {
            message = `${history.user.first_name} ${history.user.last_name} updated ${Object.keys(history.changes.updated)[0]} from ${moment(Object.values(history.changes.updated)[0]['previous'], 'YYYY-MM-DD').format('YYYY-MM-DD')} to ${moment(Object.values(history.changes.updated)[0]['current'], 'YYYY-MM-DD').format('YYYY-MM-DD')}`
          } else {
            message = `${history.user.first_name} ${history.user.last_name} updated ${Object.keys(history.changes.updated)[0]} from ${Object.values(history.changes.updated)[0]['previous']} to ${Object.values(history.changes.updated)[0]['current']}`
          }
          return message;
        } 
        return false;
      } else {
        message = `${history.user.first_name} ${history.user.last_name} ${Object.keys(Object.values(history.changes)[0])[0]} the ${Object.keys(history.changes)[0].replace('_', ' ')}`
        return message;
      }
    }

    let urlSplit = selectedEventInfo.hotel.url.split('/')
    const propertyId = urlSplit[urlSplit.length - 1]
  	const background = { 'background': 'linear-gradient(175deg, #f0f0f0, #fafafa)'};
  	return(
  		<Grid className="home-container event-history-popup-container primary-color" style={background}>
  		  <Row>
            <Col>
              <Button className="close-button fright" onClick={handleClose}>
              <ReactSVG wrapper="span" className="svg-wrapper fright" src="dist/img/popup/close-button.svg" alt="close button"/></Button>
            </Col>
  		  </Row>
  		  <Row className="row-content">
  		    <span className="primary-color heading margin"><strong>Event History</strong></span>
  		  </Row>
  		  <Row className="content-scroll padding">
  		  	{ historyInfo.map((history)=>
	  		  (<Row className="row-content margin-top">
	  		    <div className="text-color-event-history">
	  		      <span>{moment(new Date()).format('MMMM DD,YYYY') === moment(history.date).format('MMMM DD,YYYY') ? ('Today  — '+moment(history.date).format('MMMM DD, YYYY')+'') : moment(history.date).format('MMMM DD, YYYY')}  </span>
	  		    </div>
	  		    <div className="list-event row">
              <Col lg={3} md={3} sm={3} xs={4}>
  	  		      <span className="header-text-color-1 text-font12">{moment(history.date).format('hh:mm A')}</span>
  	  		      <div className="user-name leftmargin">{history.user.first_name.charAt(0)+history.user.last_name.charAt(0)}</div>
              </Col>
              <Col lg={6} md={6} sm={6} xs={6}>
    	  		   <span className="header-text-color-1 text-font12">{recentHistoryInfo(history) ? recentHistoryInfo(history) : ''}</span>
              </Col>
              <Col lg={3} md={3} sm={3} xs={2}>
  	  		      <span><a className="primary-color fright text-font12 buttonAsText rightmargin"
  	  		      href={origin+'/#/property/'+propertyId+'/rfp/view/'+selectedEventInfo.uuid+''}
  	  		      target="_blank"
  	  		      >
  	  		      <strong>Open event</strong>
  	  		      </a>
  	  		      </span>
              </Col>
	  		    </div>
	  		  </Row>))	  		
	  		}
	  		</Row>
	  		<Row>
	  		<div>
	  		</div>
  		 </Row>
  		</Grid>
  	);
  }
}
export default Popup;