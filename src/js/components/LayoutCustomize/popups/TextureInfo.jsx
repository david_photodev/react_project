import React, { Component } from 'react';
import {
  Row, Col, Button,
} from 'react-bootstrap';

class TextureInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isTextureInfo: true,
    };
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.setState({ isTextureInfo: false });
  }

  render() {
    const { isTextureInfo } = this.state;
    return (
      <div className="texture-info-container">
        {
          isTextureInfo
            && (
            <div className="texture-info-wrapper">
              <Row>
                <Button className="close-button" onClick={this.handleClick}>&#x2715;</Button>
              </Row>
              <Row className="texture-header">
                <Col md={6} className="furniture-name">
                  Chair mod.01
                </Col>
                <Col md={6} className="about">
                  About
                </Col>
              </Row>
              <Row className="texture-info">
                <Col md={6} className="texture-image">
                  <img src="dist/img/chairs/model_1.png" alt="chair" width="258px" height="307px" />
                </Col>
                <Col md={6} className="texture-info-msg">
                  <p>
                    Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ipsum dolor sit
                    amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna
                    aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.
                  </p>
                  <p>Dimensions</p>
                  <p className="text-bold text-font16">30 X 40 X 45</p>
                  <p>Materials</p>
                  <p className="text-bold text-font16">Leather, steel ...</p>
                </Col>
              </Row>
            </div>)
        }
      </div>
    );
  }
}

export default TextureInfo;
