import React, { Component } from 'react';
import {
  Grid, Row, Col, Button, Alert,
} from 'react-bootstrap';
import { Link, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import ReactSVG from 'react-svg';
import HotelBrandStore from '../../stores/HotelBrandStore';
import HotelBrandAction from '../../actions/HotelBrandAction';
import CommonLayout from '../CommonLayout/CommonLayout';
import AuthStore from '../../stores/AuthStore';
import '../../../css/HotelBrand/HotelBrandProfile.scss';
import { CountryDropdown, RegionDropdown, CountryRegionData } from 'react-country-region-selector';


class HotelBrandProfile extends CommonLayout{
	constructor(props){
		super(props);
		this.state = {
      hotelBrandInfo : HotelBrandStore.getAll(),
			isLoggedIn : false,
      setLoader: false,
      editFields: {
        logo_positive: false,
        name: false,
        address: false,
        country: false,
        city: false,
        zipcode: false,
        sender_email: false,
      },
      editValues: {
        name: '',
        address: '',
        city: '',
        country: '',
        zipcode: '',
        sender_email: '',
      },
      notificationType: null,
      chainId: '',
		};
    this.onHotelBrandStoreChange = this.onHotelBrandStoreChange.bind(this);
    this.onAuthStoreChange = this.onAuthStoreChange.bind(this);
    this.enableEditField = this.enableEditField.bind(this);
    this.updateFields = this.updateFields.bind(this);
	}
	componentWillMount(){
		window.scrollTo(0, 0);
		const isLoggedIn = AuthStore.isLoggedIn();
    let chainId;
    const context = window.location.href;
    this.setState({ setLoader: true });
    if (context.indexOf('/brand/profile/') > -1) {
      chainId = context.split('/brand/profile/')[1];
      this.setState({ chainId : chainId });
      HotelBrandAction.readChain(chainId, res => {
        this.setState({ setLoader: false });
      });
    }
		if (isLoggedIn){
			this.setState({isLoggedIn});
      HotelBrandStore.addStoreListener(this.onHotelBrandStoreChange);
      AuthStore.addStoreListener(this.onAuthStoreChange);
		}
	}

  componentDidUpdate(){
    const chainIdChange = window.location.href.split('/brand/profile/')[1];
    const { chainId } = this.state;
    if (chainIdChange !== chainId) {
      this.componentWillMount();
    }
  }

  onHotelBrandStoreChange(action) {
    const hotelBrandInfo = HotelBrandStore.getAll();
    this.setState({ hotelBrandInfo });
  }

  onAuthStoreChange() {
    const isLoggedIn = AuthStore.isLoggedIn();
    this.setState({ isLoggedIn });
  }

  emailValidation(email) {
    if (( email === '') || (!email.match(/^([a-zA-Z0-9_.+-])+@(([a-zA-Z0-9-])+.)+([a-zA-Z0-9]{2,4})+$/))) {
      return false
    } else {
      return true;
    }
  }

  updateBrandApi(field, updateInfo, brandId) {
    const { hotelBrandInfo, editValues } = this.state;
    const { selectedProfile } = hotelBrandInfo;
    clearTimeout(this.timer);
    this.timer = setTimeout(() => {
      this.setState({ displayNotification : true, notificationMsg: ['Updating...'], notificationType: 'update'});
      HotelBrandAction.updateBrand(brandId, updateInfo, (res)=>{
        selectedProfile[field] = res[field];
        this.setState({  selectedProfile });
        this.setState({ displayNotification : false, notificationMsg: [], notificationType: null});
        HotelBrandAction.getHotelBrandList(false);
      });
    }, 500);
  }

  displayErrorMsg(msg) {
    setTimeout(() => {
      this.setState({ displayNotification : true, notificationMsg: [msg] });
    }, 2000);
    setTimeout(() => {
      this.setState({ displayNotification : false, notificationMsg: [] });
    }, 5000);
  }

  updateFields(e, brandId, field) {
    const { editValues, hotelBrandInfo } = this.state;
    const { selectedProfile } = hotelBrandInfo;
    const value = e.hasOwnProperty('target') ? e.target.value : e;
    let updateInfo={};
    if (field === 'sender_email') {
      editValues[field] = value;
      this.setState({ editValues });
      const isEmailValid = this.emailValidation(value);
      if (isEmailValid) {
        updateInfo[field] = value;
        this.updateBrandApi(field, updateInfo, brandId);
      } else {
        this.displayErrorMsg('Please enter valid email');
      }
    }
    if (field === 'city' || field === 'address') {
      editValues[field] = value;
      let findAddress = selectedProfile['address'];
      let findCity = selectedProfile['city'];
      if (field === 'city') {
        findCity = value;
      }
      if (field === 'address') {
        findAddress = value;
      }
      this.setState({ editValues });
      const address = findAddress.replace(/ /g,"+")+'+'+findCity.replace(/ /g,"+");
      HotelBrandAction.getGeometryLocation(address, (GeometryLocation)=>{
        if (GeometryLocation.status === "OK") {
          updateInfo[field] = value;
          const location = GeometryLocation.results[0].geometry.location.lng+', '+GeometryLocation.results[0].geometry.location.lat;
          updateInfo['location'] = location;
          this.updateBrandApi(field, updateInfo, brandId);
        } else {
          this.displayErrorMsg('Invalid address and city. Please check');
        }
      })
    } 
    if (field === 'logo_positive') {
      const fileInput = document.getElementById("logo_positive");
      const file = fileInput.files[0];
      updateInfo[field] = file;
      if (file) {
        this.updateBrandApi(field, updateInfo, brandId);
      }
    } 
    if (field === 'country' || field === 'zipcode') {
      editValues[field] = value;
      updateInfo[field] = value;
      this.setState({ editValues });
      this.updateBrandApi(field, updateInfo, brandId);
    }  
    if(field === 'name') {
      editValues[field] = value;
      this.setState({ editValues });
      if (!(/\S/.test(value))) {
        this.displayErrorMsg('Please enter the valid corporate brand name');
      }else {
        updateInfo[field] = value;
        updateInfo['description'] = value;
        this.updateBrandApi(field, updateInfo, brandId);
      }
    }

  }

  enableEditField(fieldType, isEnable) {
    const { editFields, hotelBrandInfo, editValues } = this.state;
    const { selectedProfile } = hotelBrandInfo;
    Object.keys(editFields).map(field => {
      if (field === fieldType) { 
        editFields[fieldType] = isEnable;
        editValues[fieldType] = selectedProfile[fieldType];
        this.setState({ editValues });
      } else if (isEnable){
        editFields[field] = false;
      }
    });
    this.setState({ editFields });
  }

	render(){
		const { isLoggedIn, isRedirect, hotelBrandInfo, isMobileSidebar, setLoader, editFields, editValues, notificationType, displayNotification, notificationMsg } = this.state;
    const { selectedProfile } = hotelBrandInfo;
		if (isRedirect || !isLoggedIn){
			return <Redirect to="/" />;
		}
		return(
			<Grid className="primary-color hotel-brand-profile-container"> 
			  {this.sideBar()}
        {
          isMobileSidebar && (
            this.mobileSideBar()
          )
        }
			  <Row className="content-right">
			    {this.pageHeader(displayNotification, notificationMsg, null, notificationType)}
                {
                  setLoader ? (
                    <div className="empty-profile-msg">Loading...</div>
                  ) : (
                    <div>
                      <Row className="align-header">
                        <div className="header-title"><strong>Hotel brand profile</strong></div>
                      </Row> 
                      <Row className="tpadding20">
                        <Col lg={2} md={3} sm={3} xs={6} >
                          <div>
                            <input 
                              type="file"
                              accept="image/x-png,image/jpeg"
                              className="input-file-class visibilty-hidden"
                              onChange={(e)=>{this.updateFields(e,selectedProfile.id,'logo_positive')}}
                              id="logo_positive"
                            />
                            <label for="logo_positive" >
                              <div
                                className={`${selectedProfile.logo_positive ? 'border-hotel-logo' : 'visibilty-hidden'}`} 
                                onClick={() => {this.enableEditField('logo_positive', true)}}
                                >
                                  <img className="logo-size" src={selectedProfile.logo_positive || ''} alt="Hotel Logo" />
                              </div>
                            </label>
                          </div>
                        </Col>
                      </Row>
                      <Row className="main-content padding20">
                        <div className="text-font16">Corporate brand name
                        </div>
                        <div className="text-font16 tmargin10">
                          {!editFields.name ? 
                            (<div onClick={() => {this.enableEditField('name', true)}}>
                              <strong>{selectedProfile.name}
                              </strong>
                              <span className="lmargin10 glyphicon glyphicon-pencil"></span>
                            </div>) : 
                            (<input
                              type="text"
                              placeholder="name"
                              className="edit-input-field"
                              value={editValues.name}
                              onChange={(e)=>{this.updateFields(e,selectedProfile.id,'name')}}
                              autoFocus="true"
                              onBlur={() => {this.enableEditField('name', false)}}
                            />)
                          }
                        </div>
                      </Row>
                      <Row className="main-content-details dflex lpadding10 tpadding20">
                        <Col className="bpadding20" lg={4} md={4} sm={12} xs={12}>
                          <div className="text-font-15">Address
                          </div>
                          <div 
                            className="text-font16 tpadding10"
                          >
                          { !editFields.address ? 
                            (<Row onClick={() => {this.enableEditField('address', true)}}>
                              <Col className="lpadding0" lg={10} md={10} sm={10} xs={10}>
                               <strong>{selectedProfile.address || '-'}
                               </strong>
                              </Col>
                              <Col lg={2} md={2} sm={2} xs={2}>
                               <span className="lmargin10 glyphicon glyphicon-pencil"></span>
                              </Col>
                            </Row>) :
                            (<input
                              type="text"
                              placeholder="address"
                              className="edit-input-field"
                              value={editValues.address}
                              onChange={(e)=>{this.updateFields(e,selectedProfile.id,'address')}}
                              autoFocus="true"
                              onBlur={() => {this.enableEditField('address', false)}}
                            />)
                          }
                          </div>
                        </Col>
                        <Col className="draw-border primary-border-top primary-border-bottom-light primary-border-right-light primary-border-left-light" lg={4} md={4} sm={12} xs={12}>
                          <Row>
                            <Col className="align-content" lg={6} md={6} sm={6} xs={6}>
                              <span className="text-font-15">Country / Region</span>
                            </Col>
                            <Col lg={6} md={6} sm={6} xs={6}>
                            { !editFields.country ?
                              (<Row onClick={() => {this.enableEditField('country', true)}}>
                                <Col className="lpadding0" lg={9} md={9} sm={9} xs={9}>
                                 <span className="text-font16"><strong>{selectedProfile.country || '-'}</strong>
                                 </span>
                                </Col>
                                <Col className="lpadding0" lg={3} md={3} sm={3} xs={3}>
                                 <span className="lmargin10 glyphicon glyphicon-pencil"></span>
                                </Col>
                              </Row>) : 
                              (<CountryDropdown className="country-field"
                                defaultOptionLabel = "- -"
                                onChange={(e)=>{this.updateFields(e,selectedProfile.id,'country')}}
                                value={editValues.country}
                                autoFocus="true"
                                onBlur={() => {this.enableEditField('country', false)}}
                                name="country"
                              />
                              )
                            }
                            </Col>
                          </Row>
                          <Row className="tpadding10">
                            <Col className="align-content" lg={6} md={6} sm={6} xs={6}>
                              <span className="text-font-15">City</span>
                            </Col>
                            <Col lg={6} md={6} sm={6} xs={6}>
                            { !editFields.city ?
                              (<Row onClick={() => {this.enableEditField('city', true)}}>
                                <Col className="lpadding0" lg={9} md={9} sm={9} xs={9}>
                                 <span className="text-font16"><strong>{selectedProfile.city || '-'}</strong>
                                 </span>
                                </Col>
                                <Col className="lpadding0" lg={3} md={3} sm={3} xs={3}>
                                 <span className="lmargin10 glyphicon glyphicon-pencil"></span>
                                </Col>
                              </Row>) : 
                              (<input
                                type="text"
                                placeholder="city"
                                className="edit-input-field"
                                onChange={(e)=>{this.updateFields(e,selectedProfile.id,'city')}}
                                value={editValues.city}
                                onBlur={() => {this.enableEditField('city', false)}}
                                autoFocus="true"
                              />)
                            }
                            </Col>
                          </Row>
                          <Row className="tpadding10">
                            <Col className="align-content" lg={6} md={6} sm={6} xs={6}>
                              <span className="text-font-15">Zip code</span>
                            </Col>
                            <Col lg={6} md={6} sm={6} xs={6}>
                            { !editFields.zipcode ?
                              (<Row onClick={() => {this.enableEditField('zipcode', true)}}>
                                <Col className="lpadding0" lg={9} md={9} sm={9} xs={9}>
                                 <span className="text-font16"><strong>{selectedProfile.zipcode || '-'}</strong>
                                 </span>
                                </Col>
                                <Col className="lpadding0" lg={3} md={3} sm={3} xs={3}>
                                 <span className="lmargin10 glyphicon glyphicon-pencil"></span>
                                </Col>
                              </Row>) :
                              (<input
                                type="text"
                                placeholder="zip code"
                                className="edit-input-field"
                                value={editValues.zipcode}
                                onChange={(e)=>{this.updateFields(e,selectedProfile.id,'zipcode')}}
                                onBlur={() => {this.enableEditField('zipcode', false)}}
                                autoFocus="true"
                              />)
                            }
                            </Col>
                          </Row>
                        </Col>
                        <Col className="content-email" lg={4} md={4} sm={12} xs={12}>
                          <div className="text-font-15">e-mail
                          </div>
                          <div className="text-font16 tpadding10">
                          { !editFields.sender_email ?
                            (<Row onClick={() => {this.enableEditField('sender_email', true)}}>
                              <Col className="lpadding0" lg={9} md={9} sm={9} xs={9}>
                               <strong>{selectedProfile.sender_email || '-'}
                               </strong>
                              </Col>
                              <Col lg={3} md={3} sm={3} xs={3}>
                               <span className="lmargin10 glyphicon glyphicon-pencil"></span>
                              </Col>
                            </Row>) : 
                            (<input
                                type="text"
                                placeholder="email"
                                className="edit-input-field"
                                onChange={(e)=>{this.updateFields(e,selectedProfile.id,'sender_email')}}
                                value={editValues.sender_email}
                                onBlur={() => {this.enableEditField('sender_email', false)}}
                                autoFocus="true"
                              />)
                          }
                          </div>
                        </Col>
                      </Row>
                    </div>
                  )
                }
                <Row className="tpadding20 bpadding20">
                 <Col>
                   <Button
                    className="fright button-back primary-color primary-border-color-light rmargin10"
                    onClick={this.context.router.history.goBack}>
                    Back
                   </Button>
                 </Col>
                </Row>
                <div className="site-footer visible-xs">
                    <div className="logo-section">
                      <p className="rmargin10">Powered by</p>
                      <img src="dist/img/white-logo.png" alt="White Logo" />
                    </div>
                    <div className="footer-menu-section">
                      <span>Hotel</span>
                      <span>Meeting Rooms</span>
                    </div>
                </div>
              </Row>
			</Grid>
		)
	}
}

HotelBrandProfile.contextTypes = {
  router: PropTypes.object,
};

export default HotelBrandProfile;
