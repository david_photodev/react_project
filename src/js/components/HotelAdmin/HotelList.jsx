import React, { Component } from 'react';
import {
  Grid, Row, Col, Button, Alert,
} from 'react-bootstrap';
import { Link, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import ReactSVG from 'react-svg';
import '../../../css/HotelAdmin/HotelList.scss';
import HotelListAction from '../../actions/HotelListAction';
import RFPManagementAction from '../../actions/RFPManagementAction';
import PaginationContainer from '../Reusables/PaginationContainer';
import HotelListStore from '../../stores/HotelListStore';
import CommonLayout from '../CommonLayout/CommonLayout';
import AuthStore from '../../stores/AuthStore';

class HotelList extends CommonLayout {
  constructor(props) {
    super(props);
    this.state = {
      hotelList: HotelListStore.getAll(),
      isLoggedIn : false,
      setLoader: false,
      sortByNameAsc: false,
      isSortByNameActive: false,
    };
    this.sortBy = this.sortBy.bind(this);
    this.handleSearch = this.handleSearch.bind(this);
    this.handleToggle = this.handleToggle.bind(this);
    this.createNewEvent = this.createNewEvent.bind(this);
    this.onHotelStoreChange = this.onHotelStoreChange.bind(this);
    this.onAuthStoreChange = this.onAuthStoreChange.bind(this);
    this.handlePageChange = this.handlePageChange.bind(this);
  }
    
  componentWillMount() {
    const { router } = this.context;
    const { hotelList, isSortByNameActive, sortByNameAsc } = this.state;
    const { selectedPage, chainList } = hotelList;
    const context = router.history.location;
    const { match } = router.route;
    if (context.pathname === '/property/hotel') {
      router.history.push(`${match.url}/list`);
    }
    HotelListStore.addStoreListener(this.onHotelStoreChange);
    AuthStore.addStoreListener(this.onAuthStoreChange);
    this.getHotels(selectedPage, isSortByNameActive, sortByNameAsc);
    HotelListAction.searchBy('');
    if (chainList.length < 1) {
      HotelListAction.getHotelChainList();
    }
    window.scrollTo(0, 0);
    const isLoggedIn = AuthStore.isLoggedIn();
    if (isLoggedIn) {
      this.setState({ isLoggedIn });
    }
  }

  onAuthStoreChange() {
    const isLoggedIn = AuthStore.isLoggedIn();
    this.setState({ isLoggedIn });
  }

  getHotels(selectedPage, isSortByNameActive, sortByNameAsc) {
    this.setState({ setLoader: true });
    const msg = 'Loading...';
    HotelListAction.getNotificationMsg(msg, 'add');
    HotelListAction.getHotelList(selectedPage, isSortByNameActive, sortByNameAsc, (res) => {
      if (res) {
        HotelListAction.getNotificationMsg(msg, 'remove');
        this.setState({ setLoader: false });
      }
    });
  }

  sortBy(sortByNameAsc) {
    const { hotelList } = this.state;
    const { selectedPage } = hotelList;
    this.setState({sortByNameAsc:!sortByNameAsc, isSortByNameActive: true});
    this.getHotels(selectedPage, true, !sortByNameAsc)
  }

  handleSearch(e) {
    const { value } = e.target
    const { hotelList, isSortByNameActive, sortByNameAsc } = this.state;
    const { selectedPage } = hotelList;
    HotelListAction.searchBy(value);
    this.setState({ setLoader: true });
    const msg = 'Loading...';
    clearTimeout(this.timer);
    HotelListAction.getNotificationMsg(msg, 'add');
    this.timer = setTimeout(() => {
      if (value) {
        HotelListAction.paginationClick(1);
        HotelListAction.getHotelListBySearch(value, null, (res) => {
          if (res) {
            HotelListAction.getNotificationMsg(msg, 'remove');
            this.setState({ setLoader: false });
          }
        });
      } else {
        HotelListAction.getHotelList(selectedPage, isSortByNameActive, sortByNameAsc, (res) => {
          if (res) {
            HotelListAction.getNotificationMsg(msg, 'remove');
            this.setState({ setLoader: false });
          }
        });
      }
    }, 500);
  }

  handleToggle(hotel, e) {
    const active = e.target.checked;
    HotelListAction.statusChange(hotel, active);
  }

  createNewEvent(hotel, e) {
    e.preventDefault();
    const { router } = this.context;
    router.history.push('/property/event/create');
    const { hotelList } = this.state;
    const { data } = hotelList;
    RFPManagementAction.hotelList(data);
    RFPManagementAction.selectedHotel(hotel);
  }

  onHotelStoreChange() {
    const hotelList = HotelListStore.getAll();
    this.setState({ hotelList });
  }

  handlePageChange(selectedPage) {
    const { hotelList, isSortByNameActive, sortByNameAsc} = this.state;
    const { searchVal } = hotelList;
    this.setState({ selectedPage });
    HotelListAction.paginationClick(selectedPage);
    if (searchVal) {
      this.setState({ setLoader: true });
      const msg = 'Loading...';
      HotelListAction.getNotificationMsg(msg, 'add');
      HotelListAction.getHotelListBySearch(searchVal, selectedPage, (res) => {
        if (res) {
          HotelListAction.getNotificationMsg(msg, 'remove');
          this.setState({ setLoader: false });
        }
      });
    } else {
      this.getHotels(selectedPage, isSortByNameActive, sortByNameAsc);
    }
  }

  render() {
    const {
      isRedirect, isLoggedIn, hotelList, setLoader, sortByNameAsc, isMobileSidebar,
    } = this.state;
    const {
      hotelListDetails, searchVal, notificationMsg, selectedPage,
    } = hotelList;
    const isPagingation = hotelList && hotelList.totalCount >= 1;
    if (isRedirect || !isLoggedIn) {
      return <Redirect to="/" />;
    }

    return (
      <Grid className="primary-color hotellist-container layout-header">
        {this.sideBar()}
        {
          isMobileSidebar && (
            this.mobileSideBar()
          )
        }
        <Row className="content-right">
          <Row>
            {this.pageHeader(true, notificationMsg, null, 'update')}
            <Row>
              <Col className="align-back-button" lg={9} md={8} sm={7} xs={4}>
                <Button
                  className="back-button primary-color hotel-list-bt"
                  onClick={this.context.router.history.goBack}
                >
                  <ReactSVG wrapper="span" className="svg-wrapper" src="dist/img/event_list/back-arrow.svg" alt="back button" width="25px" /><span className="hidden-xs">&nbsp;&nbsp;Back</span>
                </Button>
              </Col>
              <Col className="primary-border-color border-search-bar tmargin20" lg={3} md={4} sm={5} xs={8}>
                <Col lg={9} md={9} sm={9} xs={10}>
                  <div>
                    <input
                      className="find-text text-font-20"
                      type="text"
                      size="20"
                      placeholder="Select Hotel"
                      value={searchVal}
                      onChange={this.handleSearch}
                    />
                  </div>
                </Col>
                <Col className="search-icon fright" lg={3} md={3} sm={3} xs={2}>
                  <span className="fright"><ReactSVG wrapper="span" className="svg-wrapper" src="dist/img/event_list/search.svg" width="25px"/></span>
                </Col>
              </Col>
            </Row>
            <Row className="row-title hotel-list">
              <Col lg={8} md={8} sm={8} xs={7}>
                <div className="column-header primary-color" >
                  <p className="header-title"><strong>Hotel list</strong></p>
                </div>
              </Col>
              <Col className="align-status-text" lg={4} md={4} sm={4} xs={5}>
                <div className="add-hotel fright i-block pointer">
                  <Link to="/property/hotel/create">
                    <img
                      className="hotel-list-add-image"
                      src="dist/img/meeting_space/icons/plus.png"
                      alt="plus"
                    />
                    <span className="add-hotel-text">Add Hotel</span>
                  </Link>
                </div>
              </Col>
            </Row>
          </Row>
          <Row className="main-content hotel-list-main-content">
            <Row className="table-title hotel-list-table-title">
              <Col lg={9} md={9} sm={9} className="hidden-xs">
                <div
                  className="content-table-heading-1 primary-color"
                  onClick={() => {this.sortBy(sortByNameAsc)}}
                >
                  <p className="content">
                    <strong>Hotel Name</strong>&nbsp;&nbsp;
                    <ReactSVG 
                      wrapper="span" 
                      className={`svg-wrapper ${!sortByNameAsc ? 'transform' : ''}`}
                      src="dist/img/meeting_space/icons/arrow-down.svg" 
                      alt="arrow down"
                      width="10px"
                    />
                  </p>
                </div>
              </Col>
              
              <Col lg={3} md={3} sm={3} className="hidden-xs">
                <div className="content-table-heading-2 primary-color">
                  <p className="content" style={{'padding-top': '10px'}}>Inactive/<strong>Active</strong></p>
                </div>
              </Col>
              <Col lg={4} md={4} sm={4} xs={8} className="visible-xs tmargin20">
                <div className="content-table-heading-1 primary-color">
                  <p className="content">
                    <strong>Hotel Name</strong>
                  </p>
                </div>
              </Col>
              <Col lg={3} md={3} sm={3} xs={4} className="align-status-text visible-xs tmargin20">
                <div className="text-status content-table-heading-2 primary-color">
                  <p className="content" style={{ 'padding-top': '10px' }}>
                    Inactive/<strong>Active</strong>
                  </p>
                </div>
              </Col>
            </Row>
            {
              hotelListDetails.length >= 1 && (
                <div>
                  {
                    hotelListDetails.map(hotel => (
                        <Row className={`list-event hotel-list-row ${hotel.status === 'Active' ? '' : 'no-pointers disable-hotel'}`}>
                          <Link to={`/property/hotel/view/${hotel.id}`}>
                            <Col lg={6} md={6} sm={6} xs={4}>
                              <div className="hotel-name primary-color">
                                <p className="text-bold tpadding10"><strong>{hotel.name}</strong></p>
                              </div>
                            </Col>
                          </Link>
                          <Col
                            className="hotel-detail visible-xs primary-border-right-light"
                            xs={5}
                            onClick={e => this.createNewEvent(hotel, e)}
                          >
                            <img
                              className="hotel-list-add-image"
                              src="dist/img/meeting_space/icons/plus.png"
                              alt="plus"
                            />
                            <span className="lmargin10">Event</span>
                          </Col>
                          <Col lg={3} md={3} sm={3} xs={3} className="hidden-xs">
                              <div className="hotel-detail">
                                <Button
                                  className="primary-color primary-border-color create-event-button-small"
                                  onClick={e => this.createNewEvent(hotel, e)}
                                >
                                  Create new event
                                </Button>
                              </div>
                          </Col>
                          <Col lg={3} md={3} sm={3} xs={3} className="hidden-xs">
                            <div className="hotel-detail visible-pointers">
                              <label class="switch">
                                <input
                                  type="checkbox"
                                  checked={hotel.status === 'Active'}
                                  onChange={e => this.handleToggle(hotel, e)}
                                />
                                <span
                                  class="slider round primary-background-color"
                                />
                              </label>
                            </div>
                          </Col>
                          <Col lg={3} md={3} sm={3} xs={3} className="visible-xs lpadding0 rpadding0">
                            <Col className="hotel-detail visible-pointers" xs={12}>
                              <label class="switch">
                                <input
                                  type="checkbox"
                                  checked={hotel.status === 'Active'}
                                  onChange={e => this.handleToggle(hotel, e)}
                                />
                                <span class="slider round" />
                              </label>
                            </Col>
                          </Col>
                        </Row>
                    ))
                  }
                </div>
              )
          }
            {
            hotelListDetails.length < 1 && !setLoader && (
              (
                <Row>
                  <div className="empty-hotel-msg">
                    <span>No Hotels Found!</span>
                  </div>
                </Row>
              )
            )
          }
            {
            hotelListDetails.length < 1 && setLoader && (
              (
              <Row>
                <div className="empty-hotel-msg">
                  <span>Loading....</span>
                </div>
              </Row>
              )
            )
          }
            <Row>
              {
                isPagingation && (
                  <PaginationContainer
                    count={hotelList.totalCount}
                    paginationCB={this.handlePageChange}
                    selectedPage={selectedPage}
                  />
                )
              }
            </Row>
          </Row>
          <div className="site-footer visible-xs">
            <div className="logo-section">
              <p className="rmargin10">Powered by</p>
              <img src="dist/img/white-logo.png" alt="White Logo" />
            </div>
            <div className="footer-menu-section">
              <span>Hotel</span>
              <span>Meeting Rooms</span>
            </div>
          </div>
        </Row>
      </Grid>
    );
  }
}

HotelList.contextTypes = {
  router: PropTypes.object,
};

export default HotelList;
