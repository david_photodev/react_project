import React, { Component } from 'react';
import {
  Grid, Row, Col, Button, Alert,
} from 'react-bootstrap';
import { Link, Redirect } from 'react-router-dom';
import DragSortableList from 'react-drag-sortable';
import VideoThumbnail from 'react-video-thumbnail';
import { Player } from 'video-react';
import converter from 'number-to-words';
import PropTypes from 'prop-types';
import ReactSVG from 'react-svg';
import { CountryDropdown } from 'react-country-region-selector';
import HotelListAction from '../../actions/HotelListAction';
import HotelBrandAction from '../../actions/HotelBrandAction';
import MeetingSpaceAction from '../../actions/MeetingSpaceAction';
import HotelListStore from '../../stores/HotelListStore';
import CommonLayout from '../CommonLayout/CommonLayout';
import '../../../css/HotelAdmin/CreateProfile.scss';
import AuthStore from '../../stores/AuthStore';
import LightBox from '../Reusables/LightBox';
import Helpers from '../Reusables/Helpers';
import '../../../../node_modules/video-react/dist/video-react.css';

class CreateHotelProfile extends CommonLayout {
  constructor(props) {
    super(props);
    this.state = {
      isLoggedIn: false,
      hotelList: HotelListStore.getAll(),
      showMultiMediaPopUp: false,
      availableSetups: null,
      errorMsg: [],
      showFloorPopup: false,
      createHotelInfo: {
        custom_domain: 'http://',
        facebook_url: 'http://',
        tripadvisor_url: 'http://',
        instagram_url: 'http://',
        status: 1,
      },
      createdHotel: {},
      facilityInfo: [
        {
          tempId: 1,
          name: '',
          placeholder: 'E.g. Business center',
        },
        {
          tempId: 2,
          name: '',
          placeholder: 'E.g. 400 spaces parking',
        },
        {
          tempId: 3,
          name: '',
          placeholder: 'E.g. Restaurant on-site',
        },
        {
          tempId: 4,
          name: '',
          placeholder: '',
        },
      ],
      // availableSetups,
      meetingRoomDetails: [],
      addedMeetingRooms: [],
      addedAssets:[],
      assestType:'photos',
      whichRoomAssetPopUp:null,
      photosTypes:['image/png','image/jpeg'],
      videosTypes:[],
      exceptVideoTypes: ["application/pdf", "image/png", "application/xml", "application/vnd.ms-excel", 
        "application/rtf", "image/jpeg", "application/vnd.ms-excel", "application/vnd.ms-excel.sheet.binary.macroenabled", 
        "application/vnd.openxmlformats-officedocument.word", "application/vnd.ms-excel.sheet.macroenabled.12"],
      typesOf3D:['bin','gltf','png'],
      isVideoPlay:false,
      videoURL: '',
      isClickedCreateHotel: false,
      whichRoomSaveBtnClicked: null,
      floors: [{
        floor: 'all',
        name: 'All',
      }],
      isConfirmationPopup: false,
      showSetupDropdown: false,
      hotelInfo: null,
      setLoader: false,
      updatebuttonDisable: false,
      hotelId: '',
      selectedFloorId: 'all',
      allFloorMeetingRooms: [],
      allEditFloorMeetingRooms: [],
      editUrl: '',
      isDisplayUrlInputField: false,
    };
    this.selectChain = this.selectChain.bind(this);
    this.updateInputs = this.updateInputs.bind(this);
    this.handleActive = this.handleActive.bind(this);
    this.addFacility = this.addFacility.bind(this);
    this.onHotelStoreChange = this.onHotelStoreChange.bind(this);
    this.onAuthStoreChange = this.onAuthStoreChange.bind(this);
    this.createHotel = this.createHotel.bind(this);
    this.addMeetingRoom = this.addMeetingRoom.bind(this);
    this.removeMeetingRoom = this.removeMeetingRoom.bind(this);
    this.updateRoomInputs = this.updateRoomInputs.bind(this);
    this.saveRoom = this.saveRoom.bind(this);
    this.showMultiMedia = this.showMultiMedia.bind(this);
    this.closeMultiMedia = this.closeMultiMedia.bind(this);
    this.onSort = this.onSort.bind(this);
    this.uploadedMedia = this.uploadedMedia.bind(this);
    this.changeAssetType = this.changeAssetType.bind(this);
    this.videoPlay = this.videoPlay.bind(this);
    this.closeVideo = this.closeVideo.bind(this);
    this.closeAlert = this.closeAlert.bind(this);
    this.getThumbnailFile = this.getThumbnailFile.bind(this);
    this.checkErrorMsg = this.checkErrorMsg.bind(this);
    this.updatelinks = this.updatelinks.bind(this);
    this.copyLinks = this.copyLinks.bind(this);
    this.addFloor = this.addFloor.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.floorName = this.floorName.bind(this);
    this.createFloor = this.createFloor.bind(this);
    this.selectFloor = this.selectFloor.bind(this);
    this.statusChange = this.statusChange.bind(this);
    this.validateUrl = this.validateUrl.bind(this);
    this.editMeetingRoom = this.editMeetingRoom.bind(this);
    this.showConfirmationPopup = this.showConfirmationPopup.bind(this);
    this.deleteMeetingRoom = this.deleteMeetingRoom.bind(this);
    this.handleSetupDropdown = this.handleSetupDropdown.bind(this);
    this.closeConfirmationPopup = this.closeConfirmationPopup.bind(this);
    this.setAddedAssets = this.setAddedAssets;
    this.addedLinks = this.addedLinks.bind(this);
    this.handleAddLink = this.handleAddLink.bind(this);
    this.sortBySpace = this.sortBySpace.bind(this);
    this.sortByDimensions = this.sortByDimensions.bind(this);
    this.sortByHeight = this.sortByHeight.bind(this);
    this.sortBySetup = this.sortBySetup.bind(this);
    this.deleteAssets = this.deleteAssets.bind(this);
  }

  componentWillMount() {
    HotelListStore.addStoreListener(this.onHotelStoreChange);
    AuthStore.addStoreListener(this.onAuthStoreChange);
     MeetingSpaceAction.getAttachementTypes((res) => { 
      const attachmentTypes = Object.keys(res);
      const {exceptVideoTypes} = this.state;
      this.setState({videosTypes: attachmentTypes.filter(attachmentType=> {return !exceptVideoTypes.includes(attachmentType)}).join(',')});
     })
    window.scrollTo(0, 0);
    const isLoggedIn = AuthStore.isLoggedIn();
    const { hotelList } = this.state;
    const { chainList } = hotelList;
    const { router } = this.context;
    const context = router.route.location.pathname;
    if (chainList.length < 1) {
      this.setState({ setLoader: true });
      HotelListAction.getHotelChainList(res => {
        if (res) {
          this.setState({ setLoader: false });
        }
      });
    }
    if (isLoggedIn) {
      this.setState ({ isLoggedIn });
    }
    if (context.indexOf('/hotel/view/') > -1) {
      this.setState({ setLoader: true });
      const hotelId = context.split('/hotel/view/')[1];
      this.setState({ hotelId : hotelId });
      HotelListAction.readHotel(hotelId, (hotelInfo) => {
        this.setState({ hotelInfo });
        this.setAddedAssets(hotelInfo.meeting_rooms);
        const updatedHotelInfo = JSON.parse(JSON.stringify(hotelInfo));
        const createHotelInfo = updatedHotelInfo;
        const setupURL = `${updatedHotelInfo.hotel_chain.url}/setup_types`;
        const hotelChainUrl = updatedHotelInfo.hotel_chain.url.split('hotel_chain/')[1];
        if (updatedHotelInfo.facilities.length) {
          const facilityInfo = updatedHotelInfo.facilities.map((facility, i) => {
            facility.tempId = i + 1;
            return facility;
          });
          this.setState({ facilityInfo });
        }
        let meetingRooms = [];
        const availableFloors = [{
          floor: 'all',
          name: 'All',
        }];
        if (updatedHotelInfo.meeting_rooms.length) {
          meetingRooms = updatedHotelInfo.meeting_rooms.map(room => {
            const checkAvailableFloor = availableFloors.filter(val => val.floor === room.floor);
            if (!checkAvailableFloor.length) {
              const floorName = `${converter.toWordsOrdinal(room.floor)} Floor`;
              const floorParams = {
                floor: room.floor,
                name: room.floor === 0 ? 'Ground Floor' : floorName.charAt(0).toUpperCase() + floorName.slice(1),
              };
              availableFloors.push(floorParams);
            }
      
            if (room.room_setups.length) {
              room.room_setups = room.room_setups.map(roomSetup => {
                let setupNameUpdated = roomSetup.type.name.toLowerCase();
                setupNameUpdated = setupNameUpdated === 'u-shape' ? 'ushape' : setupNameUpdated;
                roomSetup.name = setupNameUpdated;
                roomSetup.type = roomSetup.type.id;
                return roomSetup;
              });
            }
            return room;
          });
        }
        createHotelInfo.hotel_chain = parseInt(hotelChainUrl, 10);
        createHotelInfo.status = updatedHotelInfo.status === 'Active' ? 1 : updatedHotelInfo.status === 'Disabled' ? 2 : 3;
        const replaceItems = Object.keys(createHotelInfo).filter(info => createHotelInfo[info] === 'None');
        replaceItems.forEach((element) => {
          createHotelInfo[element] = '';
        });
        this.setState({
          viewPage: true,
          createdHotel: createHotelInfo,
          createHotelInfo,
          floors: availableFloors.sort((a, b) => a.floor - b.floor),
          addedMeetingRooms: updatedHotelInfo.meeting_rooms,
        });
        HotelListAction.getAvailableSetup(setupURL, hotelList.setUpImages, res => {
          if (res.length > 1) {
            this.setState({ availableSetups: res, setLoader: false });
          } else {
            this.setState({
              availableSetups: [],
              setLoader: false,
              setupMsg: 'This chain does not have any available setup.',
            });
          }
        });
      });
    }
  }

  componentWillUnmount() {
    HotelListStore.removeStoreListener(this.onHotelStoreChange);
  }

  componentDidUpdate(){
    const path = window.location.href.split('/hotel/')[1];
    if (path && path.includes('view/')){  
      const hotelIdChange = path.split('view/')[1];
      if (hotelIdChange != null){
         const { hotelId } = this.state;
         if (hotelIdChange !== hotelId) {
          this.componentWillMount();
         } 
      }
    }
  }

  onHotelStoreChange(action) {
    const hotelList = HotelListStore.getAll();
    this.setState({ hotelList });
  }

  onAuthStoreChange() {
    const isLoggedIn = AuthStore.isLoggedIn();
    this.setState({ isLoggedIn });
  }

  handleActive(e) {
    const { createHotelInfo } = this.state;
    const updateStatus = JSON.parse(JSON.stringify(createHotelInfo));
    const isActive = e.target.checked;
    const activeMsg = isActive ? 1 : 2;
    updateStatus['status'] = activeMsg;
    this.setState({ createHotelInfo: updateStatus });
  }

  handleSetupDropdown(id){
    const { addedMeetingRooms } = this.state;
    const selectedRoom = addedMeetingRooms.map((item) => {
      const selectedItem = item;
      if (item.id !== id) {
        selectedItem['showSetupDropdown'] = false;
        return selectedItem;
      }
      selectedItem['showSetupDropdown'] = !selectedItem['showSetupDropdown'];
      return selectedItem;
    });
    this.setState ({ addedMeetingRooms : selectedRoom });
  }


  setAddedAssets(meeting_rooms) {
    const { addedAssets } = this.state;
    meeting_rooms.map(meeting_room =>{
      const photos=[];
      const videos=[];
      const links=[];
      const DiagramOf3D=[];
      if (meeting_room.assets.length >=1) {
        meeting_room.assets.map(asset => {
          if (asset.type.id === 3) {
            photos.push({
              content: (<div>
                          <div>
                            <img src={asset.file} alt={'photos-'+asset.order} className="assets-photos"/>
                            <div 
                              className="i-block close-button-delete primary-background-color"
                              onClick={()=>{this.deleteAssets(asset.id, meeting_room.id)}}
                            >
                              <Button className="primary-background-color align-delete-button">x</Button>
                            </div>
                          </div>
                          <div className="file-name">
                            <span>{asset.file.split('/')[asset.file.split('/').length-1]}</span>
                          </div>
                        </div>),
              file: asset.file,
              classes:['bigger', 'lmargin27'],
              assetId:asset.id,
              orderId:asset.order,
              meeting_room: meeting_room.id,        
            })
          }
          else if (asset.type.id === 13) {
            videos.push({
              content: (<div>
                          <div className='no-drag'>
                            <div id={`video-${asset.id}`} className='no-drag' onClick={()=>{this.videoPlay(asset.file)}}/>
                             <div>
                              <VideoThumbnail
                                videoUrl={asset.file}
                                thumbnailHandler={(thumbnail) => {this.getThumbnailFile(thumbnail, asset.id)}}
                              />
                              <div 
                                className="i-block close-button-delete primary-background-color"
                                onClick={()=>{this.deleteAssets(asset.id, meeting_room.id)}}
                              >
                                <Button className="primary-background-color align-delete-button">x</Button>
                              </div>
                             </div>
                              <div className='file-name no-drag'>
                                <span>{asset.file.split('/')[asset.file.split('/').length-1]}</span>
                              </div>
                            </div>
                          </div>),
              file: asset.file,
              classes:['bigger', 'lmargin27'],
              assetId:asset.id,
              orderId:asset.order,
              meeting_room: meeting_room.id,
            })
          } else if (asset.type.id === 4) {
            links.push({
              assetId:asset.id,
              meeting_room: meeting_room.id,
              orderId:asset.order,
              url:asset.url,
            });
          } else if (asset.type.id === 2 || asset.type.id === 7 || asset.type.id === 8 || asset.type.id === 11 || asset.type.id === 12) {
            DiagramOf3D.push({
              content: (<div className='no-drag'>
                         <div>
                          <img src={asset.type.id === 2 ? asset.file : "dist/img/upload_assets/3d-icon.png"} alt={'photos-'+asset.order} className="assets-photos no-drag"/>
                          <div 
                            className="i-block close-button-delete primary-background-color"
                            onClick={()=>{this.deleteAssets(asset.id, meeting_room.id)}}
                          >
                            <Button className="primary-background-color align-delete-button">x</Button>
                          </div>
                         </div>
                          <div className="file-name no-drag">
                            <span>{asset.file.split('/')[asset.file.split('/').length-1]}</span>
                          </div>
                        </div>),
              file: asset.file,
              classes:['bigger', 'lmargin27'],
              assetId:asset.id,
              orderId:asset.order,
              meeting_room: meeting_room.id,
            });
          }

        })
      }
      addedAssets.push({
        id:meeting_room.id,
        photos:photos,
        videos:videos,
        links:links,
        DiagramOf3D:DiagramOf3D,
      });
    });
    this.setState({addedAssets})
  }

  selectChain(e) {
    const selectedChainId = parseInt(e.target.value, 10);
    const { createHotelInfo, hotelList, errorMsg } = this.state;
    const { chainList } = hotelList;
    const selectChain = chainList.filter(chain => chain.id === selectedChainId)[0];
    HotelListAction.getAvailableSetup(selectChain.setup_types, hotelList.setUpImages, res => {
      if (res.length > 1) {
        this.setState({ availableSetups: res });
      } else {
        this.setState({
          availableSetups: [],
          setupMsg: 'This chain does not have any available setup.',
        });
      }
    });
    this.setState ({ selectedChain: selectChain });
    createHotelInfo['hotel_chain'] = selectedChainId;
    createHotelInfo['city'] = selectChain.city;
    createHotelInfo['users'] = [];
    this.checkErrorMsg(errorMsg, createHotelInfo);
    this.setState({ createHotelInfo });
  }

  updateInputs(type, e) {
    let { createHotelInfo, createdHotel } = this.state;
    createHotelInfo = JSON.parse(JSON.stringify(createHotelInfo));
    let { errorMsg } = this.state;
    if (type === 'country') {
      createHotelInfo[type] = e;
    } else if (type === 'distance_to_airport' || type === 'capacity') {
      const { value } = e.target;
      createHotelInfo[type] = parseInt(value, 10);
    }
    // else if (type === 'capacity_in_feet' || type === 'capacity') {
    //   const { value } = e.target;
    //   const capacityVal = parseFloat(value, 10);
    //   createHotelInfo[type] = capacityVal || '';
    //   if (type === 'capacity') {
    //     createHotelInfo['capacity_in_feet'] = capacityVal ? (capacityVal * 10.764).toFixed(2) : '';
    //   } else {
    //     createHotelInfo['capacity'] = capacityVal ? (capacityVal * 0.0929).toFixed(2) : '';
    //   }
    // }
    else {
      const { value } = e.target;
      createHotelInfo[type] = value;
    }
    if (type === 'name') {
      this.setState({
        isClickedCreateHotel: false,
        whichRoomSaveBtnClicked: null,
      });
    }
    this.checkErrorMsg(errorMsg, createHotelInfo);
    this.setState({ createHotelInfo });
  }

  checkErrorMsg(errorMsg, createHotelInfo) {
    errorMsg = errorMsg.map((required) => {
      if (!createHotelInfo[required] || createHotelInfo[required] === '' || createHotelInfo[required] === null) {
        return required;
      }
      return '';
    });
    errorMsg = errorMsg.filter(err => err !== '');
    this.setState({ errorMsg });
  }

  addFacility(e) {
    const { facilityInfo } = this.state;
    const params = {
      tempId: facilityInfo.length + 1,
      name: '',
      placeholder: '',
    };
    facilityInfo.push(params);
    this.setState({ facilityInfo });
  }

  updateFacility(id, e) {
    let { facilityInfo } = this.state;
    facilityInfo = facilityInfo.map((facility) => {
      const updateFacility = facility;
      if (updateFacility.tempId === id) {
        updateFacility['name'] = e.target.value;
        updateFacility['edit'] = facility.id ? true : false;
        return updateFacility;
      }
      return updateFacility;
    });
    this.setState({ facilityInfo });
  }

  closeAlert(msg) {
    HotelListAction.getNotificationMsg(msg, 'remove');
  }

  submitHotel(meetingRooms, type = null, callback) {
    const {
      facilityInfo, addedMeetingRooms, createHotelInfo, meetingRoomDetails, createdHotel,
    } = this.state;
    const updateFields = ['hotel_chain', 'name', 'status', 'address', 'zipcode', 'city', 'country', 'email_address', 'phone',
      'custom_domain', 'distance_to_airport', 'capacity', 'facebook_url', 'tripadvisor_url', 'instagram_url',
    ];
    if (createdHotel.hasOwnProperty('id')) {
      const updateHotel = JSON.parse(JSON.stringify(createHotelInfo));
      Object.keys(updateHotel).filter((info) => {
        if (!updateFields.includes(info)) {
          delete updateHotel[info];
        }
      });
      // delete updateHotel.users;
      const updateCreatedHotel = JSON.parse(JSON.stringify(createdHotel));
      // delete updateCreatedHotel.users;
      const isHotelChange = !(Helpers.isEquivalent(updateHotel, updateCreatedHotel));
      if (isHotelChange) {
        this.setState({ updatebuttonDisable: true });
        HotelListAction.getNotificationMsg('Saving...', 'add');
        HotelListAction.updateHotel(createdHotel.id, createHotelInfo, (updatedHotel) => {
          HotelListAction.getNotificationMsg('Saving...', 'remove');
          this.setState({
            createdHotel: updatedHotel,
            updatebuttonDisable: false,
          });
        });
      }
      const addedFacilities = [];
      HotelListAction.createFacility(createdHotel.id, facilityInfo, (facility) => {
        addedFacilities.push(facility);
        this.setState({ facilityInfo: addedFacilities });
      });
      this.createMeetingRoom(createdHotel.id, meetingRooms, (success) => {
        if (success) {
          if (!type) {
            this.setState({ meetingRoomDetails: [] });
          }
          if (callback) {
            callback(true);
          }
        }
      });
    } else {
      this.setState({ updatebuttonDisable: true });
      HotelListAction.getNotificationMsg('Saving...', 'add');
      HotelListAction.createHotel(createHotelInfo, (res) => {
        if (res) {
          HotelListAction.getNotificationMsg('Saving...', 'remove');
          this.setState({
            createdHotel: res,
            updatebuttonDisable: false,
          });
        }
        console.log("create facilities init");
        
        const addedFacilities = [];
        HotelListAction.createFacility(res.id, facilityInfo, (facility) => {
          addedFacilities.push(facility);
          this.setState({
            facilityInfo: addedFacilities,
          });
        });
        console.log("create facilities finish");
        
        this.createMeetingRoom(res.id, meetingRooms, (success) => {
          if (success) {
            if (!type) {
              this.setState({ meetingRoomDetails: [] });
            }
            if (callback) {
              callback(true);
            }
          }
        });
        console.log("create metting room finish");
        
      });
    }
  }

  notificationMsg(msg) {
    HotelListAction.getNotificationMsg(msg, 'add');
    setTimeout(() => {
      HotelListAction.getNotificationMsg(msg, 'remove');
    }, 5000);
  }
  
  validateUrl(urlFields, createHotelInfo) {
    let urlErrorMsg = '';
    const pattern = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;
    
    urlFields.map(urlField=>{
      if (createHotelInfo[urlField] && typeof(createHotelInfo[urlField]) !== 'undefined' && createHotelInfo[urlField] !== '') {
        let fieldName = '';
        if (urlField === 'custom_domain') {
          fieldName = 'Website'
        } else if (urlField === 'facebook_url') {
          fieldName = 'Facebook profile'
        } else if (urlField === 'instagram_url') {
          fieldName = 'Instagram profile'
        } else {
          fieldName = 'TripAdvisor profile'
        }
        const isUrl = pattern.test(createHotelInfo[urlField]);
        const isStarttWithProtocal = (createHotelInfo[urlField].startsWith('http://') || createHotelInfo[urlField].startsWith('https://'));
        if (!isStarttWithProtocal) {
          urlErrorMsg = urlErrorMsg === '' ? ('Please enter the valid '+fieldName) : urlErrorMsg+', '+fieldName+''
        } else if (!isUrl) {
          urlErrorMsg = urlErrorMsg === '' ? ('Please enter the valid '+fieldName) : urlErrorMsg+', '+fieldName+''
        }
      }
    });
    return urlErrorMsg;
  }

  createHotel(meetingRooms, isClickedCreateHotel, type = null, callback) {
    const { facilityInfo, addedMeetingRooms } = this.state;
    const { createHotelInfo } = this.state;
    const {
      custom_domain, facebook_url, instagram_url, tripadvisor_url,
    } = createHotelInfo;
    const urlFields = ['custom_domain', 'facebook_url', 'instagram_url', 'tripadvisor_url'];
    let zipcodeValidationMsg = '';
    let phoneValidationMsg = '';
    zipcodeValidationMsg = createHotelInfo.hasOwnProperty('zipcode') && createHotelInfo['zipcode'] !== '' && (!/^\d+$/.test(createHotelInfo['zipcode'])) ? 'Please enter the valid zip code' : '';
    phoneValidationMsg = createHotelInfo.hasOwnProperty('phone') && createHotelInfo['phone'] !== '' && (!/^[\d\.\-]+$/.test(createHotelInfo['phone'])) ? 'Please enter the valid phone' : '';
    this.setState({ isClickedCreateHotel });
    const requiredFields = ['name', 'hotel_chain', 'capacity', 'users', 'email_address', 'address'];
    let emailFormatMsg;
    let errorMsg = requiredFields.map((required) => {
      if (!createHotelInfo[required] || createHotelInfo[required] === '' || createHotelInfo[required] === null) {
        return required;
      }
      if (required === 'email_address' && createHotelInfo[required] !== ''
        && (!createHotelInfo[required].match(/^([a-zA-Z0-9_.+-])+@(([a-zA-Z0-9-])+.)+([a-zA-Z0-9]{2,4})+$/))) {
        emailFormatMsg = 'Please enter the valid email';
        return required;
      }
      return '';
    });
    const createHotel = Object.keys(createHotelInfo).filter(info => createHotelInfo[info] === 'http://');
    createHotel.forEach((element) => {
      delete createHotelInfo[element];
    });
    const urlValidationMsg = this.validateUrl(urlFields, createHotelInfo);
    const validationMsgs = [urlValidationMsg, zipcodeValidationMsg, phoneValidationMsg];
    errorMsg = errorMsg.filter(err => err !== '');
    this.setState({ errorMsg });
    if (emailFormatMsg) {
      this.notificationMsg(emailFormatMsg);
    }
    // Create hotel if there is no errors
    if (errorMsg.length >= 1) {
      const msg = 'Please fill the required fields.';
      this.notificationMsg(msg);
    } else if ((urlValidationMsg !== '') || (zipcodeValidationMsg !== '') || (phoneValidationMsg !== '')) {
      validationMsgs.map((validationMsg) => {
        if (validationMsg !== '') {
          this.notificationMsg(validationMsg);
        }
      });
    } else {
      const address = `${createHotelInfo['address'].replace(/ /g, '+')}`;
      HotelBrandAction.getGeometryLocation(address, (GeometryLocation) => {
        if (GeometryLocation.status === 'OK') {
          const location = GeometryLocation.results[0].geometry.location.lat+', '+GeometryLocation.results[0].geometry.location.lng;
          createHotelInfo['location'] = location;
          this.submitHotel(meetingRooms, type, callback);
        } else {
          const msg = 'Invalid Address. Please check';
          this.notificationMsg(msg);
        }
      });
    }
  }

  addMeetingRoom() {
    const { meetingRoomDetails, addedMeetingRooms } = this.state;
    const meetingRoomId =  meetingRoomDetails.length + addedMeetingRooms.length + 1;
    const meetingRoom = {
      tempId: meetingRoomId,
      name: '',
      dimensions: '',
      measurement: 'meters',
      ceiling_height: '',
      surface_meters: 0,
      floor: 0,
      assets: [],
      room_setups: [],
      services: [],
    };
    meetingRoomDetails.push(meetingRoom);
    this.setState({
      meetingRoomDetails,
      isClickedCreateHotel: false,
      whichRoomSaveBtnClicked: null,
    });
  }

  removeMeetingRoom(room) {
    let { meetingRoomDetails, addedAssets } = this.state;
    meetingRoomDetails = meetingRoomDetails.filter(meetingRoom => meetingRoom.tempId !== room.tempId);
    if (room.hasOwnProperty('id') && room.hasOwnProperty('type') && room.type === 'edit') {
      delete room.type;
      // delete room.tempId;
      if (room.dimensions) {
        let dimension = room.dimensions.replace(/ /g, '');
        dimension = dimension.toLowerCase().includes('*')
          ? dimension.toLowerCase().split('*') : dimension.toLowerCase().split('x');
        dimension = dimension.map(dim => {
          dim = `${dim}${room.measurement === 'meters' ? 'm' : (room.measurement === 'feet' ? 'sq.ft.' : '')}`;
          return dim;
        });
        dimension = dimension.join(' x ');
        room.dimensions = dimension;
      }
      // addedMeetingRooms.push(room);
    } else {
      addedAssets = addedAssets.filter(assets => assets.id !== room.id);
    }
    this.setState({ meetingRoomDetails, addedAssets });
  }

  roomSetupUpdate(id, setup, e) {
    let { meetingRoomDetails, availableSetups, viewPage } = this.state;
    let { value } = e.target;
    value = parseInt(value, 10);
    meetingRoomDetails = meetingRoomDetails.map((room) => {
      const updateRoom = room;
      if (updateRoom.tempId === id) {
        const selectedSetup = updateRoom.room_setups.length >= 1
          && updateRoom.room_setups.filter(room_setup => room_setup.type && (room_setup.type === setup.id)).length >= 1;
        if (selectedSetup) {
          updateRoom['room_setups'] = updateRoom.room_setups.map((updateSetupCapacity) => {
            if ((setup.id === updateSetupCapacity.type)) {
              updateSetupCapacity.capacity = isNaN(value) ? '0' : value;
              return updateSetupCapacity;
            }
            return updateSetupCapacity;
          });
        } else {
          const setupInfo = {
            // id: updateRoom['room_setups'].length + 1,
            capacity: isNaN(value) ? '0' : value,
            type: setup.id,
          };
          updateRoom['room_setups'].push(setupInfo);
        }
        return updateRoom;
      }
      return updateRoom;
    });
    this.setState({ meetingRoomDetails });
  }

  updateRoomInputs(id, e, setup = {}) {
    const { name, value } = e.target;
    let { meetingRoomDetails } = this.state;
    meetingRoomDetails = meetingRoomDetails.map((room) => {
      const updateRoom = room;
      if (updateRoom.tempId === id) {
        if (name === 'measurement') {
          updateRoom[name] = e.target.checked ? 'feet' : 'meters';
        } else if (name === 'surface_meters' || name === 'ceiling_height') {
          updateRoom[name] = isNaN(parseInt(value, 10)) ? '' : parseInt(value, 10);
        } else {
          updateRoom[name] = value;
        }
        return updateRoom;
      }
      return updateRoom;
    });
    this.setState({ meetingRoomDetails });
  }

  createMeetingRoom(hotelId, meetingRooms, callback) {
    const {
      availableSetups, selectedFloorId, viewPage,
    } = this.state;
    let { addedMeetingRooms, allFloorMeetingRooms } = this.state;
    if (meetingRooms.length >= 1) {
      const roomRequiredFields = ['name', 'surface_meters'];
      let successValidation = true;
      if (selectedFloorId === 'all') {
        successValidation = false;
        const msg = 'Please select the Floor';
        this.notificationMsg(msg);
        return null;
      }
      meetingRooms = meetingRooms.map((room) => {
        const newRoomSetup = availableSetups.map((setup) => {
          if (room.room_setups.length >= 1 && room.room_setups.filter(room_setup => room_setup.type === setup.id).length >= 1) {
            const params = room.room_setups.filter(room_setup => room_setup.type === setup.id)[0];
            params.capacity = parseInt(params.capacity, 10) || 0;
            params['type'] = params.type;
            params['config'] = {};
            params['furnitures'] = [];
            return params;
          } else if (setup.type !== 'custom') {
            const params = {
              type: setup.id,
              capacity: 0,
              config: {},
              furnitures: [],
            };
            return params;
          } else {
            return '';
          }
        });
        if (viewPage) {
          room.status = room.status === 'Disabled' ? 2 : 1;
        }
        room.room_setups = newRoomSetup.filter(newRoom => newRoom !== '');
        if (selectedFloorId) {
          room.floor = selectedFloorId;
        }
        roomRequiredFields.map((required) => {
          if (required === 'surface_meters' && room[required] <= 0) {
            successValidation = false;
            const msg = "Please enter the required fields";
            this.notificationMsg(msg);
            return null;
          } else if (!room[required] || room[required] === '' || room[required] === null) {
            successValidation = false;
            const msg = "Please enter the required fields";
            this.notificationMsg(msg);
            return null;
          }
        });
        if (successValidation) {
          this.setState({ updatebuttonDisable: true });
          room.buttonDisable = true;
        }
        return room;
      });
      if (successValidation) {
        this.setState({ meetingRoomDetails: meetingRooms });
        HotelListAction.getNotificationMsg('Saving...', 'add');
        HotelListAction.createMeetingRoom(hotelId, meetingRooms, (res) => {
          this.setState({ updatebuttonDisable: false });
          HotelListAction.getNotificationMsg('Saving...', 'remove');
          addedMeetingRooms = addedMeetingRooms.filter(addedRoom => addedRoom.id !== res.id);
          allFloorMeetingRooms = allFloorMeetingRooms.filter(addedRoom => addedRoom.id !== res.id);
          console.log('addedMeetingRooms', addedMeetingRooms);
          addedMeetingRooms.push(res);
          allFloorMeetingRooms.push(res);
          this.setState({ addedMeetingRooms, allFloorMeetingRooms });
          if (res && callback) {
            callback(true);
          }
        });
      }
    }
  }

  saveRoom(room) {
    this.setState({
      whichRoomSaveBtnClicked: room.tempId,
      isClickedCreateHotel: false,
    });
    let {
      meetingRoomDetails, addedMeetingRooms, createdHotel, createHotelInfo,
    } = this.state;
    console.log('meetingRoomDetails', meetingRoomDetails);
    // addedMeetingRooms.push(room);
    this.setState({ addedMeetingRooms });
    if (createdHotel.hasOwnProperty('id')) {
      // meetingRoomDetails = meetingRoomDetails.filter(meetingRoom => meetingRoom.id !== room.id);
      // addedMeetingRooms.push(room);
      // this.setState({
      //   meetingRoomDetails, addedMeetingRooms,
      // });
      this.createMeetingRoom(createdHotel.id, [room], (res) => {
        if (res) {
          meetingRoomDetails = meetingRoomDetails.filter(meetingRoom => meetingRoom.tempId !== room.tempId);
          this.setState({ meetingRoomDetails });
        }
      });
    } else {
      this.createHotel([room], false, 'single_room', (success) => {
        if (success) {
          meetingRoomDetails = meetingRoomDetails.filter(meetingRoom => meetingRoom.tempId !== room.tempId);
          this.setState({ meetingRoomDetails });
        }
      });
    }
  }

  showMultiMedia(isDisplayMediaPopUp, RoomId) {
    this.setState({
      showMultiMediaPopUp: isDisplayMediaPopUp,
      whichRoomAssetPopUp: RoomId,
      editUrl: '',
      isDisplayUrlInputField: false,
      assestType: 'photos',
    });
    setTimeout(() => {
      const myDropzone = new Dropzone('div#my-document-upload-zone', {
        url: ' ',
        paramName: 'file',
        acceptedFiles: ".png,.jpeg,.jpg",
        accept: (file, done) => {
          this.setState({ dragImage: false });
          const uploadZone = $('#my-document-upload-zone');
          console.log('ACCEPT: ', file);
          done();
        },
        uploadMultiple: false,
        parallelUploads: 1,
        previewTemplate: document.querySelector('#file-upload-template') && document.querySelector('#file-upload-template').innerHTML,
        thumbnailWidth: 10,
        thumbnailHeight: 10,
        thumbnailMethod: 'contain',
        autoProcessQueue: false, // upload all files at once when submit button is clicked
      });

      myDropzone.on('error', (file, message, xhr) => {
        if (xhr == null) myDropzone.removeFile(file); // Remove the unsupported files
        const { assestType, videosTypes } = this.state;
        let msg = '';
        if ( assestType === 'photos' ) {
          msg = 'Unsupported file format. Please upload only .png, .jpeg files';
        }else if ( assestType === 'videos' ) {
          msg = 'Unsupported file format. Please upload only '+videosTypes.split(',').join(', ')+' files';
        } else if ( assestType === 'DiagramOf3D' ) {
          msg = 'Unsupported file format. Please upload only .bin, .gltf, .png files';
        } 
        this.notificationMsg(msg);
      });

      myDropzone.on('removedfile', (file) => {
        const uploadZone = $('#my-document-upload-zone');
        this.setState({ acceptedFiles: myDropzone.getAcceptedFiles() });
        if (myDropzone.files.length <= 0) {
          console.log('REMOVED: ', file);
          this.setState({ uploadedFile: false });
        }
      });
      myDropzone.on('addedfile', (file) => {
        this.setState({ uploadedFile: true });
        const { assestType, photosTypes, videosTypes, typesOf3D } = this.state;

        if ( (assestType === 'photos') && (photosTypes.indexOf(file.type) !== -1)) {
          this.uploadedMedia(RoomId);
        } else if ((assestType === 'videos') && (videosTypes.indexOf(file.type) !== -1)) {
          this.uploadedMedia(RoomId);
        } else if ((assestType === 'DiagramOf3D') && (typesOf3D.indexOf(file.name.split('.')[file.name.split('.').length-1]) !== -1)) {
          this.uploadedMedia(RoomId);
        }

        console.log('ADDED: ', file);
      });
      this.setState({ myDropzone });
    }, 2000);
  }

  closeMultiMedia(close, roomId) {
    const { myDropzone } = this.state;
    myDropzone.removeAllFiles( true );
    this.setState({ showMultiMediaPopUp:close });
  }

  handleAddLink () {
    this.setState({isDisplayUrlInputField: true});
  }

  addedLinks (e, roomId) {
    const {value} = e.target;
    this.setState ({editUrl:value})
    clearTimeout(this.timer);
    this.timer = setTimeout(() => {
      this.uploadedMedia(roomId);
    }, 2000);
  }

  changeAssetType (assestType) {
    const { myDropzone, videosTypes} = this.state;
    myDropzone.removeAllFiles( true );
    if ( assestType === 'photos' ) {
      myDropzone.hiddenFileInput.accept = ".png,.jpeg,.jpg";
      myDropzone.options.acceptedFiles = ".png,.jpeg,.jpg";
    } else if (assestType === 'videos') {
      myDropzone.hiddenFileInput.accept = videosTypes;
      myDropzone.options.acceptedFiles = videosTypes;
    } else if (assestType === 'DiagramOf3D') {
      myDropzone.hiddenFileInput.accept = ".png,.bin,.gltf";
      myDropzone.options.acceptedFiles = ".png,.bin,.gltf";
    }
    this.setState({ assestType, myDropzone });
  }

  videoPlay (URL) {
    this.setState({isVideoPlay:true, videoURL:URL})

  }

  getThumbnailFile (thumbnailUrl, assetId) {
    document.getElementById("video-"+assetId).innerHTML=`<img src=${thumbnailUrl} class="assets-photos"/><img src="dist/img/upload_assets/play.png" class="play"/>`;

  }

  closeVideo () {
    this.setState({isVideoPlay:false})
  }

  onSort (sortedList, roomId) {
    const {addedAssets, assestType} = this.state;
    if (assestType === 'photos') {
      addedAssets.map((asset)=> {
        if (asset.id === roomId) {
          asset.photos = sortedList;
          asset.photos.map(each => {
            each.orderId = each.rank+1;
          })
        }
      });
      sortedList.map((eachPhotos) => {
        HotelListAction.updatePhotoOrder(eachPhotos.meeting_room, eachPhotos.assetId, eachPhotos.rank+1);
      })
      this.setState({addedAssets});
    }
  }

  copyLinks(assetId) {
    const copyText = $('#link-'+assetId);
    copyText.select();
    document.execCommand("copy");
  }

  updatelinks(e, roomId, assetId) {
    const { addedAssets } = this.state;
    addedAssets.map((asset)=> {
      if (asset.id === roomId) {
        asset.links.map((eachLink)=> {
          if (eachLink.assetId === assetId) {
            eachLink.url = e.target.value;
          }
        });
      }
    });
    this.setState({addedAssets});
    clearTimeout(this.timer);
    this.timer = setTimeout(() => {
      this.setState({ uploadNotificationMsg: ['Updating...'], notificationType: 'update'});
      HotelListAction.updatelinks(roomId, assetId, $('#link-'+assetId).val(), (res) =>{
        this.setState({ uploadNotificationMsg: [], notificationType: null});
        console.log('Update Response', res);
        if (!res.hasOwnProperty('id')) {
          this.notificationMsg(Object.values(res)[0][0]);
          return false;
        }
      });
    }, 500);
  }
  deleteAssets (assetId, RoomId) {
    const { addedAssets, assestType } = this.state;
    this.setState({ uploadNotificationMsg: ['Deleting...'], notificationType: 'update'});
    HotelListAction.deleteAssets(assetId,RoomId, (res)=>{
      if (res) {
        addedAssets.map(room => {
          if (room.id === RoomId) {
            const deleteassets = room[assestType].filter(assets=>assets.assetId === assetId)[0]
            const index = room[assestType].indexOf(deleteassets);
            if (index > -1) {
              room[assestType].splice(index, 1);
            }
          }
        });
        this.setState({ uploadNotificationMsg: [], notificationType: null});
        this.setState(addedAssets)
      }else{
        this.setState({ uploadNotificationMsg: [], notificationType: null});
      }
    }) 

  }
  uploadedMedia(roomId ){
    const { myDropzone, assestType, editUrl} = this.state;
    let { addedAssets }= this.state;
    if ( assestType === 'links' && editUrl === '')
    {
      return false;
    }
    // } else if ( assestType === 'links' && $('.edit-link-input').val() !== '' ) {
    //   const pattern = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;
    //   const isUrl = pattern.test($('.edit-link-input').val());
    //   const isStarttWithProtocal = ($('.edit-link-input').val().startsWith('http://') || $('.edit-link-input').val().startsWith('https://'));
    //   if (!isStarttWithProtocal) {
    //     this.notificationMsg("Please enter a valid URL");
    //     return false;
    //   } else if (!isUrl) {
    //     this.notificationMsg("Please enter a valid URL");
    //     return false;
    //   }
    // }
    let uploadedInfo = {};
    if ((typeof(myDropzone) !== 'undefined' && myDropzone.files.length>=1) || $('.edit-link-input').val() !== '') {
      let type;
      const currentRoomAssets = addedAssets.filter(asset=>asset.id === roomId);
      let order;
      if ( assestType === 'photos' ) {
        type = 3;
        order = currentRoomAssets.length >= 1 ? 
          (currentRoomAssets[0].photos.length < myDropzone.files.length ? (myDropzone.files.length) : currentRoomAssets[0].photos.length + myDropzone.files.length) 
            : myDropzone.files.length;
      } else if( assestType === 'videos' ) {
        type = 13;
        order = currentRoomAssets.length >= 1 ? 
          (currentRoomAssets[0].videos.length < myDropzone.files.length ? (myDropzone.files.length) : currentRoomAssets[0].videos.length + myDropzone.files.length) 
            : myDropzone.files.length;
      } else if (assestType === 'links') {
        type = 4;
        order = currentRoomAssets.length >= 1 ? currentRoomAssets[0].links.length + 1 : 1;
      } else if (assestType === 'DiagramOf3D') {
        order = currentRoomAssets.length >= 1 ? 
          (currentRoomAssets[0].DiagramOf3D.length < myDropzone.files.length ? (myDropzone.files.length) : currentRoomAssets[0].DiagramOf3D.length + myDropzone.files.length) 
            : myDropzone.files.length;
        const fileExt = (myDropzone.files[myDropzone.files.length-1].name).split('.')[(myDropzone.files[myDropzone.files.length-1].name).split('.').length-1];
        const fileName = myDropzone.files[myDropzone.files.length-1].name.replace(/[^a-z]/gi, '').toLowerCase()
        if (fileExt === 'png') {
          type = 2;
        } else if ( fileExt === 'bin' &&  fileName.indexOf('meetingroom') !== -1) {
          type = 8
        } else if ( fileExt === 'bin' &&  fileName.indexOf('floorplan') !== -1 ) {
          type = 12
        } else if ( fileExt === 'gltf' && fileName.indexOf('meetingroom') !== -1 ) {
          type = 7
        } else if ( fileExt === 'gltf' && fileName.indexOf('floorplan') !== -1 ) {
          type = 11
        }
      }
      uploadedInfo['meeting_room'] = roomId;
      uploadedInfo['type'] = type;
      uploadedInfo['order'] = order;
      if (assestType === 'links') {
        uploadedInfo['url'] = editUrl; 
      } else {
        uploadedInfo['file'] = myDropzone.files[myDropzone.files.length-1];
      }
      this.setState({ uploadNotificationMsg: ['Uploading...'], notificationType: 'update'});
      const src = HotelListAction.getAssetsUploadResponse(uploadedInfo,(res)=> {
        this.setState({ uploadNotificationMsg: [], notificationType: null});
        if (!res.hasOwnProperty('id')) {
          this.notificationMsg(Object.values(res)[0][0]);
          return false;
        }
        $('.edit-link-input').val('');
        const isRoomIdPresent = addedAssets.filter(asset=>asset.id === roomId);
        if (isRoomIdPresent.length >=1 ) {
          if ( assestType === 'photos' && res.type === 3) {
            addedAssets.map((asset)=> {
              if (asset.id === roomId) {
                asset.photos.push({
                  content: (<div>
                              <img src={res.file} alt={'photos-'+res.order} className="assets-photos"/>
                              <div 
                                className="i-block close-button-delete primary-background-color"
                                onClick={()=>{this.deleteAssets(res.id, res.meeting_room)}}
                                >
                                <Button className="primary-background-color align-delete-button">x</Button>
                              </div>
                              <div className="file-name">
                                <span>{res.file.split('/')[res.file.split('/').length-1]}</span>
                              </div>
                            </div>),
                  file:res.file,
                  classes:['bigger', 'lmargin27'],
                  assetId:res.id,
                  orderId:res.order,
                  meeting_room: res.meeting_room,
                });
              }
            });
            this.setState({addedAssets});
          } else if ( assestType === 'videos' && res.type === 13) {
              addedAssets.map((asset)=> {
                if (asset.id === roomId) {
                  asset.videos.push({
                    content: (<div>
                              <div className='no-drag'>
                                <div id={`video-${res.id}`} className='no-drag' onClick={()=>{this.videoPlay(res.file)}}/>
                                  <VideoThumbnail
                                    videoUrl={res.file}
                                    thumbnailHandler={(thumbnail) => {this.getThumbnailFile(thumbnail, res.id)}}
                                  />
                                  <div 
                                    className="i-block close-button-delete-video primary-background-color"
                                    onClick={()=>{this.deleteAssets(res.id, res.meeting_room)}}
                                    >
                                    <Button className="primary-background-color align-delete-button">x</Button>
                                  </div>
                                  <div className='file-name no-drag'>
                                    <span>{res.file.split('/')[res.file.split('/').length-1]}</span>
                                  </div>
                                </div>
                                </div>),
                    file:res.file,
                    classes:['bigger', 'lmargin27'],
                    assetId:res.id,
                    orderId:res.order,
                    meeting_room: res.meeting_room,
                  });
                }
              });
              this.setState({addedAssets});
          } else if (assestType === 'links' && res.type === 4) {
            addedAssets.map((asset)=> {
              if (asset.id === roomId) {
                asset.links.push({
                  assetId:res.id,
                  meeting_room: res.meeting_room,
                  orderId:res.order,
                  url:res.url,
                });
              }
            });
            this.setState({addedAssets, editUrl:''});
          } else if (assestType === 'DiagramOf3D' && (res.type === 2 || res.type === 8 || res.type === 7 || res.type === 12 || res.type === 11)) {
            addedAssets.map((asset)=> {
              if (asset.id === roomId) {
                asset.DiagramOf3D.push({
                  content: (<div className='no-drag'>
                             <div>
                              <img src={res.type === 2 ? res.file : "dist/img/upload_assets/3d-icon.png"} alt={'photos-'+res.order} className="assets-photos no-drag"/>
                              <div 
                                className="i-block close-button-delete primary-background-color"
                                 onClick={()=>{this.deleteAssets(res.id, res.meeting_room)}}
                              >
                                <Button className="primary-background-color align-delete-button">x</Button>
                              </div>
                             </div>
                             <div className="file-name no-drag">
                              <span>{res.file.split('/')[res.file.split('/').length-1]}</span>
                             </div>
                            </div>),
                file:res.file,
                classes:['bigger', 'lmargin27'],
                assetId:res.id,
                orderId:res.order,
                meeting_room: res.meeting_room,
                });
              }
            });
            this.setState({addedAssets});
          }
        } else {
          if ( assestType === 'photos' && res.type === 3) {
            addedAssets.push({
              id:roomId,
              photos:[{
                content: (<div>
                           <div>
                            <img src={res.file} alt={'photos-'+res.order} className="assets-photos"/>
                            <div 
                              className="i-block close-button-delete primary-background-color"
                              onClick={()=>{this.deleteAssets(res.id, res.meeting_room)}}
                            >
                              <Button className="primary-background-color align-delete-button">x</Button>
                            </div>
                           </div>
                           <div className="file-name">
                            <span>{res.file.split('/')[res.file.split('/').length-1]}</span>
                           </div>
                          </div>),
                file:res.file,
                classes:['bigger', 'lmargin27'],
                assetId:res.id,
                orderId:res.order,
                meeting_room: res.meeting_room,
              }],
              videos:[],
              links:[],
              DiagramOf3D:[],
            })
          } else if(assestType === 'videos' && res.type === 13) {
            addedAssets.push({
              id:roomId,
              photos:[],
              videos:[{
                content: (<div>
                            <div className='no-drag'>
                              <div id={`video-${res.id}`} className='no-drag' onClick={()=>{this.videoPlay(res.file)}}/>
                                <VideoThumbnail
                                  videoUrl={res.file}
                                  thumbnailHandler={(thumbnail) => {this.getThumbnailFile(thumbnail, res.id)}}
                                />
                                <div 
                                  className="i-block close-button-delete-video primary-background-color"
                                  onClick={()=>{this.deleteAssets(res.id, res.meeting_room)}}
                                >
                                  <Button className="primary-background-color align-delete-button">x</Button>
                                </div>
                                <div className='file-name no-drag'>
                                  <span>{res.file.split('/')[res.file.split('/').length-1]}</span>
                                </div>
                              </div>
                            </div>),
                file:res.file,
                classes:['bigger', 'lmargin27'],
                assetId:res.id,
                orderId:res.order,
                meeting_room: res.meeting_room,
              }],
              links:[],
              DiagramOf3D:[],
            })
          } else if(assestType === 'links' && res.type === 4) {
            addedAssets.push({
              id:roomId,
              photos:[],
              videos:[],
              links:[{
                assetId:res.id,
                meeting_room: res.meeting_room,
                orderId:res.order,
                url:res.url,
              }],
              DiagramOf3D:[],
            })
          } else if (assestType === 'DiagramOf3D' && (res.type === 2 || res.type === 8 || res.type === 7 || res.type === 12 || res.type === 11)) {
            addedAssets.push({
              id:roomId,
              photos:[],
              videos:[],
              links:[],
              DiagramOf3D:[{
                content: (<div className='no-drag'>
                           <div>
                            <img src={res.type === 2 ? res.file : "dist/img/upload_assets/3d-icon.png"} alt={'photos-'+res.order} className="assets-photos no-drag"/>
                            <div 
                              className="i-block close-button-delete primary-background-color"
                              onClick={()=>{this.deleteAssets(res.id, res.meeting_room)}}
                            >
                              <Button className="primary-background-color align-delete-button">x</Button>
                            </div>
                           </div>
                           <div className="file-name no-drag">
                            <span>{res.file.split('/')[res.file.split('/').length-1]}</span>
                           </div>
                          </div>),
                file:res.file,
                classes:['bigger', 'lmargin27'],
                assetId:res.id,
                orderId:res.order,
                meeting_room: res.meeting_room,
              }],
            })
          }
          this.setState({addedAssets, editUrl:''});
        }
      });
    }

  }

  addFloor() {
    this.setState({ showFloorPopup: true });
  }

  handleClose() {
    this.setState({ showFloorPopup: false });
  }

  floorName(e) {
    const { value } = e.target;
    this.setState({ currentFloor: value });
  }

  createFloor() {
    const { currentFloor, floors } = this.state;
    if (currentFloor && currentFloor !== '') {
      const floorVal = Helpers.getFloor(currentFloor);
      const params = {
        floor: floorVal,
        name: currentFloor,
      };
      floors.push(params);
      floors.sort((a, b) => a.floor - b.floor);
      this.setState({
        floors,
        showFloorPopup: false,
        selectedFloorId: floorVal,
      });
    }
  }

  selectFloor(e) {
    const {
      floors, addedMeetingRooms, allFloorMeetingRooms, meetingRoomDetails, allEditFloorMeetingRooms,
    } = this.state;
    const { value } = e.target;
    this.setState({
      allFloorMeetingRooms: allFloorMeetingRooms.length ? allFloorMeetingRooms : addedMeetingRooms,
      allEditFloorMeetingRooms: allEditFloorMeetingRooms.length ? allEditFloorMeetingRooms : meetingRoomDetails,
    });
    if (value !== 'all') {
      const selectedFloor = floors.filter(floor => floor.floor === parseInt(value, 10));
      const selectedFloorId = selectedFloor[0].floor;
      let afterFilterMeetingRooms = JSON.parse(JSON.stringify(allFloorMeetingRooms.length ? allFloorMeetingRooms : addedMeetingRooms));
      afterFilterMeetingRooms = afterFilterMeetingRooms.filter(room => room.floor === selectedFloorId);
      let afterFilterEditMeetingRooms = JSON.parse(JSON.stringify(allEditFloorMeetingRooms.length ? allEditFloorMeetingRooms : meetingRoomDetails));
      afterFilterEditMeetingRooms = afterFilterEditMeetingRooms.filter(room => room.floor === selectedFloorId);
      this.setState({
        selectedFloorId,
        addedMeetingRooms: afterFilterMeetingRooms,
        // meetingRoomDetails: afterFilterEditMeetingRooms,
      });
    } else {
      this.setState({
        selectedFloorId: 'all',
        addedMeetingRooms: allFloorMeetingRooms.length ? allFloorMeetingRooms : addedMeetingRooms,
        // meetingRoomDetails: allEditFloorMeetingRooms.length ? allEditFloorMeetingRooms : meetingRoomDetails,
      });
    }
  }

  statusChange(roomId, e) {
    let { addedMeetingRooms, viewPage } = this.state;
    const isActive = e.target.checked;
    addedMeetingRooms = addedMeetingRooms.map((addedRoom) => {
      if (addedRoom.id === roomId) {
        addedRoom['status'] = isActive ? 1 : 2;
        addedRoom['status'] = viewPage && isActive ? 'Active' : 'Disabled';
        return addedRoom;
      }
      return addedRoom;
    });
    this.setState({ addedMeetingRooms });
    const params = {
      status: isActive ? 1 : 2,
    };
    HotelListAction.getNotificationMsg('Saving...', 'add');
    HotelListAction.roomStatusChange(roomId, params, (res)=>{
      HotelListAction.getNotificationMsg('Saving...', 'remove');
    });
  }

  editMeetingRoom(editRoom, type) {
    let {
      addedMeetingRooms, allFloorMeetingRooms, meetingRoomDetails, allEditFloorMeetingRooms,
    } = this.state;
    editRoom.tempId = addedMeetingRooms.length + meetingRoomDetails.length + 1;
    editRoom.type = type;
    if (editRoom.dimensions) {
      editRoom.measurement = editRoom.dimensions.includes('m') ? 'meters' : 'feet';
      editRoom.dimensions = editRoom.dimensions.replace(/m|sq.ft./g, '');
    }
    meetingRoomDetails = meetingRoomDetails.filter(room => room.id !== editRoom.id);
    allEditFloorMeetingRooms = allEditFloorMeetingRooms.filter(room => room.id !== editRoom.id);
    addedMeetingRooms = addedMeetingRooms.map(room => {
      if (room.id !== editRoom.id) {
        return room;
      }
      room.type = 'edit';
      return room;
    });
    meetingRoomDetails.push(editRoom);
    allEditFloorMeetingRooms.push(editRoom);
    console.log('meetingRoomDetails', meetingRoomDetails);
    allFloorMeetingRooms = allFloorMeetingRooms.filter(room => room.id !== editRoom.id);
    this.setState({
      meetingRoomDetails,
      addedMeetingRooms,
      allFloorMeetingRooms,
      allEditFloorMeetingRooms,
    });
  }

  showConfirmationPopup(room) {
    this.setState({
      isConfirmationPopup: true,
      deleteRoom : room,
    });
  }

  closeConfirmationPopup() {
    this.setState({ isConfirmationPopup: false });
  }

  deleteMeetingRoom() {
    const { deleteRoom } = this.state;
    let { addedMeetingRooms, allFloorMeetingRooms } = this.state;
    HotelListAction.deleteMeetingRoom(deleteRoom, deletedRoom => {
      if (deletedRoom) {
        addedMeetingRooms = addedMeetingRooms.filter(room => room.id !== deletedRoom.id);
        allFloorMeetingRooms = allFloorMeetingRooms.filter(room => room.id !== deletedRoom.id);
        this.setState({ addedMeetingRooms, allFloorMeetingRooms });
      }
    });
    this.setState({ isConfirmationPopup: false });
  }

  sortBySpace(sortingOrder) {
    let { addedMeetingRooms } = this.state;
    this.setState(prevState => ({
      sortingSpaceAscOrder : prevState.sortingOrder === 'room_space' ? !prevState.sortingSpaceAscOrder : true,
      sortingOrder: 'room_space',
      setupType: '',
    }), () => {
      const { sortingSpaceAscOrder } = this.state;
      HotelListAction.sort(addedMeetingRooms,'room_space', sortingSpaceAscOrder, false, addedMeetingRooms => {
        if (addedMeetingRooms) {
          this.setState({ addedMeetingRooms });
        }
      });
    });
  }

  sortByDimensions(sortingOrder) {
    let { addedMeetingRooms } = this.state;
    this.setState(prevState => ({
      sortingDimensAscOrder : prevState.sortingOrder === 'dimensions' ? !prevState.sortingDimensAscOrder : true,
      sortingOrder: 'dimensions',
      setupType: '',
    }), () => {
      const { sortingDimensAscOrder } = this.state;
      HotelListAction.sort(addedMeetingRooms, 'dimensions', sortingDimensAscOrder, false, addedMeetingRooms => {
        if (addedMeetingRooms) {
          this.setState({ addedMeetingRooms });
        }
      });
    });
  }

  sortByHeight(sortingOrder) {
    let { addedMeetingRooms } = this.state;
    this.setState(prevState => ({
      sortingHeightAscOrder : prevState.sortingOrder === 'ceiling_height' ? !prevState.sortingHeightAscOrder : true,
      sortingOrder: 'ceiling_height',
      setupType: '',
    }), () => {
      const { sortingHeightAscOrder } = this.state;
      HotelListAction.sort(addedMeetingRooms, 'ceiling_height', sortingHeightAscOrder, false, addedMeetingRooms => {
        if (addedMeetingRooms) {
          this.setState({ addedMeetingRooms });
        }
      });
    });
  }

  sortBySetup(setup) {
    let { addedMeetingRooms } = this.state;
    this.setState(prevState => ({
      sortBySetupAscOrder : prevState.setupType === setup.type.toLowerCase() ? !prevState.sortBySetupAscOrder : true,
      sortingOrder: 'setup',
      setupType: setup.type.toLowerCase(),
    }), () => {
      const { sortBySetupAscOrder } = this.state;
      HotelListAction.sort(addedMeetingRooms, 'setup', sortBySetupAscOrder, setup, addedMeetingRooms => {
        if (addedMeetingRooms) {
          this.setState({ addedMeetingRooms });
        }
      });
    });
  }

  render() {
    const {
      isRedirect, isLoggedIn, hotelList, createHotelInfo, meetingRoomDetails, availableSetups, facilityInfo,
      isMobileSidebar, addedMeetingRooms, showMultiMediaPopUp, addedAssets, assestType, whichRoomAssetPopUp,
      isVideoPlay, videoURL, selectedChain, setupMsg, errorMsg, isClickedCreateHotel, whichRoomSaveBtnClicked, 
      showFloorPopup, floors, createdHotel, isConfirmationPopup, hotelInfo, setLoader, viewPage, updatebuttonDisable,
      notificationType, uploadNotificationMsg, editUrl, isDisplayUrlInputField, sortingOrder, setupType, sortingSpaceAscOrder, 
      sortingDimensAscOrder, sortingHeightAscOrder, sortBySetupAscOrder,
    } = this.state;
    facilityInfo.sort((a, b) => {
      return a.tempId - b.tempId;
    });
    let setupAvailableMsg = 'Please select the chain to see the available setups.';
    setupAvailableMsg = setupMsg || setupAvailableMsg;
    const { chainList, notificationMsg } = hotelList;
    const background = { 'background': 'linear-gradient(175deg, #f0f0f0, #fafafa)'};
    const addFloorHeader = 'Add floor';
    if (isRedirect || !isLoggedIn) {
      return <Redirect to="/" />;
    }
    const capacityVal = (room, setup) => {
      if (room.room_setups.length >= 1) {
        const filteredSetup = room.room_setups.filter(room_setup => room_setup.type === setup.id);
        if (filteredSetup.length >= 1) {
          return filteredSetup[0].capacity;
        }
      }
      return 0;
    };
    let previewList;
    if ( assestType === 'photos' ) {
      previewList = addedAssets.filter(asset=>asset.id === whichRoomAssetPopUp).length>=1 ? addedAssets.filter(asset=>asset.id === whichRoomAssetPopUp)[0].photos : [];
    } else if ( assestType === 'videos' ) {
      previewList = addedAssets.filter(asset=>asset.id === whichRoomAssetPopUp).length>=1 ? addedAssets.filter(asset=>asset.id === whichRoomAssetPopUp)[0].videos : [];
    } else if ( assestType === 'links' ) {
      previewList = addedAssets.filter(asset=>asset.id === whichRoomAssetPopUp).length>=1 ? addedAssets.filter(asset=>asset.id === whichRoomAssetPopUp)[0].links : [];
    } else if ( assestType === 'DiagramOf3D' ) {
      previewList = addedAssets.filter(asset=>asset.id === whichRoomAssetPopUp).length>=1 ? addedAssets.filter(asset=>asset.id === whichRoomAssetPopUp)[0].DiagramOf3D : [];
    }

    previewList.sort(function(a, b){            
      if(a.orderId < b.orderId) return -1;
      if(a.orderId > b.orderId) return 1;
      return 0;
    });

    const selectFirstImage = (addedAssets, addedRoom) => {
      let firstImage;
      const selectedAsset = addedAssets.filter(addedAsset => addedAsset.id === addedRoom.id);
      const firstImageAsset = selectedAsset[0].photos.filter(each => each.orderId === 1);
      if (firstImageAsset.length) {
        firstImage = firstImageAsset[0].file;
      } else {
        firstImage = selectedAsset[0].photos[0].file;
      }
      return firstImage;
    };
    return (
      <Grid className="create-hotel-profile-container primary-color">
        {this.sideBar()}
        {
          isMobileSidebar && (
            this.mobileSideBar()
          )
        }
        <div className="content-right">
          {this.pageHeader(true, notificationType === 'update' ? uploadNotificationMsg : notificationMsg, null, notificationType)}
          <div>
            <Button
              className="back-button primary-color"
              onClick={this.context.router.history.goBack}
            >
              <ReactSVG
                wrapper="span"
                className="svg-wrapper"
                src="dist/img/event_list/back-arrow.svg"
                alt="back button"
                width="25px"
              />
              <span className="hidden-xs">&nbsp;&nbsp;Back</span>
            </Button>
          </div>
          {
            setLoader ? (
              <div className="loader-style">Loading....</div>
            ) : (
              <div>
                <div className="header-title">
                  <p className="title list-title">
                    <strong>{hotelInfo ? hotelInfo.name : 'Create hotel profile'}</strong>
                  </p>
                </div>
                <Row className="bpadding10 bottom-border">
                  <div className="title i-block tpadding10">General/Contact info</div>
                  <Button
                    className="primary-color save-btn"
                    onClick={() => { this.createHotel(meetingRoomDetails, true) }}
                    disabled={updatebuttonDisable}
                  >
                    <span className="save-text">Save</span>
                    <ReactSVG wrapper="span" className="svg-wrapper" src="dist/img/rfp_management/save.svg" alt="doc icon" />
                  </Button>
                </Row>
                <Row className="general-contact-info-container">
                  <Col className="hidden-xs hidden-sm" lg={3} md={3} sm={3}>
                    <div className="i-block add-visuals-container">
                      <ReactSVG wrapper="span" className="svg-wrapper" src="dist/img/hotel_admin/gallery.svg" alt="doc icon" />
                      <span className="lpadding10">Add visuals</span>
                    </div>
                  </Col>
                  <Col lg={9} md={9} sm={12} xs={12}>
                    <Row>
                      <Col className="align-content-top" lg={4} md={4} sm={12} xs={12}>
                        {
                          hotelInfo ? (
                            <div className="text-bold">{hotelInfo.hotel_chain.name}</div>
                          ) : (
                            <select
                              className={`select-hotel-chain align-right ${errorMsg && errorMsg.includes('hotel_chain')
                              ? 'red-border' : 'primary-border-color-light'}`}
                              onChange={this.selectChain}
                            >
                              <option selected disabled hidden>Select the chain</option>
                              {
                                chainList.map(chain => (
                                  <option
                                    value={chain.id}
                                  >
                                    {chain.name}
                                  </option>
                                ))
                              }
                            </select>
                          )
                        }
                        {
                          // <p className="text-bold">Hotel NH Collection Madrid Eurobuilding</p>
                        }
                      </Col>
                      <Col className="align-content-top" lg={4} md={3} sm={12} xs={12}>
                        <p className="visible-xs visible-sm">Hotel name</p>
                        <input
                          type="text"
                          className={`contact-text ${errorMsg && errorMsg.includes('name')
                          ? 'red-border' : 'border-hotel-logo '}`}
                          placeholder="Enter Hotel Name"
                          value={createHotelInfo.name || ''}
                          onChange={(e) => this.updateInputs('name', e)}
                        />
                      </Col>
                      <Col className="hidden-xs hidden-sm" lg={4} md={5} sm={5}> 
                        <div className="fleft i-block tmargin5">
                            <span className="text-font-12">Inactive</span></div>
                        <div className="fleft i-block lmargin5"><label className="fleft switch">
                              <input type="checkbox" onChange={this.handleActive} checked={createHotelInfo.status === 1}/>
                              <span className="slider round"></span>
                            </label></div>
                        <div className="fleft i-block tmargin5 lmargin5"><span className="fleft text-font-12"><strong>Active</strong></span></div>
                      </Col>
                    </Row>
                    <Row className="align-content-top">
                      <Col className="align-content-top text-center hidden-xs visible-sm" sm={12} xs={12}>
                        <div className="i-block add-visuals-container">
                          <ReactSVG wrapper="span" className="svg-wrapper" src="dist/img/hotel_admin/gallery.svg" alt="doc icon" />
                          <span className="lpadding10">Add visuals</span>
                        </div>
                     </Col>
                    </Row>
                    <Row className="contact-container">
                      <Col lg={4} md={4} sm={12} xs={12}>
                        <div>
                          <p>Address</p>
                          <input
                            className={`contact-text ${errorMsg && errorMsg.includes('address')
                          ? 'red-border' : 'border-hotel-logo '}`}
                            type="text"
                            value={createHotelInfo.address || ''}
                            onChange={e => this.updateInputs('address', e)}
                          />
                        </div>
                      </Col>
                      <Col className="align-content-top" lg={4} md={4} sm={4} xs={4}>
                        <div>
                          <p>Zip code</p>
                          <input
                            className="contact-text border-hotel-logo"
                            type="text"
                            value={createHotelInfo.zipcode || ''}
                            onChange={(e) => this.updateInputs('zipcode', e)}
                          />
                        </div>
                      </Col>
                      <Col className="visible-xs visible-sm align-content-top" lg={4} md={4} sm={8} xs={8}>
                        <div>
                          <p>Country/Region</p>
                          <CountryDropdown
                            className="input-brand-name primary-border-color-light tmargin10 profile-country"
                            defaultOptionLabel=""
                            onChange={e => this.updateInputs('country', e)}
                            value={createHotelInfo.country || ''}
                            name="country"
                          />
                        </div>
                      </Col>
                      <Col className="hidden-xs hidden-sm" lg={3} md={3}>
                        <div className="prelative">
                          <span className="text-bold i-block room-name">{createHotelInfo.city || ''}</span>
                        </div>
                      </Col>
                    </Row>
                    <Row className="contact-container">
                      <Col className="hidden-xs hidden-sm" lg={4} md={4} sm={9} xs={9}>
                        <div>
                          <p>Country/Region</p>
                          <CountryDropdown
                            className="input-brand-name primary-border-color-light tmargin10 profile-country"
                            defaultOptionLabel=""
                            onChange={e => this.updateInputs('country', e)}
                            value={createHotelInfo.country || ''}
                            name="country"
                          />
                        </div>
                      </Col>
                      <Col className="align-content-top" lg={4} md={4} sm={12} xs={12}>
                        <div>
                        <p>Email</p>
                        <input
                          className={`contact-text ${errorMsg && errorMsg.includes('email_address')
                          ? 'red-border' : 'border-hotel-logo '}`}
                          type="text"
                          value={createHotelInfo.email_address || ''}
                          onChange={(e) => this.updateInputs('email_address', e)}
                        />
                        {
                          // <span className="text-bold i-block room-name">{createHotelInfo.city || ''}</span>
                        }
                        </div>
                      </Col>
                      <Col className="align-content-top" lg={4} md={4} sm={4} xs={4}>
                        <div className="prelative">
                          <p>Phone</p>
                          <input
                            className="contact-text border-hotel-logo"
                            type="text"
                            value={createHotelInfo.phone || ''}
                            onChange={e => this.updateInputs('phone', e)}
                            name="phone"
                          />
                        </div>
                      </Col>
                      <Col className="align-content-top visible-xs visible-sm" sm={8} xs={8}>
                        <div>
                          <p>Distance to the airport</p>
                          <input
                            className="contact-text border-hotel-logo"
                            type="text"
                            value={createHotelInfo.distance_to_airport || ''}
                            onChange={e => this.updateInputs('distance_to_airport', e)}
                            name="distance_to_airport"
                          />
                        </div>
                      </Col>
                    </Row>
                    <Row className="contact-container">
                      <Col className="align-content-top" lg={4} md={4} sm={12} xs={12}>
                        <div>
                          <p>Website</p>
                          <input
                            className="align-text-input contact-text border-hotel-logo"
                            type="text"
                            value={createHotelInfo.custom_domain || ''}
                            onChange={e => this.updateInputs('custom_domain', e)}
                            name="custom_domain"
                          />
                        </div>
                      </Col>
                      <Col className="hidden-xs hidden-sm" lg={4} md={4} sm={4}>
                        <div>
                          <p>Distance to the airport</p>
                          <input
                            className="contact-text border-hotel-logo"
                            type="text"
                            value={createHotelInfo.distance_to_airport || ''}
                            onChange={e => this.updateInputs('distance_to_airport', e)}
                            name="distance_to_airport"
                          />
                        </div>
                      </Col>
                      <Col className="align-content-top" lg={4} md={4} sm={12} xs={12}>
                        <div>
                          <p>Capacity</p>
                          <input
                            className={`align-text-input contact-text ${errorMsg && errorMsg.includes('capacity')
                            ? 'red-border' : 'border-hotel-logo '}`}
                            type="text"
                            value={createHotelInfo.capacity || ''}
                            onChange={e => this.updateInputs('capacity', e)}
                            name="capacity"
                          />
                        </div>
                      </Col>
                    </Row>
                    <Row className="contact-container">
                      <Col className="align-content-top" lg={4} md={4} sm={12} xs={12}>
                          <Row>
                            <Col className="align-logo lpadding0 hidden-xs hidden-sm" lg={1} md={2}>
                              <img src="/dist/img/hotel_admin/facebook.svg" alt="Facebook icon" />
                            </Col>
                            <Col className="rpadding0 align-input-field" lg={11} md={10} sm={12} xs={12}>
                              <p>Facebook profile</p>
                              <input
                                className="contact-text link-text border-hotel-logo"
                                type="text"
                                value={createHotelInfo.facebook_url || ''}
                                onChange={e => this.updateInputs('facebook_url', e)}
                                name="facebook_url"
                              />
                            </Col>
                        </Row>
                      </Col>
                      <Col className="align-content-top" lg={4} md={4} sm={12} xs={12}>
                          <Row>
                           <Col className="align-logo lpadding0 hidden-xs hidden-sm" lg={1} md={2}>
                            <img src="/dist/img/hotel_admin/instagram.svg" alt="Instagram icon" />
                           </Col>
                           <Col className="rpadding0 align-input-field" lg={11} md={10} xs={12} sm={12}>
                            <p>Instagram profile</p>
                            <input
                              className="contact-text link-text border-hotel-logo"
                              type="text"
                              value={createHotelInfo.instagram_url || ''}
                              onChange={e => this.updateInputs('instagram_url', e)}
                              name="instagram_url"
                            />
                           </Col>
                          </Row>
                      </Col>
                      <Col className="align-content-top" lg={4} md={4} sm={12} xs={12}>
                        <div>
                          <p>TripAdvisor profile</p>
                          <input
                            className="contact-text link-text border-hotel-logo"
                            type="text"
                            value={createHotelInfo.tripadvisor_url || ''}
                            onChange={e => this.updateInputs('tripadvisor_url', e)}
                            name="tripadvisor_url"
                          />
                        </div>
                      </Col>
                    </Row>
                  </Col>
                </Row>
                <div className="facilities-info border-bottom-light-color">
                  <p className="title">Facilities info</p>
                </div>
                <Row className="hotel-facilities-info">
                  <Col lg={12} md={12}>
                    <Row>
                      {
                        facilityInfo.map(facility => ([
                          <Col lg={3} md={3} sm={6} xs={6}>
                            <p className="tpadding10">{`Facility 0${facility.tempId}`}</p>
                            <input
                              className="contact-text border-hotel-logo"
                              type="text"
                              value={facility.name}
                              onChange={e => this.updateFacility(facility.tempId, e)}
                              placeholder={facility.placeholder}
                            />
                          </Col>,
                        ]))
                      }
                      <Col lg={3} md={3} sm={12} xs={12}>
                        <div
                          className="add-facility pointer i-block"
                          onClick={this.addFacility}
                        >
                          <img
                            className="add-facility-img primary-background-color"
                            src="dist/img/meeting_space/icons/plus.png"
                            alt="plus"
                          />
                          <span className="add-hotel-text">&nbsp;&nbsp;Add facility</span>
                        </div>
                      </Col>
                    </Row>
                  </Col>
                </Row>
                <Row className="tpadding20">
                  <Col className="meeting-space-tab" lg={3} md={3} sm={3} xs={5}>
                    <p className="text-meeting-space hidden-xs hidden-sm"><strong>Meeting Spaces</strong></p>
                    <p className="text-meeting-space visible-xs visible-sm"><strong>Create Spaces</strong></p>
                  </Col>
                  <Col lg={9} md={9} sm={9} xs={7} className="individual-tab-toggle tpadding10 primary-color">
                    <div
                      className="fright bpadding5 pointer"
                      onClick={this.addMeetingRoom}
                    >
                      <span className="add-hotel-text">Add meeting room&nbsp;&nbsp;</span>
                      <img
                        className="add-meeting-room fright primary-background-color"
                        src="dist/img/meeting_space/icons/plus.png"
                        alt="plus"
                      />
                    </div>
                  </Col>
                </Row>
                <Row className="main-content">
                  <Row className="layout-sub-tab tpadding20">
                      <Col lg={6} md={6} sm={6} xs={6}>
                              <Button className="button-selection primary-color">
                                <span className="i-block primary-color">Capacity Chart</span>
                                <hr className="selected-tab" />
                              </Button>
                      </Col>
                      <Col lg={6} md={6} sm={6} xs={6}>
                              <Button className="button-selection primary-color">
                                <span className="i-block primary-color">
                                <ReactSVG wrapper="span" className="svg-wrapper icon-3D" src="dist/img/3d-icon.svg" />Request
                                </span>
                                <hr className="unselected-tab"/>
                              </Button>
                      </Col>
                  </Row>
                  <Row className="floor-details tpadding10 bpadding10 dflex">
                   <Col className="tpadding10" lg={4} md={4} sm={12} xs={12}>
                    
                     <Col className="align-add-button pointer" lg={2} md={2} sm={2} xs={2}
                       onClick={this.addFloor}>
                        <img
                          className="add-meeting-room fright primary-background-color"
                          src="dist/img/meeting_space/icons/plus.png"
                          alt="plus"
                        />
                     </Col>
                     <Col className="text-font16" lg={8} md={8} sm={10} xs={10}>
                        <p>Floor</p>
                        <div className="select-floor-background">
                          <select
                            className="select-floor"
                            type="text"
                            onChange={this.selectFloor}
                          >
                            {
                              floors.length >= 1 && floors.map(floor => (
                                <option value={floor.floor}>{floor.name}</option>
                              ))
                            }
                          </select>
                        </div>
                     </Col>
                    
                   </Col>
                   <Col className={`pointer tpadding25 hidden-xs hidden-sm ${sortingOrder === 'room_space' ? 'text-bold' : ''}`} lg={1} md={1}
                        onClick={() => this.sortBySpace(sortingSpaceAscOrder)}>
                     <div className="tpadding20">
                      <span className="text-font-12">M2
                        <ReactSVG 
                          wrapper="span"  
                          className={`svg-wrapper arrow-icon ${sortingOrder === 'room_space' && sortingSpaceAscOrder ? '' : 'transform'}`}
                          src="dist/img/meeting_space/icons/arrow-down.svg"/>
                      </span>
                     </div>
                   </Col>
                   <Col className={`pointer tpadding25 primary-border-left-light hidden-xs hidden-sm ${sortingOrder === 'dimensions' ? 'text-bold' : ''}`} lg={1} md={1}
                        onClick={() => this.sortByDimensions(sortingDimensAscOrder)}>
                     <div>
                      <span className="text-font-12">Room dimens.
                        <ReactSVG 
                          wrapper="span" 
                          className={`svg-wrapper arrow-icon ${sortingOrder === 'dimensions' && sortingDimensAscOrder ? '' : 'transform'}`} 
                          src="dist/img/meeting_space/icons/arrow-down.svg"/>
                      </span>
                     </div>
                   </Col>
                   <Col className={`pointer tpadding25 primary-border-left-light hidden-xs hidden-sm ${sortingOrder === 'ceiling_height' ? 'text-bold' : ''}`} lg={1} md={1}
                        onClick={() => this.sortByHeight(sortingHeightAscOrder)}>
                     <div>
                      <span className="text-font-12">Ceiling Height
                        <ReactSVG 
                          wrapper="span" 
                          className={`svg-wrapper arrow-icon ${sortingOrder === 'ceiling_height' && sortingHeightAscOrder ? '' : 'transform'}`} 
                          src="dist/img/meeting_space/icons/arrow-down.svg"/>
                      </span>
                     </div>
                   </Col>
                   
                    {
                      availableSetups && availableSetups.length > 1 ? (
                        <Col className="dflex hidden-xs hidden-sm" lg={7} md={7}>
                          {
                            availableSetups.map(availableSetup => (
                                  availableSetup.type !== 'custom' && (
                                     <Col className="i-block text-center primary-border-left-light"
                                          onClick={() => this.sortBySetup(availableSetup)}>
                                        <div className="setup-image-container bpadding10">
                                          <ReactSVG wrapper="span" className="svg-wrapper setup-image" src={availableSetup.img}/>
                                            <span className={`wrap-text tmargin10 text-font-12 text-center ${availableSetup.type.toLowerCase() === setupType ? 'text-bold' : ''}`}>{availableSetup.name}    
                                              <ReactSVG 
                                                wrapper="span" 
                                                className={`svg-wrapper arrow-icon ${sortBySetupAscOrder && availableSetup.type.toLowerCase() === setupType ? '' : 'transform'}`}
                                                src="dist/img/meeting_space/icons/arrow-down.svg"/>
                                            </span>
                                        </div>
                                     </Col>
                                  )
                              
                            ))
                          }
                        </Col>
                      ) : (
                        <Col lg={7} md={7} className="setup-available-msg hidden-xs hidden-sm">{setupAvailableMsg}</Col>
                      )
                    }
                  </Row>
                  {
                    meetingRoomDetails.length >= 1 && meetingRoomDetails.map(room => (
                      <Row className={`row-details tpadding10 ${room.type === 'edit' ? 'hide' : ''}`}>
                        <Col className="padding10" lg={3} md={3}>
                          <Col>
                            <div className="i-block">Room name</div>
                            <div className="fright visible-sm visible-xs">
                              <Button
                                className="close-button fright"
                                onClick = { () => this.removeMeetingRoom(room) }
                                disabled={room.buttonDisable}
                              >
                              <ReactSVG wrapper="span" className="svg-wrapper fright" src="dist/img/popup/close-button.svg" alt="close button"/></Button>
                            </div>
                          </Col>
                          <Col>
                            <div>
                              <input
                                className={`contact-text ${room.name === '' && (isClickedCreateHotel || (whichRoomSaveBtnClicked === room.tempId) ) ? 'red-border' : 'border-hotel-logo'}`}
                                type="text"
                                placeholder=""
                                value={room.name || ''}
                                onChange={e => this.updateRoomInputs(room.tempId, e)}
                                name="name"
                              />
                            </div>
                            
                            {
                              addedAssets.length >= 1 && addedAssets.filter(addedAsset => addedAsset.id === room.id).length >= 1 && addedAssets.filter(addedAsset => addedAsset.id === room.id)[0].photos.length >=1 ? (
                                <div>
                                  <div
                                    className="i-block edit-visuals-room-details"
                                    onClick={() => this.showMultiMedia(!showMultiMediaPopUp, room.id)}
                                  >
                                    <img className="add-visual-photo edit-visual-photo" src={selectFirstImage(addedAssets, room)} alt="add visual photo" />
                                  </div>
                                </div>
                              ) : (
                              <div
                                className="i-block text-center hidden-xs add-visuals-room-details"
                                onClick={() => this.notificationMsg('Please create the meeting room to upload the assets.')}
                              >
                                <ReactSVG wrapper="span" className="svg-wrapper" src="dist/img/hotel_admin/gallery.svg" alt="doc icon" />
                                <span className="lpadding10">Add visuals</span>
                              </div>
                            )}
                          </Col>
                        </Col>
                        { 
                          // ---- Mobile and iPad ----
                        }
                        <Col className="bpadding20 visible-sm visible-xs">
                         <Row>
                          <Col sm={4} xs={4}>
                            <div>Size
                            </div>
                            <div className="tmargin10">
                              <input
                                className={`content-text-room-details ${room.surface_meters <= 0 && (isClickedCreateHotel || (whichRoomSaveBtnClicked === room.tempId) ) ? 'red-border' : 'border-hotel-logo'}`}
                                type="text"
                                placeholder=""
                                value={room.surface_meters || ''}
                                onChange={e => this.updateRoomInputs(room.tempId, e)}
                                name="surface_meters"
                              />
                            </div>
                          </Col> 
                          <Col sm={8} xs={8}>
                           <div className="i-block fright rmargin20">
                            <div className="fleft i-block tmargin5">
                              <span className="text-font14"><strong>Meters</strong></span></div>
                            <div className="fleft i-block lmargin5">
                              <label className="fleft switch">
                                <input
                                  type="checkbox"
                                  name="measurement"
                                  checked={room.measurement === 'feet'}
                                  onChange={e => this.updateRoomInputs(room.tempId, e)}
                                />
                                <span className="slider round"></span>
                              </label>
                            </div>
                            <div className="fleft i-block tmargin5 lmargin5"><span className="fleft text-font14">Feet</span></div>
                           </div>
                          </Col>
                         </Row>
                         <Row>
                          <Col>
                            <Col className="rpadding0" sm={7} xs={7}>
                             <div>Dimensions
                             </div>
                             <div className="lpadding0 width-100 fleft i-block col-sm-5 col-xs-5">
                              <input
                                  className="width-100 content-text-room-details border-hotel-logo"
                                  type="text"
                                  value={room.dimensions || ''}
                                  onChange={e => this.updateRoomInputs(room.tempId, e)}
                                  name="dimensions"
                                />
                             </div>
                             
                            </Col>
                            <Col sm={5} xs={5}>
                             <div className="lmargin10">Ceiling  Height
                             </div>
                             <div>
                              <input
                                className="content-text-room-details lmargin10 border-hotel-logo"
                                type="text"
                                placeholder=""
                                value={room.ceiling_height || ''}
                                onChange={e => this.updateRoomInputs(room.tempId, e)}
                                name="ceiling_height"
                              />
                             </div>
                            </Col>
                          </Col>
                         </Row>
                         <hr className="color-hr" />
                        </Col>
                        <Col>
                        {
                          availableSetups && availableSetups.length > 1 ? (
                            <Col className="visible-xs visible-sm" sm={12} xs={12}>
                              {
                                availableSetups.map((availableSetup,i) => (                    
                                    availableSetup.type !== 'custom' && (
                                      <Col className="lpadding0 rpadding0" sm={6} xs={6}>
                                        <div className="bpadding10">
                                          <ReactSVG wrapper="span" className="svg-wrapper setup-image" src={availableSetup.img}/>
                                            <p className="wrap-text tmargin10 text-font-12 text-center">{availableSetup.name}</p>
                                            <div className={`text-center ${i % 2 === 0 ? 'button-left' : 'button-right'}`}>
                                              <input
                                              className="lmargin10 text-center content-text-room-details border-hotel-logo"
                                              type="text"
                                              placeholder=""
                                              value={capacityVal(room, availableSetup)}
                                              onChange={e => this.roomSetupUpdate(room.tempId, availableSetup, e)}
                                              />
                                              <img className="lmargin5" src="dist/img/meeting_space/icons/person.png"/>
                                            </div>
                                        </div>
                                      </Col>
                                    )
                                ))
                              }
                            </Col>
                          ) : (
                            <Col sm={12} xs={12} className="setup-available-msg visible-sm visible-xs">{setupAvailableMsg}</Col>
                          )
                        } 
                        </Col>
                        <Col className="bpadding10 primary-border-bottom-light visible-xs visible-sm" sm={12} xs={12}>
                          <Button
                              className="button-save primary-color primary-border-color tmargin5 rmargin10"
                              onClick={() => this.saveRoom(room)}
                              disabled={room.buttonDisable}
                            >
                            Done
                          </Button>
                        </Col>
                        { 
                          // ---- Desktop ----
                        }
                        <Col className="column-corner-radius padding10 hidden-xs hidden-sm" lg={9} md={9}>
                          <Row>
                            <div className="fleft i-block tmargin5">
                              <span className="text-font14"><strong>Meters</strong></span></div>
                            <div className="fleft i-block lmargin5">
                              <label className="fleft switch">
                                <input
                                  type="checkbox"
                                  name="measurement"
                                  checked={room.measurement === 'feet'}
                                  onChange={e => this.updateRoomInputs(room.tempId, e)}
                                />
                                <span className="slider round"></span>
                              </label>
                            </div>
                            <div className="fleft i-block tmargin5 lmargin5"><span className="fleft text-font14">Feet</span></div>
                            <Col>
                              <Button
                                className="close-button fright"
                                onClick={() => this.removeMeetingRoom(room)}
                                disabled={room.buttonDisable}
                              >
                                <ReactSVG
                                  wrapper="span"
                                  className="svg-wrapper fright"
                                  src="dist/img/popup/close-button.svg"
                                  alt="close button"
                                />
                              </Button>
                            </Col>
                          </Row>
                          <Row className="tpadding25 bpadding20 primary-border-bottom-light">
                            <Col className="lpadding0 rpadding0" lg={4} md={4}>
                             
                              <div className="fleft i-block lpadding0 width33">
                                <input
                                  className={`content-text-room-details lmargin10 ${room.surface_meters <= 0 && (isClickedCreateHotel || (whichRoomSaveBtnClicked === room.tempId) ) ? 'red-border' : 'border-hotel-logo'}`}
                                  type="text"
                                  placeholder="m"
                                  value={room.surface_meters || ''}
                                  onChange={e => this.updateRoomInputs(room.tempId, e)}
                                  name="surface_meters"
                                />
                              </div>
                              <div className="fleft lmargin5 lpadding5 i-block width33 primary-border-left-light">
                                <input
                                  className="content-text-room-details lmargin10 border-hotel-logo"
                                  type="text"
                                  placeholder="m X m X m"
                                  value={room.dimensions || ''}
                                  onChange={e => this.updateRoomInputs(room.tempId, e)}
                                  name="dimensions"
                                />
                              </div>
                              <div className="fleft lmargin5 lpadding5 i-block width33 primary-border-left-light">
                                <input
                                  className="content-text-room-details lmargin10 border-hotel-logo"
                                  type="text"
                                  placeholder="m"
                                  value={room.ceiling_height || ''}
                                  onChange={e => this.updateRoomInputs(room.tempId, e)}
                                  name="ceiling_height"
                                />
                              </div>
                             
                            </Col>
                            <Col className="lpadding0 rpadding0" lg={8} md={8}>
                              {
                                availableSetups && availableSetups.map(setup => (
                                  <div className={`fleft i-block lpadding5 width14 primary-border-left-light ${setup.type === 'custom' ? 'hide' : ''}`}>
                                    <input
                                      className="content-text-room-details fleft border-hotel-logo"
                                      type="text"
                                      placeholder=""
                                      value={capacityVal(room, setup) >= 0 ? capacityVal(room, setup):'0'}
                                      onChange={e => this.roomSetupUpdate(room.tempId, setup, e)}
                                    />
                                    <img className="tmargin5 lmargin5 fleft" src="dist/img/meeting_space/icons/person.png"/>
                                  </div>
                                ))
                              }
                              {
                                (!availableSetups || availableSetups.length < 1) && (
                                  <div className="text-center">-----</div>
                                )
                              }

                            </Col>
                          </Row>
                          <Row className="tpadding10 bpadding10">
                           <Col className="fright" lg={4} md={4} sm={12} xs={12}>
                            <Button
                              className="fright button-save primary-color primary-border-color tmargin5 rmargin10"
                              onClick={() => this.saveRoom(room)}
                              disabled={room.buttonDisable}
                            >
                              {room.buttonDisable ? 'Saving...' : 'Save'}
                            </Button>
                           </Col>
                          </Row>
                        </Col>
                      </Row>
                    ))
                  }
                  {
                    addedMeetingRooms.length >= 1 && addedMeetingRooms.map(addedRoom => ([
                      <Row className={`row-margin tpadding10 hidden-sm hidden-xs ${addedRoom.type === 'edit' ? 'hide' : ''}`}>
                        <Col className="rpadding0 lpadding0 prelative" lg={3} md={3}>
                          {
                            addedAssets.length >= 1 && addedAssets.filter(addedAsset => addedAsset.id === addedRoom.id).length >= 1 && addedAssets.filter(addedAsset => addedAsset.id === addedRoom.id)[0].photos.length >=1 ?
                            (<div>
                              <div
                                className="i-block add-visuals-room-details"
                                onClick={()=>{this.showMultiMedia(!showMultiMediaPopUp, addedRoom.id)}}
                              >
                                <img className="add-visual-photo" src={selectFirstImage(addedAssets, addedRoom)} alt="add visual photo" />
                              </div>
                              <div className="room-name-details lmargin10 rmargin10">
                                <span>{addedRoom.name}</span>
                                <span className="fright rmargin20"><ReactSVG wrapper="span" className="svg-wrapper" src="dist/img/hotelProfile/edit-multimedia.svg" /></span>
                              </div>
                            </div>) :
                            (<div
                              className="i-block add-visuals-room-details"
                              onClick={()=>{this.showMultiMedia(!showMultiMediaPopUp, addedRoom.id)}}
                            >
                             <div>
                              <ReactSVG wrapper="span" className="svg-wrapper" src="dist/img/hotel_admin/gallery.svg" alt="doc icon" />
                              <span className="lpadding10">Add visuals</span>
                             </div>
                             <div className="primary-color room-name-details-added-room">
                               <div className="align-meeting-room-name">{addedRoom.name}</div>
                             </div>
                            </div>)
                          }
                          {
                            // <img className="meeting-room-thumbnail pointer" src="dist/img/hotelProfile/image-room-setup.png" />
                          }
                        </Col>
                        <Col className="column-corner-radius lpadding0 rpadding0 tmargin10" lg={9} md={9}>
                          <Row className="primary-border-bottom-light">
                            <Col className="dflex lpadding0 rpadding0 tpadding10 bpadding5" lg={4} md={4}>
                              <div className="fleft i-block width33 text-center capacity-column">
                                <span>{addedRoom.surface_meters}</span>
                              </div>
                              <div className="fleft lmargin5 i-block width33 text-center primary-border-left-light capacity-column">
                                <span>{addedRoom.dimensions}</span>
                              </div>
                              <div className="fleft lmargin5 i-block width33 text-center primary-border-left-light capacity-column">
                                <span>{addedRoom.ceiling_height}</span>
                              </div>
                            </Col>
                            {
                              <Col className="lpadding0 rpadding0 tpadding10" lg={8} md={8}>
                                {
                                  availableSetups && availableSetups.map(setup => {
                                    const setupValue = {
                                      capacity: 0,
                                    };
                                    let setupVal = addedRoom.room_setups.filter(roomSetup => roomSetup.type.hasOwnProperty('id') ? (roomSetup.type.id === setup.id) : (roomSetup.type === setup.id))[0];
                                    if (!setupVal) {
                                      setupVal = setupValue;
                                    }
                                    return (
                                      <div
                                        className={`fleft i-block width14 text-center primary-border-left-light capacity-column ${setup.type === 'custom' ? 'hide' : ''}`}
                                      >
                                        <span>
                                          {setup.type === 'custom' ? '-' : setupVal.capacity}
                                          <img className="lmargin5" src="dist/img/meeting_space/icons/person.png" />
                                        </span>
                                      </div>
                                    )
                                  })
                                }
                              </Col>
                            }
                           </Row>
                           <Row className="tpadding10 bpadding5">
                            <Col className="fright" lg={8} md={10}>
                              <Col className="fleft i-block" lg={3} md={3}>
                                <Button
                                  className="button-edit primary-color primary-border-color rmargin10"
                                  onClick={() => this.editMeetingRoom(addedRoom, 'edit')}
                                >
                                  Edit
                                </Button>
                              </Col>
                              <Col className="fleft i-block" lg={7} md={7}>
                                <div className="fleft i-block tmargin5">
                                  <span className="text-font14"><strong>Disabled</strong></span></div>
                                  <div className="fleft i-block lmargin5">
                                    <label className="fleft switch">
                                      <input
                                        type="checkbox"
                                        checked={viewPage ? addedRoom.status !== 'Disabled' : addedRoom.status !== 2}
                                        onChange={(e) => this.statusChange(addedRoom.id, e)}
                                      />
                                      <span className="slider round"></span>
                                    </label>
                                  </div>
                                <div className="fleft i-block tmargin5"><span className="fleft text-font14">Enabled</span></div>
                              </Col>
                              <Col className="fleft i-block primary-border-left fright tmargin5" lg={2} md={2}>
                                <div
                                  onClick={() => this.showConfirmationPopup(addedRoom)}
                                >
                                  <ReactSVG
                                    wrapper="span"
                                    className="svg-wrapper fright lmargin5 pointer"
                                    src="dist/img/meeting_space/icons/delete-red@3x.svg"
                                  />
                                </div>
                              </Col>
                            </Col>
                           </Row>
                        </Col>
                      </Row>,
                      <Row className={`row-details tpadding10 hidden-sm hidden-xs ${addedRoom.type !== 'edit' ? 'hide' : ''}`}>
                        <Col className="padding10" lg={3} md={3}>
                          <Col>
                            <div className="i-block">Room name</div>
                            <div className="fright visible-sm visible-xs">
                              <Button
                                className="close-button fright"
                                onClick = { () => this.removeMeetingRoom(addedRoom) }
                                disabled={addedRoom.buttonDisable}
                              >
                              <ReactSVG wrapper="span" className="svg-wrapper fright" src="dist/img/popup/close-button.svg" alt="close button"/></Button>
                            </div>
                          </Col>
                          <Col>
                            <div>
                              <input
                                className={`contact-text ${addedRoom.name === '' && (isClickedCreateHotel || (whichRoomSaveBtnClicked === addedRoom.tempId) ) ? 'red-border' : 'border-hotel-logo'}`}
                                type="text"
                                placeholder=""
                                value={addedRoom.name || ''}
                                onChange={e => this.updateRoomInputs(addedRoom.tempId, e)}
                                name="name"
                              />
                            </div>
                            
                            {
                              addedAssets.length >= 1 && addedAssets.filter(addedAsset => addedAsset.id === addedRoom.id).length >= 1 && addedAssets.filter(addedAsset => addedAsset.id === addedRoom.id)[0].photos.length >=1 ? (
                                <div>
                                  <div
                                    className="i-block edit-visuals-room-details"
                                    onClick={() => this.showMultiMedia(!showMultiMediaPopUp, addedRoom.id)}
                                  >
                                    <img className="add-visual-photo edit-visual-photo" src={selectFirstImage(addedAssets, addedRoom)} alt="add visual photo" />
                                  </div>
                                </div>
                              ) : (
                              <div
                                className="i-block text-center hidden-xs add-visuals-room-details"
                                onClick={() => this.notificationMsg('Please create the meeting room to upload the assets.')}
                              >
                                <ReactSVG wrapper="span" className="svg-wrapper" src="dist/img/hotel_admin/gallery.svg" alt="doc icon" />
                                <span className="lpadding10">Add visuals</span>
                              </div>
                            )}
                          </Col>
                        </Col>
                        { 
                          // ---- Mobile and iPad ----
                        }
                        <Col className="bpadding20 visible-sm visible-xs">
                         <Row>
                          <Col sm={4} xs={4}>
                            <div>Size
                            </div>
                            <div className="tmargin10">
                              <input
                                className={`content-text-room-details ${addedRoom.surface_meters <= 0 && (isClickedCreateHotel || (whichRoomSaveBtnClicked === addedRoom.tempId) ) ? 'red-border' : 'border-hotel-logo'}`}
                                type="text"
                                placeholder=""
                                value={addedRoom.surface_meters || ''}
                                onChange={e => this.updateRoomInputs(addedRoom.tempId, e)}
                                name="surface_meters"
                              />
                            </div>
                          </Col> 
                          <Col sm={8} xs={8}>
                           <div className="i-block fright rmargin20">
                            <div className="fleft i-block tmargin5">
                              <span className="text-font14"><strong>Meters</strong></span></div>
                            <div className="fleft i-block lmargin5">
                              <label className="fleft switch">
                                <input
                                  type="checkbox"
                                  name="measurement"
                                  checked={addedRoom.measurement === 'feet'}
                                  onChange={e => this.updateRoomInputs(addedRoom.tempId, e)}
                                />
                                <span className="slider round"></span>
                              </label>
                            </div>
                            <div className="fleft i-block tmargin5 lmargin5"><span className="fleft text-font14">Feet</span></div>
                           </div>
                          </Col>
                         </Row>
                         <Row>
                          <Col>
                            <Col className="rpadding0" sm={7} xs={7}>
                             <div>Dimensions
                             </div>
                             <div className="lpadding0 width-100 fleft i-block col-sm-5 col-xs-5">
                              <input
                                  className="width-100 content-text-room-details border-hotel-logo"
                                  type="text"
                                  value={addedRoom.dimensions || ''}
                                  onChange={e => this.updateRoomInputs(addedRoom.tempId, e)}
                                  name="dimensions"
                                />
                             </div>
                             
                            </Col>
                            <Col sm={5} xs={5}>
                             <div className="lmargin10">Ceiling  Height
                             </div>
                             <div>
                              <input
                                className="content-text-room-details lmargin10 border-hotel-logo"
                                type="text"
                                placeholder=""
                                value={addedRoom.ceiling_height || ''}
                                onChange={e => this.updateRoomInputs(addedRoom.tempId, e)}
                                name="ceiling_height"
                              />
                             </div>
                            </Col>
                          </Col>
                         </Row>
                         <hr className="color-hr" />
                        </Col>
                        <Col>
                        {
                          availableSetups && availableSetups.length > 1 ? (
                            <Col className="visible-xs visible-sm" sm={12} xs={12}>
                              {
                                availableSetups.map((availableSetup,i) => (                    
                                    availableSetup.type !== 'custom' && (
                                      <Col className="lpadding0 rpadding0" sm={6} xs={6}>
                                        <div className="bpadding10">
                                          <ReactSVG wrapper="span" className="svg-wrapper setup-image" src={availableSetup.img}/>
                                            <p className="wrap-text tmargin10 text-font-12 text-center">{availableSetup.name}</p>
                                            <div className={`text-center ${i % 2 === 0 ? 'button-left' : 'button-right'}`}>
                                              <input
                                              className="lmargin10 text-center content-text-room-details border-hotel-logo"
                                              type="text"
                                              placeholder=""
                                              value={capacityVal(addedRoom, availableSetup)}
                                              onChange={e => this.roomSetupUpdate(addedRoom.tempId, availableSetup, e)}
                                              />
                                              <img className="lmargin5" src="dist/img/meeting_space/icons/person.png"/>
                                            </div>
                                        </div>
                                      </Col>
                                    )
                                ))
                              }
                            </Col>
                          ) : (
                            <Col sm={12} xs={12} className="setup-available-msg visible-sm visible-xs">{setupAvailableMsg}</Col>
                          )
                        } 
                        </Col>
                        <Col className="bpadding10 primary-border-bottom-light visible-xs visible-sm" sm={12} xs={12}>
                          <Button
                              className="button-save primary-color primary-border-color tmargin5 rmargin10"
                              onClick={() => this.saveRoom(addedRoom)}
                              disabled={addedRoom.buttonDisable}
                            >
                            Done
                          </Button>
                        </Col>
                        { 
                          // ---- Desktop ----
                        }
                        <Col className="column-corner-radius padding10 hidden-xs hidden-sm" lg={9} md={9}>
                          <Row>
                            <div className="fleft i-block tmargin5">
                              <span className="text-font14"><strong>Meters</strong></span></div>
                            <div className="fleft i-block lmargin5">
                              <label className="fleft switch">
                                <input
                                  type="checkbox"
                                  name="measurement"
                                  checked={addedRoom.measurement === 'feet'}
                                  onChange={e => this.updateRoomInputs(addedRoom.tempId, e)}
                                />
                                <span className="slider round"></span>
                              </label>
                            </div>
                            <div className="fleft i-block tmargin5 lmargin5"><span className="fleft text-font14">Feet</span></div>
                            <Col>
                              <Button
                                className="close-button fright"
                                onClick={() => this.removeMeetingRoom(addedRoom)}
                                disabled={addedRoom.buttonDisable}
                              >
                                <ReactSVG
                                  wrapper="span"
                                  className="svg-wrapper fright"
                                  src="dist/img/popup/close-button.svg"
                                  alt="close button"
                                />
                              </Button>
                            </Col>
                          </Row>
                          <Row className="tpadding25 bpadding20 primary-border-bottom-light">
                            <Col className="lpadding0 rpadding0" lg={4} md={4}>
                             
                              <div className="fleft i-block lpadding0 width33">
                                <input
                                  className={`content-text-room-details lmargin10 ${addedRoom.surface_meters <= 0 && (isClickedCreateHotel || (whichRoomSaveBtnClicked === addedRoom.tempId) ) ? 'red-border' : 'border-hotel-logo'}`}
                                  type="text"
                                  placeholder="m"
                                  value={addedRoom.surface_meters || ''}
                                  onChange={e => this.updateRoomInputs(addedRoom.tempId, e)}
                                  name="surface_meters"
                                />
                              </div>
                              <div className="fleft lmargin5 lpadding5 i-block width33 primary-border-left-light">
                                <input
                                  className="content-text-room-details lmargin10 border-hotel-logo"
                                  type="text"
                                  placeholder="m X m X m"
                                  value={addedRoom.dimensions || ''}
                                  onChange={e => this.updateRoomInputs(addedRoom.tempId, e)}
                                  name="dimensions"
                                />
                              </div>
                              <div className="fleft lmargin5 lpadding5 i-block width33 primary-border-left-light">
                                <input
                                  className="content-text-room-details lmargin10 border-hotel-logo"
                                  type="text"
                                  placeholder="m"
                                  value={addedRoom.ceiling_height || ''}
                                  onChange={e => this.updateRoomInputs(addedRoom.tempId, e)}
                                  name="ceiling_height"
                                />
                              </div>
                             
                            </Col>
                            <Col className="lpadding0 rpadding0" lg={8} md={8}>
                              {
                                availableSetups && availableSetups.map(setup => (
                                  <div className={`fleft i-block lpadding5 width14 primary-border-left-light ${setup.type === 'custom' ? 'hide' : ''}`}>
                                    <input
                                      className="content-text-room-details fleft border-hotel-logo"
                                      type="text"
                                      placeholder=""
                                      value={capacityVal(addedRoom, setup) >= 0 ? capacityVal(addedRoom, setup):'0'}
                                      onChange={e => this.roomSetupUpdate(addedRoom.tempId, setup, e)}
                                    />
                                    <img className="tmargin5 lmargin5 fleft" src="dist/img/meeting_space/icons/person.png"/>
                                  </div>
                                ))
                              }
                              {
                                (!availableSetups || availableSetups.length < 1) && (
                                  <div className="text-center">-----</div>
                                )
                              }

                            </Col>
                          </Row>
                          <Row className="tpadding10 bpadding10">
                           <Col className="fright" lg={4} md={4} sm={12} xs={12}>
                            <Button
                              className="fright button-save primary-color primary-border-color tmargin5 rmargin10"
                              onClick={() => this.saveRoom(addedRoom)}
                              disabled={addedRoom.buttonDisable}
                            >
                              {addedRoom.buttonDisable ? 'Saving...' : 'Save'}
                            </Button>
                           </Col>
                          </Row>
                        </Col>
                      </Row>,
                    ]))
                  }
                  { 
                    // ------ Mobile and iPad -----
                  }
                  {
                    addedMeetingRooms.length >= 1 && addedMeetingRooms.map(addedRoom => ([
                      <Row className={`row-margin tpadding10 visible-sm visible-xs ${addedRoom.type === 'edit' ? 'hide' : ''}`}>
                        <Row className="background-white">
                          {
                            addedAssets.length >= 1 && addedAssets.filter(addedAsset => addedAsset.id === addedRoom.id).length >= 1 && addedAssets.filter(addedAsset => addedAsset.id === addedRoom.id)[0].photos.length >=1 ?
                            (<div>
                              <div
                                className="i-block add-visuals-room-details"
                              >
                                <img className="add-visual-photo-mobile" src={selectFirstImage(addedAssets, addedRoom)} alt="add visual photo" />
                              </div>
                            </div>) :
                            (<div
                              className="i-block text-center border-color-primary add-visuals-room-details-view"
                            >
                              <ReactSVG wrapper="span" className="svg-wrapper" src="dist/img/hotel_admin/gallery.svg" alt="doc icon" />
                              <span className="lpadding10">Visuals</span>
                            </div>)
                          }
                          
                          <Col className="tpadding10 bpadding10 border-bottom" sm={12} xs={12}>
                            <Row>
                             <Col className="lpadding0" sm={3} xs={3}>
                               <Button 
                                 className="button-edit primary-color primary-border-color rmargin10"
                                 onClick={() => this.editMeetingRoom(addedRoom, 'edit')}
                               >
                                 Edit
                               </Button>
                             </Col>
                             <Col sm={9} xs={9}>
                              <div className="i-block tmargin5 fright">
                               <div className="fleft i-block tmargin5">
                                  <span className="text-font14"><strong>Disabled</strong></span>
                               </div>
                               <div className="fleft i-block lmargin5">
                                  <label className="fleft switch">
                                    <input
                                      type="checkbox"
                                      checked={viewPage ? addedRoom.status !== 'Disabled' : addedRoom.status !== 2}
                                      onChange={(e) => this.statusChange(addedRoom.id, e)}
                                    />
                                    <span className="slider round"></span>
                                  </label>
                               </div>
                               <div className="fleft i-block tmargin5">
                                  <span className="fleft text-font14">Enabled</span>
                               </div>
                              </div>
                             </Col>
                            </Row>
                          </Col>
                          <Col className="tpadding10 bpadding10 border-bottom" xs={12} sm={12}>
                           <Row className="dflex">
                            <Col sm={10} xs={10}>
                              <div className="align-meeting-room-name text-color-black text-font14"><strong>{addedRoom.name}</strong>
                              </div>
                              <div>
                                <div className="fleft tmargin10 i-block">
                                  <img src="dist/img/meeting_space/icons/superficie-gray-bg.png"/>
                                  <span className="lmargin10">{addedRoom.surface_meters}</span>
                                </div>
                                <div className="fleft lmargin50 i-block">
                                  <img src="dist/img/meeting_space/icons/people-black-bg.png"/>
                                </div>
                              </div>
                            </Col>
                            <Col 
                              className="primary-border-left-light" sm={2} xs={2}
                              onClick={() => this.showConfirmationPopup(addedRoom)}
                            >
                              <ReactSVG 
                              wrapper="span" 
                              className="svg-wrapper bpadding10 padding-top-delete-button fright lmargin5" 
                              src="dist/img/meeting_space/icons/delete-red@3x.svg" />
                            </Col>
                           </Row>
                          </Col>
                          <Col 
                           className="pointer tpadding10" 
                           xs={12} 
                           sm={12}
                           onClick={ () => {this.handleSetupDropdown(addedRoom.id)}}>
                           <Row>
                            <Col sm={10} xs={10}>
                             <span>Room set up</span>
                            </Col>
                            <Col sm={2} xs={2}>
                              <ReactSVG 
                              wrapper="span" 
                              className="svg-wrapper align-dropdown fright lmargin5" 
                              src="dist/img/meeting_space/icons/arrow-down.svg" 
                              />
                            </Col>
                           </Row>
                          </Col>
                          {
                            addedRoom.showSetupDropdown
                            &&(
                              <Col className="tmargin10" sm={12} xs={12}>
                               {
                                availableSetups && availableSetups.map((setup,i) => {
                                    const setupVal = addedRoom.room_setups.filter(roomSetup => roomSetup.type === setup.id)[0];
                                    return (
                                      <Col
                                        className={`fleft lpadding0 rpadding0 i-block text-center tmargin10 ${setup.type === 'custom' ? 'hide' : ''}`} 
                                        sm={6} 
                                        xs={6}
                                      >
                                        <div>
                                          <ReactSVG wrapper="span" className="svg-wrapper setup-image" src={setup.img}/>
                                          <p className="wrap-text tmargin10 text-font-12 text-center">{setup.name}</p>
                                          <div className={`text-center ${i % 2 === 0 ? 'button-left' : 'button-right'}`}>
                                            <span>
                                              {setup.type === 'custom' ? '-' : setupVal.capacity}
                                              <img className="lmargin5" src="dist/img/meeting_space/icons/person.png" />
                                            </span>
                                          </div>
                                        </div>
                                      </Col>
                                    )
                                  })
                              }
                              </Col>
                            )
                          }
                        </Row>
                      </Row>,
                      <Row className={`row-details tpadding10 visible-sm visible-xs ${addedRoom.type !== 'edit' ? 'hide' : ''}`}>
                        <Col className="padding10" lg={3} md={3}>
                          <Col>
                            <div className="i-block">Room name</div>
                            <div className="fright visible-sm visible-xs">
                              <Button
                                className="close-button fright"
                                onClick = { () => this.removeMeetingRoom(addedRoom) }
                                disabled={addedRoom.buttonDisable}
                              >
                              <ReactSVG wrapper="span" className="svg-wrapper fright" src="dist/img/popup/close-button.svg" alt="close button"/></Button>
                            </div>
                          </Col>
                          <Col>
                            <div>
                              <input
                                className={`contact-text ${addedRoom.name === '' && (isClickedCreateHotel || (whichRoomSaveBtnClicked === addedRoom.tempId) ) ? 'red-border' : 'border-hotel-logo'}`}
                                type="text"
                                placeholder=""
                                value={addedRoom.name || ''}
                                onChange={e => this.updateRoomInputs(addedRoom.tempId, e)}
                                name="name"
                              />
                            </div>
                            
                            {
                              addedAssets.length >= 1 && addedAssets.filter(addedAsset => addedAsset.id === addedRoom.id).length >= 1 && addedAssets.filter(addedAsset => addedAsset.id === addedRoom.id)[0].photos.length >=1 ? (
                                <div>
                                  <div
                                    className="i-block edit-visuals-room-details"
                                    onClick={() => this.showMultiMedia(!showMultiMediaPopUp, addedRoom.id)}
                                  >
                                    <img className="add-visual-photo edit-visual-photo" src={selectFirstImage(addedAssets, addedRoom)} alt="add visual photo" />
                                  </div>
                                </div>
                              ) : (
                              <div
                                className="i-block text-center hidden-xs add-visuals-room-details"
                                onClick={() => this.notificationMsg('Please create the meeting room to upload the assets.')}
                              >
                                <ReactSVG wrapper="span" className="svg-wrapper" src="dist/img/hotel_admin/gallery.svg" alt="doc icon" />
                                <span className="lpadding10">Add visuals</span>
                              </div>
                            )}
                          </Col>
                        </Col>
                        { 
                          // ---- Mobile and iPad ----
                        }
                        <Col className="bpadding20 visible-sm visible-xs">
                         <Row>
                          <Col sm={4} xs={4}>
                            <div>Size
                            </div>
                            <div className="tmargin10">
                              <input
                                className={`content-text-room-details ${addedRoom.surface_meters <= 0 && (isClickedCreateHotel || (whichRoomSaveBtnClicked === addedRoom.tempId) ) ? 'red-border' : 'border-hotel-logo'}`}
                                type="text"
                                placeholder=""
                                value={addedRoom.surface_meters || ''}
                                onChange={e => this.updateRoomInputs(addedRoom.tempId, e)}
                                name="surface_meters"
                              />
                            </div>
                          </Col> 
                          <Col sm={8} xs={8}>
                           <div className="i-block fright rmargin20">
                            <div className="fleft i-block tmargin5">
                              <span className="text-font14"><strong>Meters</strong></span></div>
                            <div className="fleft i-block lmargin5">
                              <label className="fleft switch">
                                <input
                                  type="checkbox"
                                  name="measurement"
                                  checked={addedRoom.measurement === 'feet'}
                                  onChange={e => this.updateRoomInputs(addedRoom.tempId, e)}
                                />
                                <span className="slider round"></span>
                              </label>
                            </div>
                            <div className="fleft i-block tmargin5 lmargin5"><span className="fleft text-font14">Feet</span></div>
                           </div>
                          </Col>
                         </Row>
                         <Row>
                          <Col>
                            <Col className="rpadding0" sm={7} xs={7}>
                             <div>Dimensions
                             </div>
                             <div className="lpadding0 width-100 fleft i-block col-sm-5 col-xs-5">
                              <input
                                  className="width-100 content-text-room-details border-hotel-logo"
                                  type="text"
                                  value={addedRoom.dimensions || ''}
                                  onChange={e => this.updateRoomInputs(addedRoom.tempId, e)}
                                  name="dimensions"
                                />
                             </div>
                             
                            </Col>
                            <Col sm={5} xs={5}>
                             <div className="lmargin10">Ceiling  Height
                             </div>
                             <div>
                              <input
                                className="content-text-room-details lmargin10 border-hotel-logo"
                                type="text"
                                placeholder=""
                                value={addedRoom.ceiling_height || ''}
                                onChange={e => this.updateRoomInputs(addedRoom.tempId, e)}
                                name="ceiling_height"
                              />
                             </div>
                            </Col>
                          </Col>
                         </Row>
                         <hr className="color-hr" />
                        </Col>
                        <Col>
                        {
                          availableSetups && availableSetups.length > 1 ? (
                            <Col className="visible-xs visible-sm" sm={12} xs={12}>
                              {
                                availableSetups.map((availableSetup,i) => (                    
                                    availableSetup.type !== 'custom' && (
                                      <Col className="lpadding0 rpadding0" sm={6} xs={6}>
                                        <div className="bpadding10">
                                          <ReactSVG wrapper="span" className="svg-wrapper setup-image" src={availableSetup.img}/>
                                            <p className="wrap-text tmargin10 text-font-12 text-center">{availableSetup.name}</p>
                                            <div className={`text-center ${i % 2 === 0 ? 'button-left' : 'button-right'}`}>
                                              <input
                                              className="lmargin10 text-center content-text-room-details border-hotel-logo"
                                              type="text"
                                              placeholder=""
                                              value={capacityVal(addedRoom, availableSetup)}
                                              onChange={e => this.roomSetupUpdate(addedRoom.tempId, availableSetup, e)}
                                              />
                                              <img className="lmargin5" src="dist/img/meeting_space/icons/person.png"/>
                                            </div>
                                        </div>
                                      </Col>
                                    )
                                ))
                              }
                            </Col>
                          ) : (
                            <Col sm={12} xs={12} className="setup-available-msg visible-sm visible-xs">{setupAvailableMsg}</Col>
                          )
                        } 
                        </Col>
                        <Col className="bpadding10 primary-border-bottom-light visible-xs visible-sm" sm={12} xs={12}>
                          <Button
                              className="button-save primary-color primary-border-color tmargin5 rmargin10"
                              onClick={() => this.saveRoom(addedRoom)}
                              disabled={addedRoom.buttonDisable}
                            >
                            Done
                          </Button>
                        </Col>
                        { 
                          // ---- Desktop ----
                        }
                        <Col className="column-corner-radius padding10 hidden-xs hidden-sm" lg={9} md={9}>
                          <Row>
                            <div className="fleft i-block tmargin5">
                              <span className="text-font14"><strong>Meters</strong></span></div>
                            <div className="fleft i-block lmargin5">
                              <label className="fleft switch">
                                <input
                                  type="checkbox"
                                  name="measurement"
                                  checked={addedRoom.measurement === 'feet'}
                                  onChange={e => this.updateRoomInputs(addedRoom.tempId, e)}
                                />
                                <span className="slider round"></span>
                              </label>
                            </div>
                            <div className="fleft i-block tmargin5 lmargin5"><span className="fleft text-font14">Feet</span></div>
                            <Col>
                              <Button
                                className="close-button fright"
                                onClick={() => this.removeMeetingRoom(addedRoom)}
                                disabled={addedRoom.buttonDisable}
                              >
                                <ReactSVG
                                  wrapper="span"
                                  className="svg-wrapper fright"
                                  src="dist/img/popup/close-button.svg"
                                  alt="close button"
                                />
                              </Button>
                            </Col>
                          </Row>
                          <Row className="tpadding25 bpadding20 primary-border-bottom-light">
                            <Col className="lpadding0 rpadding0" lg={4} md={4}>
                             
                              <div className="fleft i-block lpadding0 width33">
                                <input
                                  className={`content-text-room-details lmargin10 ${addedRoom.surface_meters <= 0 && (isClickedCreateHotel || (whichRoomSaveBtnClicked === addedRoom.tempId) ) ? 'red-border' : 'border-hotel-logo'}`}
                                  type="text"
                                  placeholder="m"
                                  value={addedRoom.surface_meters || ''}
                                  onChange={e => this.updateRoomInputs(addedRoom.tempId, e)}
                                  name="surface_meters"
                                />
                              </div>
                              <div className="fleft lmargin5 lpadding5 i-block width33 primary-border-left-light">
                                <input
                                  className="content-text-room-details lmargin10 border-hotel-logo"
                                  type="text"
                                  placeholder="m X m X m"
                                  value={addedRoom.dimensions || ''}
                                  onChange={e => this.updateRoomInputs(addedRoom.tempId, e)}
                                  name="dimensions"
                                />
                              </div>
                              <div className="fleft lmargin5 lpadding5 i-block width33 primary-border-left-light">
                                <input
                                  className="content-text-room-details lmargin10 border-hotel-logo"
                                  type="text"
                                  placeholder="m"
                                  value={addedRoom.ceiling_height || ''}
                                  onChange={e => this.updateRoomInputs(addedRoom.tempId, e)}
                                  name="ceiling_height"
                                />
                              </div>
                             
                            </Col>
                            <Col className="lpadding0 rpadding0" lg={8} md={8}>
                              {
                                availableSetups && availableSetups.map(setup => (
                                  <div className={`fleft i-block lpadding5 width14 primary-border-left-light ${setup.type === 'custom' ? 'hide' : ''}`}>
                                    <input
                                      className="content-text-room-details fleft border-hotel-logo"
                                      type="text"
                                      placeholder=""
                                      value={capacityVal(addedRoom, setup) >= 0 ? capacityVal(addedRoom, setup):'0'}
                                      onChange={e => this.roomSetupUpdate(addedRoom.tempId, setup, e)}
                                    />
                                    <img className="tmargin5 lmargin5 fleft" src="dist/img/meeting_space/icons/person.png"/>
                                  </div>
                                ))
                              }
                              {
                                (!availableSetups || availableSetups.length < 1) && (
                                  <div className="text-center">-----</div>
                                )
                              }

                            </Col>
                          </Row>
                          <Row className="tpadding10 bpadding10">
                           <Col className="fright" lg={4} md={4} sm={12} xs={12}>
                            <Button
                              className="fright button-save primary-color primary-border-color tmargin5 rmargin10"
                              onClick={() => this.saveRoom(addedRoom)}
                              disabled={addedRoom.buttonDisable}
                            >
                              {addedRoom.buttonDisable ? 'Saving...' : 'Save'}
                            </Button>
                           </Col>
                          </Row>
                        </Col>
                      </Row>,
                    ]))
                  }
                  {
                    addedMeetingRooms.length < 1 && meetingRoomDetails.length < 1 && (
                      <div className="empty-meetingroom-msg text-center">No meeting rooms added.</div>
                    )
                  }
                  {
                    // ---- Desktop ----
                  }
                  {
                    showMultiMediaPopUp && (

                      <Row className="home-container multimedia-image-container primary-color" style={background}>
                        <Row>
                            <Col>
                              <Button
                                className="close-button fright"
                                onClick={()=>{this.closeMultiMedia(false, whichRoomAssetPopUp, 'cancel')}}
                              >
                              <ReactSVG wrapper="span" className="svg-wrapper fright" src="dist/img/popup/close-button.svg" alt="close button"/></Button>
                            </Col>
                        </Row>
                        <Row className="row-content">
                          <span className="primary-color heading margin"><strong>Multimedia</strong></span>
                          <span className="block">Manage here all multimedia files of your meeting room.</span>
                        </Row>
                        <Row className="tmargin20 multimedia-tab">
                          <Col xs={3} sm={3} md={3} lg={3} className="text-center lpadding0 pointer">
                            <div
                              className="tmargin20"
                              onClick={()=>{this.changeAssetType('photos')}}
                            >
                              Photos
                            </div>
                            {assestType === 'photos' && <div className="asset-type primary-background-color"></div>}
                          </Col>
                          <Col xs={3} sm={3} md={3} lg={3} className="text-center pointer">
                            <div
                              className="tmargin20"
                              onClick={()=>{this.changeAssetType('videos')}}
                            >
                              Videos
                            </div>
                            {assestType === 'videos' && <div className="asset-type primary-background-color"></div>}
                          </Col>
                          <Col xs={3} sm={3} md={3} lg={3} className="text-center pointer">
                            <div
                              className="tmargin20"
                              onClick={()=>{this.changeAssetType('links')}}
                            >
                              Links
                          </div>
                            {assestType === 'links' && <div className="asset-type primary-background-color"></div>}
                          </Col>
                          <Col xs={3} sm={3} md={3} lg={3} className="text-center rpadding0 pointer">
                            <div
                              className="tmargin20"
                              onClick={()=>{this.changeAssetType('DiagramOf3D')}}
                            >
                              3D diagrams
                            </div>
                           {assestType === 'DiagramOf3D' && <div className="asset-type primary-background-color"></div>}
                          </Col>
                        </Row>
                        { assestType === 'photos' && (<Row className="configure-info">
                            <Col sm={2} md={1} lg={1}>
                              <ReactSVG wrapper="span" className="svg-wrapper fright" src="dist/img/media-img.svg" alt="close button"/>
                            </Col>
                            <Col sm={10} md={11} lg={11}>
                              Configure the order of the images by drag and drop.
                            </Col>
                          </Row>)
                        }
                        <Row className="photo-list">
                          <Col sm={12} md={12} lg={12} >
                          {addedAssets.length>=1 &&
                            <div className="drog-files">
                              <DragSortableList
                                items={previewList}
                                moveTransitionDuration={0.3}
                                dropBackTransitionDuration={0.3}
                                placeholder={assestType === 'photos'? 'drop here' : ''}
                                onSort={(e)=>this.onSort(e,whichRoomAssetPopUp)}
                                type="grid"
                              />
                            </div>
                          }
                          </Col>
                        </Row>
                      <Row className="photos-container" style={assestType === 'links' ? {'display':'none'}:{'display':'block'}}>
                          <Col sm={12} md={12} lg={12} className="upload-section">
                          <div id="my-document-upload-zone" />
                              <div id="file-upload-template">
                                <div className="dz-preview dz-file-preview">
                                  <div className="dz-details">
                                    <img src="" data-dz-thumbnail />
                                    <span data-dz-name className="file-dz-name" />
                                    (
                                      <span data-dz-size />
                                    )
                                    <span data-dz-remove className="dz-remove pointer">✘</span>
                                    <span className="dz-error-message" data-dz-errormessage />
                                  </div>
                                <div className="dz-progress">
                                  <span className="dz-upload" data-dz-uploadprogress />
                                </div>
                              </div>
                          </div>
                          <div className="drag-upload-file">
                            <ReactSVG wrapper="div" className="svg-wrapper text-center" src="dist/img/upload-icon.svg" alt="Upload Icon" />
                            <span className="block tmargin20 font16 text-center">
                              Drag and drop files here
                              <br />
                              or click upload
                            </span>
                          </div>
                          </Col>
                          <Col sm={12} md={12} lg={12}>
                            <Button
                              className="bt btn-default color-white primary-background-color multimedia-btn fright"
                              onClick={()=>{this.closeMultiMedia(false, whichRoomAssetPopUp, 'done')}}
                            >
                              Done
                            </Button>
                            <Button
                              className="bt btn-default multimedia-btn fright"
                              onClick={()=>{this.closeMultiMedia(false, whichRoomAssetPopUp, 'cancel')}}
                            >
                              Cancel
                            </Button>
                          </Col>
                        </Row>
                      { assestType === 'links' && (<Row><Row className="under-line">
                        <Col sm={12} md={12} lg={12}>
                         <Row>
                          <Col lg={1} md={1} sm={1}>
                            <ReactSVG 
                            wrapper="span" 
                            className="svg-wrapper" 
                            src="dist/img/hotelProfile/hand-icon.svg" 
                            alt="Link" 
                            />
                          </Col>
                          <Col lg={11} md={11} sm={11}>
                            <span> Add link to your resources, both on-line or cloud files.</span>
                          </Col>
                         </Row>
                        </Col>
                        <Col className="tmargin20" sm={12} md={12} lg={12}>
                          <Row>
                           <Col lg={1} md={1} sm={1}>
                            <ReactSVG 
                            wrapper="span" 
                            className="svg-wrapper" 
                            src="dist/img/hotelProfile/hand-icon.svg" 
                            alt="Link" 
                            />
                           </Col>
                           <Col lg={11} md={11} sm={11}>
                            <span> Videos, matterport tours[...] are also valid.</span>
                           </Col>
                          </Row>
                        </Col>
                      </Row>
                      { previewList.length >=1 &&  previewList.map(each=>(
                        <Row className="add-link">
                          <span className="link-text">Link</span>
                          <div className="added-link">
                         <ReactSVG 
                            wrapper="span" 
                            className="svg-wrapper" 
                            src="dist/img/link-icon.svg" 
                            alt="Link"
                            onClick={()=>{this.copyLinks(each.assetId)}} 
                          />
                          <input
                            type="text"
                            className="link-input"
                            id={"link-"+each.assetId}
                            onChange={(e) =>{this.updatelinks(e,whichRoomAssetPopUp,each.assetId)}}
                            value={each.url}
                          />
                          <ReactSVG
                            onClick={()=>{this.deleteAssets(each.assetId, whichRoomAssetPopUp)}}
                            wrapper="span"
                            className="svg-wrapper align-delete-button-links pointer"
                            src="dist/img/meeting_space/icons/delete-red@3x.svg"
                          />
                          </div>
                        </Row>))
                      }
                      <Row className="bmargin20">
                        <div 
                          className="added-link tmargin25 pointer"
                        >
                          <div 
                            onClick={()=>this.handleAddLink(whichRoomAssetPopUp)}
                          >
                            <img className="add-image primary-background-color" src="dist/img/meeting_space/icons/plus.png" width="30px"/>
                            &nbsp;&nbsp;&nbsp;Add link
                            </div>
                        </div>
                      </Row>
                      { isDisplayUrlInputField && 
                        <Row className="add-link">
                          <span className="link-text">Link</span>
                          <div>
                            <input
                              type="text"
                              className="primary-border-color-light edit-link-input"
                              value={editUrl}
                              onChange = {(e) =>{this.addedLinks(e,whichRoomAssetPopUp)}}
                            />
                          </div>
                        </Row>
                      }
                        <Row>
                          <Col sm={12} md={12} lg={12}>
                            <Button
                              className="bt btn-default multimedia-btn color-white primary-background-color fright"
                              onClick={()=>{this.closeMultiMedia(false, whichRoomAssetPopUp, 'done')}}
                            >
                              Done
                            </Button>
                            <Button
                              className="bt btn-default multimedia-btn fright"
                              onClick={()=>{this.closeMultiMedia(false, whichRoomAssetPopUp, 'cancel')}}
                            >
                              Cancel
                            </Button>
                          </Col>
                        </Row>
                      </Row>)}
                      </Row>
                    )
                  }
                  { isVideoPlay &&
                    (<Row className="home-container videoPlayerPpUP primary-color" style={background}>
                      <Row>
                        <Col>
                          <Button
                            className="close-button fright"
                            onClick={()=>{this.closeVideo()}}
                          >
                          <ReactSVG wrapper="span" className="svg-wrapper fright" src="dist/img/popup/close-button.svg" alt="close button"/></Button>
                        </Col>
                      </Row>
                      <Row>
                        <Player
                          ref="player"
                          autoPlay
                          playsInline
                          poster="/assets/poster.png"
                          src={videoURL}
                        />
                      </Row>
                    </Row>)
                  }
                </Row>
                <Row className="bpadding20">
                  <Col className="fright tmargin20" lg={4} md={4} sm={12} xs={12}>
                    <Button
                      className="button-save fright primary-background-color rmargin10 color-white"
                      onClick={() => { this.createHotel(meetingRoomDetails, true) }}
                      disabled={updatebuttonDisable}
                    >
                      {(createdHotel.hasOwnProperty('id') || viewPage) ? 'Save' : 'Create'}
                    </Button>
                  </Col>
                </Row>
              </div>
            )
          }
        </div>
        {
          showFloorPopup && (
            <LightBox
              handleClose={this.handleClose}
              handleSubmit={this.createFloor}
              message="Floor name"
              inputField
              placeholder="E.g. First Floor"
              handleChange={this.floorName}
              header={addFloorHeader}
              successText="Done"
              failureText="Cancel"
              showModal={showFloorPopup}
            />
          )
        }
        {
          isConfirmationPopup && (
            <LightBox
              handleClose={this.closeConfirmationPopup}
              handleSubmit={this.deleteMeetingRoom}
              message="Are you sure you want to discard this meeting room?"
              header="Discard meeting room"
              successText="Discard"
              failureText="Cancel"
              showModal={isConfirmationPopup}
            />
          )
        }
      </Grid>
    )
  }
}

CreateHotelProfile.contextTypes = {
  router: PropTypes.object,
};

export default CreateHotelProfile;