import StoreInterface from './StoreInterface';
import MeetingSpaceConstants from '../constants/MeetingSpaceConstants';

class RFPManagementStore extends StoreInterface {
  constructor() {
    super({
    	data: {},
    	selectedRooms: [],
    	guestInfo: [],
    	contact: {},
    	eventInfo: {},
    	eventStatus: {},
    	selectedEventId: null,
      selectedUuId: null,
    	selectedEventInfo: {},
    	selectedEventHotelInfo: {},
      hotelMeetingRooms:[],
    	setupTypes:[],
    	attachedFiles: [],
    	meetingRoomInfo: [],
    	historyInfo: {},
    	searchVal: '',
    	eventTypes:{},
      hotelListDetails: [],
      allHotelList: [],
      selectedPage: 1,
      allEventHistoryInfo : [],
      userInfoOfLatestHistory : {},
      selectedHotel: {},
      activeCount:0,
      pastCount:0,
      activityPrograms: [],
      notificationMsg: [],
      save3D: false,
      dropdown:{
        1 : 'Requested',
        2 : 'Rooms Blocked',
        3 : 'Contract Pending',
        4 : 'Canceled',
        5 : 'Closed',
        6 : 'Draft',
        7 : 'Contract Signed',
      },
    }, 'hotelEventInfo');
  }

  getUserEventStatus(status){
  	this._store.eventStatus = status;
    status.results.map((status,i) => {
      const statusId = status.id;
      status['event_status'] = this._store.dropdown[statusId];
    })
    
    this._store.eventStatus = status;
  }

  getSelectedEventInfo(selectedEvents){
  	this._store.selectedEventInfo = selectedEvents;
    const statusId = selectedEvents.status.id;
    selectedEvents.status['event_status'] = this._store.dropdown[statusId];
  }

  getHistoryInfo(historyInfo){
  	const orderedHistoryInfo = historyInfo.results.sort(function(a, b){
															    var keyA = new Date(a.date),
															        keyB = new Date(b.date);
															    // Compare the 2 dates
															    if(keyA < keyB) return -1;
															    if(keyA > keyB) return 1;
															    return 0;
																});
  	
  	this._store.historyInfo = orderedHistoryInfo;
  }

  getAllHistoryInfo(allEventHistory){
  	
  	allEventHistory.map(eventHistory =>{
  		const	orderedEventHistory = eventHistory.results.sort(function(a, b){
														    var keyA = new Date(a.date),
														        keyB = new Date(b.date);
														    // Compare the 2 dates
														    if(keyA < keyB) return 1;
														    if(keyA > keyB) return -1;
														    return 0;
															});
  	eventHistory['orderedEventHistory'] = orderedEventHistory;
  	})

  	this._store.allEventHistoryInfo = allEventHistory;
  
  }


  getContactInfo(contactDetails){
  	this._store.contact = contactDetails;
  }

  getAttachedFiles(attachedFiles){
  	this._store.attachedFiles = attachedFiles.results;
  }

  addNotification(action) {
    const { notificationMsg } = this._store;
    const { msg, val } = action;
    const index = notificationMsg.indexOf(msg);
    if (val === 'remove' && index > -1) {
      notificationMsg.splice(index, 1);
    } else if (val === 'add') {
      if ( index > -1) {
        notificationMsg.splice(index, 1);
      }
      notificationMsg.push(msg);
    }
    this._store.notificationMsg = notificationMsg;
  }

  uploadingFile(action) {
    const attachedFiles = this._store.attachedFiles.filter(file => file.id === action.file.id);
    if (attachedFiles.length < 1) {
      this._store.attachedFiles.push(action.file);
    }
  }

  removeUploadedFile(action) {
    const attachedFiles = this._store.attachedFiles.filter(file => file.id !== action.attachmentId);
    this._store.attachedFiles = attachedFiles;
  }

  getGuestInfo(guestDetails){
  	this._store.guestInfo = guestDetails;
    const guestInfoDetails = guestDetails.map((guest,i) => {
       guest['tempID'] = i + 1;
    }); 
  }

  getUserSelectedEventHotelInfo(selectedEventsHotel){
  	this._store.selectedEventHotelInfo = selectedEventsHotel;
    const hotelMeetingRooms = JSON.parse(JSON.stringify(selectedEventsHotel.meeting_rooms));
    hotelMeetingRooms.map((meetingRooms) => {
      const setupTypes = meetingRooms.room_setups.map((setup) => {
         let setupNameUpdated = setup.type.name.toLowerCase();
         setupNameUpdated = setupNameUpdated === 'u-shape' ? 'ushape' : setupNameUpdated;
         setup.type.name = setupNameUpdated;
      });
    });
    this._store.allHotelMeetingRooms = JSON.parse(JSON.stringify(hotelMeetingRooms));
    this._store.hotelMeetingRooms = hotelMeetingRooms.filter(room => room.status === 'Active');
  }

  getSelectedEventId(selectedEventId, selectedUuId){
  	this._store.selectedEventId = selectedEventId;
    this._store.selectedUuId = selectedUuId;
  }

  getSetupTypes(availableSetup){
  	this._store.setupTypes = availableSetup.results;
  }

  getEventTypes(eventTypes){
  	this._store.eventTypes = eventTypes;
  }

  saveActivitydetails(selectedActivityInfos){
  	this._store.selectedRooms = selectedActivityInfos;
  }

  saveMeetingRoomInfo(meetingRoomInfo){
  	this._store.meetingRoomInfo.push(meetingRoomInfo);
  }

  eventSearch() {
  	const eventList = JSON.parse(JSON.stringify(this._store.eventInfo));
    if (this._store.data.hasOwnProperty('results') && this._store.data.results.length >= 1) {
      const searchedEvent = this._store.data.results.filter(item =>
        item.name.toLowerCase().indexOf(this._store.searchVal.toLowerCase()) >= 0
      );
      eventList.results = searchedEvent;
      this._store.eventInfo = eventList;
    }
  }

  updateAddons(action){
    let clickedDropDown = 'meetingRoomInfo';
    if (action.type === 'services'){
      clickedDropDown = 'addon';
    }
    const createdEvent = this._store.selectedRooms.map((room) => {
      const selectedItem = JSON.parse(JSON.stringify(room));
      const updateSetup = selectedItem.setup.map((roomConfig) => {
        const roomEventType = JSON.parse(JSON.stringify(roomConfig));
        if (selectedItem.id === action.room.id && roomEventType.setup_id === action.setup.setup_id) {
          roomEventType[clickedDropDown] = roomEventType.hasOwnProperty(clickedDropDown) ? !roomEventType[clickedDropDown] : true;
          return roomEventType;
        }
        roomEventType[clickedDropDown] = clickedDropDown === 'addon' ? false :  true;
        return roomEventType;
      });
      selectedItem['setup'] = updateSetup;
      return selectedItem;
    });
    this._store.selectedRooms = createdEvent;
  }

  getUserEventInfo(eventInfo, eventType) {
  	this._store.eventInfo = eventInfo;
    // if (!this._store.activeCount && eventType === 'active') {
    //   this._store.activeCount = eventInfo.count
    //   console.log('this._store.activeCount',this._store.activeCount);
    // }

    eventInfo.results.map((results,i) => {
      const statusId = results.status.id;
      results.status['event_status'] = this._store.dropdown[statusId];
    });
    
  	this._store.data = eventInfo;
  	this.eventSearch();
  }

  getPastEventInfo(pastEventInfo, activeCount) {
    this._store.pastCount = pastEventInfo.count
    this._store.activeCount = activeCount;
    pastEventInfo.results.map((results,i) => {
      const statusId = results.status.id;
      results.status['event_status'] = this._store.dropdown[statusId];
    });
  }

  updateActivityName(params) {
    const {
      roomId, setupId, name, type, edit, id,
    } = params;
    const createdEvent = this._store.selectedRooms.map((room) => {
      const selectedItem = room;
      if (room.id !== roomId) {
        return selectedItem;
      }
      const updateSetup = selectedItem.setup.map((roomConfig) => {
        const roomEventType = JSON.parse(JSON.stringify(roomConfig));
        if (roomEventType.setup_id !== setupId) {
          return roomEventType;
        }
        if (type === 'service') {
          roomEventType['edit'] = edit;
          const index = roomEventType.services_ids.indexOf(name);
          if (index > -1) {
            roomEventType.services_ids.splice(index, 1);
          } else {
            roomEventType.services_ids.push(name);
          }
          return roomEventType;
        } else {
          roomEventType[type] = name;
          roomEventType['edit'] = edit;
          return roomEventType;
        }
      });
      selectedItem['setup'] = updateSetup;
      return selectedItem;
    });
    this._store.selectedRooms = createdEvent;
  }

  paginationClick(action) {
    const { page } = action;
    this._store.selectedPage = parseInt(page, 10);
  }

  getHotelList(hotels) {
    const hotelListDetails = hotels.filter(hotel => hotel.status !== 'Disabled');
    this._store.hotelListDetails = hotelListDetails;
  }
  
  getUserInfoOfLatestHistory(userInfoOfLatestHistory) {
    this._store.userInfoOfLatestHistory = userInfoOfLatestHistory;
    if (userInfoOfLatestHistory.profile.role == 'Admin') {
      userInfoOfLatestHistory.profile['roleId'] = 'SA';
    } else {
      const firstInitialRole = userInfoOfLatestHistory.profile.role[0];
      const lastInitialRole = userInfoOfLatestHistory.profile.role.split(' ')[1] ? userInfoOfLatestHistory.profile.role.split(' ')[1].charAt(0) : '';
      let userRole = `${firstInitialRole}${lastInitialRole}`;
      userRole = userRole.toUpperCase();
      userInfoOfLatestHistory.profile['roleId'] = userRole;
    }
  }

  deletePeriod(action) {
    const guestInfo = this._store.guestInfo.filter(item => item.id !== action.id);
    this._store.guestInfo = guestInfo;
  }

  saveContact(contact) {
    this._store.contact = contact;
  }

  duplicateLayout(action) {
    const createdEvent = this._store.selectedRooms.map((room) => {
      const selectedItem = room;
      if (room.id !== action.room.id) {
        return selectedItem;
      }
      const updateSetup = selectedItem.setup.map((roomConfig) => {
        const roomEventType = JSON.parse(JSON.stringify(roomConfig));
        if (roomEventType.setup_id !== action.setup.setup_id) {
          roomEventType['addon'] = false;
          return roomEventType;
        }
        roomEventType['enable3D'] = false;
        roomEventType['active'] = true;
        return roomEventType;
      });
      selectedItem['setup'] = updateSetup;
      const newSetup = updateSetup.filter(each=>each.setup_id === action.setup.setup_id);
      if (newSetup.length === 0) {
        selectedItem['setup'].push(action.setup);
      }
      return selectedItem;
    });
    this._store.selectedRooms = createdEvent;
  }

  duplicateGuestInfo(action) {
    const { addGuest } = action;
    this._store.guestInfo.push(addGuest);
  }

  removeLayout(action) {
    const createdEvent = this._store.selectedRooms.map((room) => {
      const selectedItem = room;
      if (room.id !== action.room.id) {
        return selectedItem;
      }
      selectedItem['setup'] = selectedItem.setup.filter(setup => setup.setup_id !== action.setup.setup_id);
      return selectedItem;
    });
    this._store.selectedRooms = createdEvent;
  }

  removeGuestInfo(action) {
    const createdGuestInfo = this._store.guestInfo.filter((guest) => {
      return guest.tempID !== action.guest.tempID;
    });
    this._store.guestInfo = createdGuestInfo;
  }

  handle3DEdit(action) {
    this._store.save3D = action.isActive;
  }

  submitLayout(action) {
    const { layout, roomId } = action;
    const selectedRooms = this._store.selectedRooms.map((room) => {
      if (room.id !== roomId) return room;
      const updateRoomLayout = room;
      updateRoomLayout['setup'] = updateRoomLayout.setup.map((layoutRoom) => {
        if (layoutRoom.setup_id !== layout.setup_id) return layoutRoom;
        layout['config'] = layout['columns_per_row'] ? { columns: layout['columns_per_row'] } : layout['config'];
        const model3d = {};
        model3d['data'] = {};
        model3d['layout'] = layout.type;
        model3d['model3d'] = {};
        model3d['model2d'] = {};
        if (room.scenes.length >= 1) {
          model3d['model3d'] = room.scenes[0].model3d;
          model3d['model2d'] = room.scenes[0].model2d;
        }
        model3d['available_model'] = room.available_model || false;
        model3d['img'] = layout.img || false;
        layout['model_3d'] = model3d;
        return layout;
      });
      return updateRoomLayout;
    });
    this._store.layout = layout;
    this._store.selectedRooms = selectedRooms;
  }
 
  _registerToActions(payload) {
    const { action } = payload;
    let emitChange = true;
    switch (action.actionType) {
      case 'GET_USER_EVENT_INFO':
        this.getUserEventInfo(action.data, action.eventType);
        break;
      case 'GET_PAST_EVENT_INFO':
        this.getPastEventInfo(action.data, action.activeCount);
        break;
      case 'CLEAR_STORE_EVENT_INFO':
        this._store.eventInfo = {};
        this._store.activeCount = 0;
        this._store.pastCount = 0;
        break;
      case 'ADD_NOTIFICATION':
        this.addNotification(action);
        break;
		  case 'GET_USER_EVENT_STATUS':
		    this.getUserEventStatus(action.status);
		    break;
		  case 'GET_SELECTED_EVENT_ID':
		    this.getSelectedEventId(action.eventId, action.uuId);
		    break;
      case 'SET_SELECTED_EVENT_INFO_AS_EMPTY':
        this._store.selectedEventInfo = {};
        break;
		  case 'GET_SELECTED_EVENT_INFO':
		    this.getSelectedEventInfo(action.response);
		    break;
		  case 'GET_SELECTED_EVENT_HOTEL_INFO':
		    this.getUserSelectedEventHotelInfo(action.eventHotelInfo);
		    break;
		  case 'SAVE_ACTIVITY':
        this.saveActivitydetails(action.ActivityInfos);
        break;
      case 'GET_SETUP_TYPES':
        this.getSetupTypes(action.setupTypes);
        break;
      case 'SAVE_CONTACT':
        this.getContactInfo(action.contactDetails);
        break;
      case 'SAVE_PERIOD':
        this.getGuestInfo(action.guestInfo);
        break;
      case MeetingSpaceConstants.ATTACHED_FILE:
        this.getAttachedFiles(action.data);
        break;
      case 'ADDON_UPDATE':
        this.updateAddons(action);
        break;
      case 'SAVE_MEETING_ROOM_INFO':
        this.saveMeetingRoomInfo(action.meetingRoomInfo);
        break;
      case 'GET_HISTORY_INFO':
        this.getHistoryInfo(action.historyInfo);
        break;
      case 'EVENT_SEARCH':
      	this._store.searchVal = action.searchVal;
      	this.eventSearch();
      	break;
      case 'GET_EVENT_TYPES':
        this.getEventTypes(action.evnentTypes);
        break;
      case 'EVENT_LIST_PAGE':
        this.paginationClick(action);
        break;
      case 'GET_ALL_EVENT_HISTORY_INFO':
        this.getAllHistoryInfo(action.allEventHistory);
        break;
      case 'REMOVE_ATTACH_FILE':
        this.removeUploadedFile(action);
        break;
      case 'UPLOADING_FILE':
        this.uploadingFile(action);
        break;
      case 'SINGLE_HOTEL_LIST':
        this._store.allHotelList = this._store.allHotelList.concat(action.hotelList.results);
        this.getHotelList(this._store.allHotelList);
        break;
      case 'GET_USER_INFO_OF_LATEST_HISTORY':
        this.getUserInfoOfLatestHistory(action.userHistoryInfo);
        break;
      case 'CREATE_HOTEL_EVENT':
        this._store.selectedHotel = action.hotel;
        break;
      case 'SAVE_CONTACT':
        this.saveContact(action.contactDetails);
        break;
      case 'HOTEL_LIST':
        this._store.hotelListDetails = action.hotels;
        break;
      case 'GET_ACTIVITY_PROGRAMS':
        this._store.activityPrograms = action.res;
        break;
      case 'UPDATE_ACTIVITY_NAME':
        this.updateActivityName(action.params);
        break;
      case 'DELETE_PERIOD':
        this.deletePeriod(action);
        break;
      case 'GET_EVENT_HOTEL_CHAIN':
        this._store.selectedHotelChain = action.selectedHotelChain;
        break;
      case 'DUPLICATE_LAYOUT':
        this.duplicateLayout(action);
        break;
      case 'DUPLICATE_GUEST_INFO':
        this.duplicateGuestInfo(action);
        break;
      case 'REMOVE_LAYOUT':
        this.removeLayout(action);
        break;
      case 'REMOVE_GUEST_INFO':
        this.removeGuestInfo(action);
        break;
      case 'EDIT_3D_HANDLE':
        this.handle3DEdit(action);
        break;
      case 'SUBMIT_LAYOUT':
        this.submitLayout(action);
        break;
      default:
        emitChange = false;
        break;
    }
    if (emitChange) {
      this._emitChange(action.actionType);
    }
  }
}
export default new RFPManagementStore();
