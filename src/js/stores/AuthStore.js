import request from 'superagent';
import Cookies from 'js-cookie';
import StoreInterface from './StoreInterface';


const CLIENT_LIST = [
  {
    hotel_id: 1,
    client_id: 'eqr6PnaYgtQupd5psizUMM8GPs4ZzR4k56Q6linZ',
    client_secret: 'bV7H7jvkdkKqZqIqMPArGibHXtmbtgAPBBVdIo0lJw6SRECCFS52FRWIyFWoX09pq76H64af96H25Hx8ZyHkdVdikouOMbrDJDvQWcgwFlTR8UdPAwD9YDyELAmrrMW1',
    sys_user_name: 'api-anonymous-nh',
    sys_user_pwd: '4keJyZRL',
  },
  {
    hotel_id: 2,
    client_id: 'YZk9PoLQYqN6QkLMWW5P0TaRQbn3Q6HMjGCKl3KJ',
    client_secret: 'I2QDaQplL9tKW0a8k8rCH7DWBPpKSbEw2oRzfKlerWJz2gqaUWqrAvxnwe5PevP9embe1FD4b0XqkwHcoYZqS2puVrvlIxKvyxNd0xbyAHX9OWvu6soKONY4udRKldQ4',
    sys_user_name: 'api-anonymous-melia',
    sys_user_pwd: 'Cpr3nhAw',
  },
  {
      hotel_id: 6,
      client_id: 'GHDMJyn2178baJdwPjkrrPAIzJORJ7C0JYp2LXVp',
      client_secret: '5KsxzPFs2sAhaEkQptfoBS7YBkMI1rZVYheszspvqP4Ya4Jp0ZrRTPtJqHmurEpXT9dP0H3FCO1TB6zJNjVYfcPQmKTjTMMe4RJSXWfW9jXYhm52tNYVLsFZvcEaL3yU',
      sys_user_name: 'api-anonymous-barcelo',
      sys_user_pwd: 'OprYAv80'
  },
  {
      hotel_id: 12,
      client_id: 'TaUX6AYrvznRbMxPFGpqznK45HsvPha5JvnciZZi',
      client_secret: 'aoKH6D1FFtgoyxn5lzZB3mjAb58Ilicfhmgqi8wf3dQ9AvtWOQk9h6CPEntEHAuwVCVuOCzNm5RKD4cJcz58HEEwhFx17Mr5NpEjhY2yuFubl2YEPrtXxoQf4XGKaVUi',
      sys_user_name: 'api-anonymous-eurostars',
      sys_user_pwd: 'p4j4rr4c0'
  }
];

class AuthStore extends StoreInterface {
  constructor() {
    super({
      hotel: {
        id: null,
        client_token: null,
        access_token: null,
        refresh_token: null,
        expiry_time: null,
      },
      client: null,
      user: null,
    }, 'AuthStore');

    // queue system for serializing access to tokens
    this._queue = [];
    this._isProcessingQueue = false;

    // restore session from cookie
    this.restoreSession();
  }

  _addToQueue(callback) {
    this._queue.push(callback);
    if (!this._isProcessingQueue) {
      this._processQueue();
    }
  }

  _processQueue() {
    this._isProcessingQueue = false;
    const callback = this._queue.shift();
    if (callback) {
      this._isProcessingQueue = true;
      callback(() => { this._processQueue(); });
    }
  }

  /**
   * Restore session from cookie
   */
  restoreSession() {
    const hotel = Cookies.getJSON('hotel');
    if (hotel) {
      Object.assign(this._store.hotel, hotel);
    }
    const user = Cookies.getJSON('user');
    if (user) {
      if (!this._store.user) {
        this._store.user = {};
      }
      Object.assign(this._store.user, user);
    }
  }

  /**
   * Save session details in cookie
   */
  saveSession() {
    // save hotel
    const { hotel } = this._store;
    if (hotel) {
      const params = ['id', 'client_token', 'access_token', 'refresh_token', 'expiry_time', 'data'];
      const attributes = { expires: 10000 }; // never expires
      this._setJSONCookie('hotel', params, hotel, attributes);
    } else {
      // delete cookie
      Cookies.remove('hotel');
    }

    // save user
    const { user } = this._store;
    if (user) {
      const params = ['id', 'hotel_id', 'access_token', 'refresh_token', 'expiry_time', 'data'];
      const attributes = { expires: 10000 }; // never expires
      this._setJSONCookie('user', params, user, attributes);
    } else {
      // delete cookie
      Cookies.remove('user');
    }
  }

  /**
   * Set cookie with given name. Value is a subset dictionary of `data`. `params` has the key to be saved in cookie
   * @param name
   * @param params
   * @param data
   * @param attributes
   * @private
   */
  _setJSONCookie(name, params, data, attributes) {
    if (!attributes) {
      attributes = {};
    }
    const value = {};
    for (let i = 0; i < params.length; i++) {
      const k = params[i];
      if (data.hasOwnProperty(k)) {
        value[k] = data[k];
      }
    }
    Cookies.set(name, value, attributes);
  }

  /**
   * Delete all session cookies and reset session details in _store
   */
  resetHotel() {
    const { hotel } = this._store;
    for (const k in hotel) {
      if (hotel.hasOwnProperty(k)) {
        hotel[k] = null;
      }
    }
    Cookies.remove('hotel');
  }

  /**
   * get API end point
   * @returns {string}
   */
  getApiHost() {
    return Config.api_host.replace(/[\s/]*$/, '');
  }

  /**
   * Get client config like client id, secret, etc for given hotel id
   * @param hotel_id
   * @param callback
   */
  getClientConfig(hotel_id, callback) {
    console.log(`${hotel_id } hotel_id getClientConfig`);
    // @TODO - Fetch from backend
    setTimeout(() => {
      let result = null;
      for (let i = 0; i < CLIENT_LIST.length; i++) {
        const client = CLIENT_LIST[i];
        if (client.hotel_id == hotel_id) {
          result = client;
        }
      }
      callback(result);
    });
  }

  /**
   * Generates access tokens for the system/anonymous user for given hotel
   * @param hotel_id
   * @param callback
   */
  switchHotel(hotel_id, callback) {
    // console.log("Q: switchHotel", hotel_id);
    if (this._store.hotel.id === hotel_id) {
      this.getClientConfig(hotel_id, (config) => {
        if (config) {
          this._store.client = config;
        }
      });
      setTimeout(() => {
        callback(true);
      });
      return;
    }

    // flush existing session
    this.resetHotel();
    // get client details for new hotel
    this.getClientConfig(hotel_id, (config) => {
      let uname = null;
      let pwd = null;
      if (config) {
        this._store.client = config;
        this._store.hotel.id = hotel_id;
        this._store.hotel.client_token = btoa(`${config.client_id}:${config.client_secret}`);
        uname = config.sys_user_name;
        pwd = config.sys_user_pwd;
      }
      // generate system user access token with client details
      this.createAccessToken('hotel', uname, pwd, (success, data) => {
        if (success) {
          Object.assign(this._store.hotel, data);
          this.saveSession();
        }
        callback(success);
      });
    });
  }

  /**
   * Generate access and refresh tokens for given username and password. Uses current client_toekn set in the session
   * @param username
   * @param password
   * @param callback(success, data) data = {access_token, refresh_token, expiry_time}
   */
  createAccessToken(entityType, username, password, callback) {
    if (entityType === 'hotel') {
      console.log(`hotel_id this ${this._store.hotel.id}`);
      // hard coding access token for hotels (public)
      setTimeout(() => {
        const data = {
          access_token: this.getAnonymousTokenForHotel(),
          refresh_token: '',
          expiry_time: 0,
        };
        callback(true, data, {});
      });
      return;
    }


    const formData = new FormData();
    formData.append('grant_type', 'password');
    formData.append('username', username);
    formData.append('password', password);
    if (this._store.client) {
      formData.append('client_id', this._store.client.client_id);
      formData.append('client_secret', this._store.client.client_secret);
    }
    request.post(`${this.getApiHost()}/api/users/token`)
      .send(formData)
      .then((res) => {
        let success = true;
        let data = null;
        if (res.body && res.body.access_token) {
          data = {
            access_token: res.body.access_token,
            refresh_token: res.body.refresh_token,
            expiry_time: this._getExpiryTime(res.body.expires_in),
          };
          console.error('Success while creating access token. Response: ', res.body);
        } else {
          console.error('Error while creating access token. Response: ', res.text);
          success = false;
        }
        if (callback) {
          callback(success, data, res.body);
        }
      })
      .catch((err) => {
        console.error('Error while creating access token. ', err);
        if (callback) {
          const errRes = JSON.parse(err.response.text);
          callback(false, null, errRes);
        }
      });
  }

  /**
   * For the given entity(hotel or user), refresh access token using existing refresh token and client token
   * New token is generated only if existing access token is expired
   * @param entity
   * @param callback
   */
  refreshAccessToken(entity, callback) {
    console.log('Q: refreshAccessToken');

    // check if existing access token is expired
    if (this.isAccessTokenExpired(entity)) {
      const formData = new FormData();
      formData.append('grant_type', 'refresh_token');
      formData.append('refresh_token', entity.refresh_token);
      console.log('CLIENT_TOKEN:', this._store.hotel);
      this.getClientConfig(2, (config) => {
        if (config) {
          this._store.client = config;
          if (config) {
            formData.append('client_id', config.client_id);
            formData.append('client_secret', config.client_secret);
          }
          // console.log("Q: refreshAccessToken: refresh_token:", entity.refresh_token);
          request.post(`${this.getApiHost()}/api/users/token`)
          // .set('Authorization', 'Basic '+this._store.hotel.client_token)
            .send(formData)
            .then((res) => {
              let success = true;
              if (res.body && res.body.access_token) {
                entity.access_token = res.body.access_token;
                entity.refresh_token = res.body.refresh_token;
                entity.expiry_time = this._getExpiryTime(res.body.expires_in);
                // save in cookie
                this.saveSession();
              } else {
                this.logout();
                console.error('Error while refreshing access token. ', entity, res.text);
                success = false;
              }
              if (callback) {
                callback(success);
              }
            })
            .catch((err) => {
              this.logout();
              console.error('Error while refreshing access token. ', entity, err);
              if (callback) {
                callback(false);
              }
            });
        }
      });
    } else {
      // console.log("Q: refreshAccessToken: not expired");
      if (callback) {
        callback(true);
      }
    }
  }

  /**
   * Check if access token is expired for given entity(hotel/user)
   * @param entity
   * @returns {boolean}
   */
  isAccessTokenExpired(entity) {
    // console.log("Q: isAccessTokenExpired: ", JSON.stringify(entity));

    if (!entity) {
      return true;
    }
    if (!entity.expiry_time) {
      return false; // no expiry
    }
    return (new Date().getTime() > entity.expiry_time);
  }

  /**
   *
   * @param expires_in in seconds
   * @returns {Date}
   * @private
   */
  _getExpiryTime(expires_in) {
    let OFFSET = 300; // set expiry X seconds earlier
    if (expires_in < OFFSET) {
      OFFSET = 0;
    }
    const expire_in_ms = (expires_in - OFFSET) * 1000;
    return (new Date().getTime() + expire_in_ms);
  }

  /**
   * Get access_token for use in API
   * If logged in, user user's token else hotel's(anonymous user)
   * @param callback - If present, do refresh and return in callback. Else, return current access token
   */
  getAccessToken(callback) {
    console.log('Q: getAccessToken');
    let entity = this._store.hotel;
    if (this._store.user) {
      // logged in. use users' tokens
      entity = this._store.user;
    }
    if (callback) {
      // refresh if required
      this.refreshAccessToken(entity, (success) => {
        const access_token = success ? entity.access_token : null;
        callback(access_token);
      });
    } else {
      return entity.access_token;
    }
  }

  getAnonymousTokenForHotel() {
    console.log(`HOTEL_ID: ${this._store.hotel.id}`);
    this.anonymous_token = 'RF2TCj3XiIoT8n9ezymfyWge3bPACJ';
    // Barcelo fIwKgdeC5jIbDA9mNOFfY5huNrDezF
    return this.anonymous_token;
  }

  /**
   * Get Bearer header required for API calls
   * If logged in, use users' token else use system/hotel token
   * @param hotel_id - if absent, uses current hotel id
   * @param callback - If present, do refresh and return in callback. Else, return current access token in header
   */
  getHeaders(hotel_id, callback) {
    // HARDCODE_TOKENS-START
    // if ( ! callback) {
    //   // return current value as it is
    //   const token = this.getAnonymousTokenForHotel(hotel_id);
    //   return {
    //     'Authorization': 'Bearer '+token
    //   }
    // }
    // // else
    // setTimeout(() => {
    //   const access_token = this.getAnonymousTokenForHotel(hotel_id);
    //   // generate headers
    //   let headers = {
    //     'Authorization': 'Bearer '+access_token
    //   };
    //   callback(headers);
    // });
    //
    // return;
    // HARDCODE_TOKENS-END


    if (!callback) {
      // return current value as it is
      console.log(`hotel_id 432 ${hotel_id}`);
      const token = this.getAccessToken(hotel_id);
      return {
        Authorization: `Bearer ${token}`,
      };
    }
    // else
    this._addToQueue((next) => {
      // ensure we are in correct hotel
      hotel_id = hotel_id || this._store.hotel.id;
      console.log(`hotel_id 444 ${hotel_id}`);
      this.switchHotel(hotel_id, (success) => {
        if (success) {
          // get appropriate access token depending on login status
          this.getAccessToken((access_token) => {
            // run callback without blocking next request
            setTimeout(() => {
              // generate headers
                if(hotel_id == 6){
                    access_token = "QwkrO7O1XkOMdGNUmE5WZWiQfnaiGn";
                } else if(hotel_id == 12){
                    access_token = "eYAcHuBqkir7R6CGQ6RlQtGaIcoShf";
                } else if(hotel_id == 14){
                    access_token = "Vd6aqXTG7skPfUrT2Mb4qDCasX7XdJ";
                } else if(hotel_id == 1){
                    access_token = "RF2TCj3XiIoT8n9ezymfyWge3bJCAP";
                }
              let headers = {
                'Authorization': 'Bearer '+access_token
              };
              if (access_token) {
                callback(headers);
              } else {
                this.logout();
              }
            });

            // trigger next task
            next();
          });
        } else {
          // trigger next task
          next();
        }
      });
    });
  }

  /**
   * Login with given details
   * @param params
   * @param callback
   */
  login(params, callback) {
    console.log(params);
    this.switchHotel(2, (success) => {
      this.createAccessToken('user', params.username, params.password, (success, data, response) => {
        if (success) {
          if (!this._store.user) {
            this._store.user = {};
          }
          // this._store.user.id = response.data.user.id;
          // this._store.user.data = response.data.user;
          // data.id = response.id ? response.id : 0;
          Object.assign(this._store.user, data);
          this.saveSession();
        }
        callback(success, response);
      });
    });
  }

  /**
   * Logout. Make API call and also delete from cookie
   * @param callback
   */
  logout(callback) {
    // @TODO - Call backend API?
    setTimeout(() => {
      this._store.user = null;
      this.saveSession();
      if (callback) {
        callback(true);
      }
      this._emitChange(true);
    });
  }

  /**
   * Check if user is logged in
   * @returns {boolean}
   */
  isLoggedIn() {
    return (!!this._store.user);
  }

  /**
   * Get logged in user details
   * @param callback
   */
  getUser(callback) {
    if (!this._store.user) {
      // not logged in
      callback(null);
    } else if (!this._store.user.data) {
      // logged in, but data is not loaded yet.
      // fetch data from back end

      // dummy data
      // let tmpData = {
      //   "username": "testuser1",
      //   "email": "kalai+testuser1@tymtix.com",
      //   "first_name": "Test",
      //   "last_name": "User1",
      //   "last_login": null,
      //   "profile": {
      //     "role": "Admin",
      //     "birthday": "1990-01-15",
      //     "gender": "Male",
      //     "address": "24 Nile Street",
      //     "location": null,
      //     "city": "Madrid",
      //     "country": "ES",
      //     "postal_code": "28005",
      //     "status": {
      //       "id": 2,
      //       "name": "PENDING"
      //     },
      //     "notifications_enabled": true,
      //     "hotel_chain": {
      //       "name": "NH Hotels",
      //       "url": "http://spazious-api-dev.us-west-1.elasticbeanstalk.com/api/hotel_chain/1"
      //     }
      //   },
      //   "hotels": "http://spazious-api-dev.us-west-1.elasticbeanstalk.com/api/users/11/hotels"
      // };

      request.get(`${this.getApiHost()}/api/users/me`)
        .set('Authorization', `Bearer ${this._store.user.access_token}`)
        .then((res) => {
          let success = true;
          const data = null;
          if (res.body) {
            this._store.user.data = res.body;
            this.saveSession();
          } else {
            console.error('Error while fetching user data. Response: ', res.text);
            success = false;
            // this._store.user.data = tmpData; // comment out after we have user id
          }
          if (callback) {
            callback(success, this._store.user.data);
          }
        })
        .catch((err) => {
          console.error('Error while fetching user data. ', err);
          if (callback) {
            this._store.user.data = tmpData; // comment out after we have user id
            callback(false, this._store.user.data);
          }
        });
    } else {
      callback(this._store.user.data);
    }
  }

  /**
   * Forgot Password. Send link to the given email.
   * @param callback
   */
  forgotPassword(data, callback) {
    request.post(`${this.getApiHost()}/api/users/password_reset/`)
      .send(data)
      .end((err, res) => {
        let success = true;
        const response = JSON.parse(res.text);
        if (!err) {
          callback(success, response);
        } else {
          success = false;
          callback(success, response);
        }
      });
  }


  /**
   * Update New Password.
   * @param callback
   */
  resetPassword(data, callback) {
    request.post(`${this.getApiHost()}/api/users/password_reset/confirm/`)
      .send(data)
      .end((err, res) => {
        let success = true;
        const response = JSON.parse(res.text);
        if (!err) {
          callback(success, response);
        } else {
          success = false;
          callback(success, response);
        }
      });
  }

  getGoogleGeoLocationKey() {
    return 'AIzaSyAkeOA-cNb0DSxw6EMcynlQel6VRqteeto';
  }

  _registerToActions(payload) {
    const { action } = payload;
    let emitChange = true;
    switch (action.actionType) {
      default:
        emitChange = false;
        break;
    }
    if (emitChange) {
      this._emitChange(action.actionType);
    }
  }
}

export default new AuthStore();
